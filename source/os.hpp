#pragma once

#include <boost/optional.hpp>
#include <boost/coroutine/symmetric_coroutine.hpp>

#include <list>
#include <map>
#include <unordered_map>
#include <memory>
#include <queue>
#include <thread>

namespace Interpreter {
struct CPUContext;
struct Setup;
class ProcessorController;
}

namespace HLE {

namespace OS {

// Type used to identify objects by a number internally
// This is needed so that the emulated CPU core can pass around object handles without actually storing objects in emulated memory.
using Handle = uint32_t;
const Handle HANDLE_INVALID = 0;

// Type of a virtual address
// TODO: Make this a strong typedef
using VAddr = uint32_t;

// Type of a physical address
// TODO: Make this a strong typedef
using PAddr = uint32_t;

// Abstract object with some unique ID.
class Object {
public:
    virtual ~Object() = default;
};

// TODO: Add a reference counting base class?

// A single thread of execution.
class Thread;

// Utility function to print the dynamic type of the given thread at the beginning of each line.
std::ostream& ThreadLogger(Thread* thread);

// Called "KSynchronizationObject" on 3dbrew.
class ObserverSubject : public Object {
    // List of observers. May include the same Thread multiple times.
    std::list<std::weak_ptr<Thread>> observers;

    virtual bool TryAcquireImpl(std::shared_ptr<Thread>) {
        throw std::runtime_error("This should never be called!");
    }

protected:
    /**
     * Wake up all observers such that they can check again for resource
     * readiness using TryAcquire. Since the circumstances of resources
     * becoming ready vary, this function may only be called by
     * implementations of ObserverSubject.
     */
    void Signal();

public:
    virtual ~ObserverSubject() = default;

    /**
     * Tries to acquire the maintained resource in the given thread.
     * If successful, thread will be unregistered from observers.
     */
    bool TryAcquire(std::shared_ptr<Thread> thread);

    /**
     * Registers the given thread for notifications. This will make sure that
     * threads waiting for this subject to become ready will be woken up when
     * it does.
     * The thread will be unregistered once the event has been acquired using
     * TryAcquire.
     */
    void Register(std::shared_ptr<Thread> thread);

    /**
     * Manually unregister the given thread from notifications.
     */
    void Unregister(std::shared_ptr<Thread> thread);
};

/**
 * A handle table is a collection of handles owned by a particular
 * process (TODO: Currently, they are managed by threads!). Note that handles
 * are process-unique, but handles of different threads may refer to the same
 * object. The objects are "owned" in the sense that deleting all handles that
 * refer to a particular object will trigger destruction of the object.
 */
class HandleTable {
    std::unordered_map<Handle, std::shared_ptr<Object>> table;

    Handle cur_handle = 0;

    // Prints an internal logging message about a type mismatch.
    void ErrorWrongType(std::shared_ptr<Object> object, const char* requested_type);

public:
    template<typename Type>
    using Entry = std::pair<Handle, std::shared_ptr<Type>>;

    /**
     * Inserts the given Object into the handle table and returns a Handle to the object.
     * @return Immutable pair of Handle and pointer the object.
     */
    template<typename Type>
    Entry<Type> CreateHandle(std::shared_ptr<Type> object) {
        static_assert(std::is_base_of<Object,Type>::value, "object must be a shared_ptr to a derivative class of Object!");
        ++cur_handle; // TODO: Make sure this never becomes one of the reserved handles
        table[cur_handle] = object;
        return { cur_handle, object };
    }

    /**
     * Inserts the given Object into the handle table with the specified Handle.
     * Use this to set up reserved handles (e.g. 0xFFFF8001 for the handle of the current process)
     */
    template<typename Type>
    void CreateEntry(Handle handle, std::shared_ptr<Type> object) {
        static_assert(std::is_base_of<Object,Type>::value, "object must be a shared_ptr to a derivative class of Object!");
        if (table.count(handle))
            throw std::runtime_error("Handle already in table!");

        table[handle] = object;
    }

    /**
     * Returns a shared pointer to the kernel Object referenced by handle if
     * it is in the table and if the object can be downcast to Type.
     * @param fail_expected If true, we won't log an error when an object of different type is found with the given handle
     * @return shared_ptr of the given Type. nullptr if the handle can't be used to obtain a pointer to Type.
     */
    template<typename Type>
    std::shared_ptr<Type> FindObject(Handle handle, bool fail_expected = false) {
        static_assert(std::is_base_of<Object,Type>::value, "object must be a shared_ptr to a derivative class of Object!");

        if (table.count(handle) == 0)
            return nullptr;

        auto object_ptr = std::dynamic_pointer_cast<Type>(table[handle]);
        if (!fail_expected && !object_ptr)
            ErrorWrongType(table[handle], typeid(Type).name());
        return object_ptr;
    }

    template<typename Type>
    Handle FindHandle(std::shared_ptr<Type> object) {
        static_assert(std::is_base_of<Object,Type>::value, "object must be a shared_ptr to a derivative class of Object!");
        for (auto& entry : table) {
            if (entry.second == object) {
                return entry.first;
            }
        }
        return HANDLE_INVALID;
    }

    /**
     * Sets the currently running thread (to be exposed as handle 0xFFFF8000)
     */
    void SetCurrentThread(std::shared_ptr<Thread> thread) {
        table[0xFFFF8000] = thread;
    }

    /**
     * Closes the given handle. Does not release any locks still hold on ObserverSubjects.
     * @todo Implement error case?
     */
    void CloseHandle(Handle handle);
};

class Process;
class OS;

class Thread : public ObserverSubject {
    struct VirtualMemoryBlock {
        uint32_t phys_start;
        uint32_t size;
    };
public: // TODO: Un-public-ize this!
    // Map from starting address to the virtual memory block
    std::map<uint32_t, VirtualMemoryBlock> virtual_memory;

protected:
    /**
     * Called from MapVirtualMemory when a mapping took place.
     * @param phys_addr Base physical address to map data from
     * @param size Number of bytes to map.
     * @param vaddr Virtual address at which to map the given physical memory
     * @todo This should actually be a Process matter!
     */
    virtual void OnVirtualMemoryMapped(PAddr phys_addr, uint32_t size, VAddr vaddr) = 0;

public:
    std::weak_ptr<Process> parent_process;

    // TODO: Make this private!
    struct Context {
        uint32_t regs[16];
        uint32_t cpsr;
    } context;

    bool sleeping = false; // Waiting until SignalResourceReady is called
    std::list<std::pair<int32_t,std::shared_ptr<ObserverSubject>>> wait_list; // list of pairs of (original) index and waitable object; wait_list elements must have a handle in the current thread/process!

    // Notify thread of a resource being ready for being TryAcquire'ed
    void SignalResourceReady();

    boost::coroutines::symmetric_coroutine<void>::call_type coroutine;
    boost::coroutines::symmetric_coroutine<void>::yield_type* coroutine_yield;

    Thread();

    virtual ~Thread() = default;

    void SetOwningProcess(std::shared_ptr<Process> process);

    // Guaranteed to be valid, since every thread must have a process.
    std::shared_ptr<Process> GetParentProcess() {
        return parent_process.lock();
    }

    std::shared_ptr<Thread> GetPointer();

    HandleTable& GetHandleTable();

    OS& GetOS();

    // Save the current CPU context into "context"
    virtual void SaveContext(const Interpreter::CPUContext& ctx) = 0;

    // Restore the saved CPU context
    virtual void RestoreContext(Interpreter::CPUContext& ctx) = 0;

    /**
     * Run the thread. Execution may yield back to the OS and consecutively other threads through the OS's internal scheduling coroutine.
     * @note In order for execution to yield back to the OS, some system call must be invoked.
     */
    virtual void Run() = 0;

    /**
     * Read a word from the TLS at the given byte offset (must be a multiple of 4)
     * @todo Obsolete. Replace usage of this with ReadMemory
     */
    virtual uint32_t ReadTLS(uint32_t byte_offset) = 0;

    /**
     * Write a word to the TLS at the given byte offset (must be a multiple of 4)
     * @todo Obsolete. Replace usage of this with WriteMemory
     */
    virtual void WriteTLS(uint32_t byte_offset, uint32_t value) = 0;

    /**
     * Write a byte to a location in this thread's virtual memory
     */
    virtual void WriteMemory(VAddr addr, uint8_t value) = 0;

    /**
     * Read a byte from a location in this thread's virtual memory
     */
    virtual uint8_t ReadMemory(VAddr addr) = 0;

    /**
     * Translate the given virtual address to a physical one.
     */
    boost::optional<uint32_t> ResolveVirtualAddr(VAddr addr);

    /**
     * Finds the first available virtual address range of the given size in bytes within the given bounds.
     * @param size Minimum number of bytes in the given range
     * @param vaddr_start Minimal virtual address at which to look for available locations
     * @param vaddr_end Virtual address at which to stop looking for available locations
     * @return On success, returns the base virtual address for the found range
     * @todo This should actually be a Process matter!
     */
    boost::optional<uint32_t> FindAvailableVirtualMemory(uint32_t size, VAddr vaddr_start, VAddr vaddr_end);

    /**
     * Set up a mapping from virtual memory to physical memory.
     * @param phys_addr Base physical address to map data from
     * @param size Number of bytes to map.
     * @param vaddr Virtual address at which to map the given physical memory
     * @return true on success, false otherwise
     * @todo This should actually be a Process matter!
     */
    bool MapVirtualMemory(PAddr phys_addr, uint32_t size, VAddr vaddr);
};

// A thread performing CPU emulation. This is as close as to a "true" emulated userland thread as it gets.
class EmuThread : public Thread {
public: // TODO: Un-public-ize this!
    Interpreter::CPUContext& ctx;
//private: // TODO: This one, too
    VAddr tls;

    std::array<uint32_t,16> regs;

public:
    EmuThread(Interpreter::CPUContext& ctx, VAddr tls);

    virtual ~EmuThread() = default;

    void SaveContext(const Interpreter::CPUContext& ctx) override;

    void RestoreContext(Interpreter::CPUContext& ctx) override;

    virtual void Run() override;

    virtual uint32_t ReadTLS(uint32_t offset) override;

    virtual void WriteTLS(uint32_t offset, uint32_t value) override;

    void WriteMemory(VAddr addr, uint8_t value) override;

    uint8_t ReadMemory(VAddr addr) override;

    void OnVirtualMemoryMapped(PAddr phys_addr, uint32_t size, VAddr vaddr) override;
};

// A high-level emulated thread that omits any CPU emulation and instead executes a given C++ function
// Implementations must provide the Thread::Run() member function.
class FakeThread : public Thread {
    // Fake thread local storage - initialized to zero to make sure all static buffer parameters are reset, etc.
    std::array<uint32_t,0x200> tls{};

    struct StaticBuffer {
        std::vector<uint8_t> data;
    };

    /**
     * Fake static buffers (maps from the fake start address to the data).
     * Contained buffers are guaranteed to be non-overlapping.
     */
    std::map<uint32_t, StaticBuffer> static_buffers;

protected:
    /**
     * Allocate a static buffer for use in IPC requests.
     * @return A fake address used to identify the static buffer data.
     */
    uint32_t AllocateStaticBuffer(uint32_t size);

    /**
     * Allocate a static buffer for use in IPC requests
     * @param address Address previously obtained by AllocateStaticBuffer, which is used to identify the buffer data.
     */
    void FreeStaticBuffer(uint32_t address);

public:
    virtual ~FakeThread() = default;

    void SaveContext(const Interpreter::CPUContext& ctx) override {
        // No context to save
        // TODO: Actually, we might want to restore MMU configuration (e.g. to access IO registers)!
    }

    void RestoreContext(Interpreter::CPUContext& ctx) override {
        // No context to restore
    }

    uint32_t ReadTLS(uint32_t offset) override {
        return tls[offset / 4];
    }

    void WriteTLS(uint32_t offset, uint32_t value) override {
        tls[offset / 4] = value;
    }

    void WriteMemory(VAddr addr, uint8_t value) override;

    uint8_t ReadMemory(VAddr addr) override;

    void OnVirtualMemoryMapped(PAddr phys_addr, uint32_t size, VAddr vaddr) override;

    /**
     * Allocate an internal memory buffer and set up a mapping from virtual
     * memory to Process-internal memory that is accessible by the kernel but
     * need not be accessible by any other system entities (in particular,
     * not by EmuThread).
     *
     * @param size Number of bytes to map.
     * @param vaddr Virtual address at which to map the given physical memory
     * @return true on success, false otherwise
     * @todo This should actually be a Process matter!
     */
    bool CreatePrivateMemory(uint32_t size, VAddr vaddr);
};

// A process (for a very broad definition of "process").
class Process : public Object {
    void WaitForSignal();

    OS& os;

public:
    // TODO: Consider making this private!
    std::list<std::shared_ptr<Thread>> threads;

    HandleTable handle_table;

    Process(OS& os);

    virtual ~Process() = default;

    OS& GetOS() { return os; }

    std::shared_ptr<Thread> GetThreadPointer(Thread& thread);

    // Returns the use_count of the std::shared_pointer holding the given thread
    long GetThreadUseCount(Thread& thread);
};

class Port;
class ServerSession;

class ServerPort : public ObserverSubject {
public:
    ServerPort(std::shared_ptr<Port> port) : port(port) {
    }

    virtual ~ServerPort() = default;

    // TODO: This should actually consume and return a ServerSession if possible!
    bool TryAcquireImpl(std::shared_ptr<Thread> thread) override;

    void OfferSession(std::shared_ptr<ServerSession> session);

    std::shared_ptr<Port> port;

    // Queue of incoming sessions
    std::queue<std::shared_ptr<ServerSession>> session_queue;
};

class ClientPort : public ObserverSubject {
public:
    ClientPort(std::shared_ptr<Port> port) : port(port) {
    }

    virtual ~ClientPort() = default;

    std::shared_ptr<Port> port;
};

class Session;

class ServerSession : public ObserverSubject {
    uint32_t ipc_commands_pending = 0;

public:
    ServerSession(std::shared_ptr<Session> session) : session(session) {
    }

    virtual ~ServerSession() = default;

    bool TryAcquireImpl(std::shared_ptr<Thread> thread) override;

    void CommandReady();

    std::shared_ptr<Session> session;
};

class ClientSession : public ObserverSubject {
    bool ready = false;

public:
    ClientSession(std::shared_ptr<Session> session) : session(session) {
    }

    virtual ~ClientSession() = default;

    bool TryAcquireImpl(std::shared_ptr<Thread> thread) override;

    void SetReady();

    std::shared_ptr<Session> session;

    // location of the thread that last sent an IPC request using this session (for the server to read IPC requests from its TLS)
    std::weak_ptr<Thread> thread;
};

class Port : Object {
public:
    virtual ~Port() = default;

    // NOTE: Port is owned by its children, since one child is enough for a Port to exist, but if both children are released, the parent Port dies to.
    std::weak_ptr<ServerPort> server;
    std::weak_ptr<ClientPort> client;
};

class Session : Object {
public:
    virtual ~Session() = default;

    // NOTE: Session is owned by its children, since one child is enough for a Session to exist, but if both children are released, the parent Session dies to.
    std::weak_ptr<ClientSession> client;
    std::weak_ptr<ServerSession> server;
};

class Mutex : public ObserverSubject {
    // TODO: Make this class thread-safe!

    bool locked = false;

    // Thread currently holding a lock in this mutex
    std::weak_ptr<Thread> owner;

public:
    Mutex(bool locked, std::shared_ptr<Thread> initial_owner) : locked(locked), owner(initial_owner) {};

    virtual ~Mutex() = default;

    bool TryAcquireImpl(std::shared_ptr<Thread> thread) override;

    void Release();

    bool IsOwner(std::shared_ptr<Thread> thread) const;
};

enum class ResetType : uint32_t {
    OneShot = 0, // Be acquirable once, then reset
    Sticky  = 1, // Be acquirable until explicitly reset
    Pulse   = 2, // Timer-only: Be acquirable once, then reset and restart the timer.
};

class Event : public ObserverSubject {
    // TODO: Make this class thread-safe!

    bool signalled = false;

    ResetType type;

public:
    Event(ResetType type) : type(type) {
    }

    virtual ~Event() = default;

    bool TryAcquireImpl(std::shared_ptr<Thread> thread) override;

    void SignalEvent();

    void ResetEvent();
};

struct MemoryBlock {
    uint32_t size_bytes;
};

struct RegionAllocation {
    // physical starting address of region
    uint32_t region_start_paddr;

    uint32_t region_size_bytes;

    // Maps describing the free/taken memory blocks (mapped by their starting physical address)
    std::map<uint32_t, MemoryBlock> free;
    std::map<uint32_t, MemoryBlock> taken;
};

class MemoryManager {
    RegionAllocation region_app;

public:
    MemoryManager();

    /**
     * Allocate a memory block of the given size.
     * @return Starting address of the allocated buffer. boost::none on failure.
     */
    boost::optional<uint32_t> AllocateBlock(uint32_t size_bytes);
};

class SharedMemoryBlock : public Object {
public:
    // Address to data stored in emulated memory
    uint32_t phys_address;

    uint32_t size;

    SharedMemoryBlock(uint32_t physical_address, uint32_t size) : phys_address(physical_address), size(size) {
    }

    virtual ~SharedMemoryBlock() = default;
};

// A service is an anonymous port that is queryable through the port srv, which needs service processes to provide the GetPrivateName method.
class Service : Port {
public:
    virtual const char* GetPrivateName() = 0;
};

class OS {
    // Thread list for the scheduler (threads are owned by processes).
    std::list<std::weak_ptr<Thread>> threads;

/*    // GDB stub for Core 1
    std::unique_ptr<Interpreter::GDBStub> gdbstub;
*/
    // Thread for running the GDB stub
    std::unique_ptr<std::thread> gdbthread;

    // TODO: Currently, we never release any of the references hold here before shutdown!
    std::map<std::string, std::shared_ptr<Port>> ports;

    boost::coroutines::symmetric_coroutine<void>::call_type scheduler_coro;

    MemoryManager memory;

public:
    // Process handle table - all processes are owned exclusively by this table.
    HandleTable process_handles;

    static const int32_t MAX_SESSIONS = 0x1000; // Arbitrary choice

    Thread* active_thread;

    using Result = int32_t;

    enum class BreakReason : uint32_t {
        Panic  = 0,
        Assert = 1,
        User   = 2
    };

    // System calls begin here
    std::pair<OS::Result,uint32_t> SVCControlMemory(Thread& source, uint32_t addr0, uint32_t addr1, uint32_t size, uint32_t operation, uint32_t permissions);

    std::pair<OS::Result,HandleTable::Entry<Thread>> SVCCreateThread(Thread& source, uint32_t entry, uint32_t arg, uint32_t stack_top, uint32_t priority, uint32_t processor_id);

    void SVCExitProcess(std::shared_ptr<Process> source);
    void SVCExitThread(Thread& source);

    std::pair<Result,HandleTable::Entry<Mutex>> SVCCreateMutex(Thread& source, bool lock);
    Result SVCReleaseMutex(Thread& source, Handle mutex);

    std::pair<Result,HandleTable::Entry<Event>> SVCCreateEvent(Thread& source, ResetType type);
    Result SVCSignalEvent(Thread& source, Handle event);
    Result SVCClearEvent(Thread& source, Handle event);

    std::pair<Result,HandleTable::Entry<SharedMemoryBlock>> SVCCreateMemoryBlock(Thread& source, VAddr addr, uint32_t size, uint32_t owner_perms, uint32_t other_perms);
    Result SVCMapMemoryBlock(Thread& source, Handle block_handle, VAddr addr, uint32_t caller_perms, uint32_t other_perms);

    Result SVCCloseHandle(Thread& source, Handle object);
    Result SVCWaitSynchronization(Thread& source, Handle handle, uint64_t timeout);
    std::pair<Result,int32_t> SVCWaitSynchronizationN(Thread& source, Handle* handles, uint32_t handle_count, bool wait_for_all, uint64_t timeout);
    std::pair<Result,HandleTable::Entry<ClientSession>> SVCConnectToPort(Thread& source, const std::string& name);
    Result SVCSendSyncRequest(Thread& source, Handle session);
    void SVCBreak(Thread& source, BreakReason reason);
    std::tuple<Result,HandleTable::Entry<ServerPort>,HandleTable::Entry<ClientPort>> SVCCreatePort(Thread& source, const std::string& name, int32_t max_sessions);
    std::pair<Result,HandleTable::Entry<ClientSession>> SVCCreateSessionToPort(Thread& source, Handle client_port);
    std::pair<Result,HandleTable::Entry<ServerSession>> SVCAcceptSession(Thread& source, ServerPort& server_port); // returns server session
    std::pair<Result,int32_t> SVCReplyAndReceive(Thread& source, Handle* handles, uint32_t handle_count, Handle reply_target); // returns index into @p handles

    // Fake SVCs begin here - these don't map to native SVCs but provide useful functionality
    void SVCRaw(Thread& source, unsigned svc_id, Interpreter::CPUContext& ctx);
    void SVCDoNothing(Thread& source); // TODO: We don't really need this one, it was just for testing!

    /**
     * Add the given Thread to the internal thread list recognized by the scheduler.
     * Intended to be used for FakeThreads, only. Native threads are added automatically when calling SVCCreateThread.
     */
    void SVCAddThread(std::shared_ptr<Thread> thread);

    OS();

    /**
     * Initialized the OS, spawning all service processes along the way.
     */
    void Initialize();

    /**
     * Creates a process.
     * Intended to be used as part of initialization for setting up user code.
     * @todo needs to have the virtual address information, too...
     * @todo actually, needs to have the full CodeSet info passed as a parameter
     */
    void CreateProcess(uint32_t entry_point_virtual, Interpreter::CPUContext& ctx);

    /**
     * Creates a process with a single thread.
     */
    std::shared_ptr<Process> MakeProcessFromThread(std::shared_ptr<Thread> thread);

    /**
     * Runs the OS dispatcher.
     * @note This must be called after Initialize()
     * @note You have to add any user process explicitly before calling this if it should be ran, too.
     */
    void Run(std::shared_ptr<Interpreter::Setup> setup, std::unique_ptr<Interpreter::ProcessorController> controller);

    /**
     * Runs the OS dispatcher. Does not return until all threads have exited.
     * @todo This currently seems to overlap with Run in terms of functionality, but ultimately we want to move the whole OS scheduler/dispatcher to this function.
     */
    void StartScheduler(boost::coroutines::symmetric_coroutine<void>::yield_type& yield);

    /**
     * @pre The given thread must be the one currently running.
     */
    void SwitchToSchedulerFromThread(Thread& thread);

    /**
     * Translate an IPC message (i.e. a request or a reply) from the source
     * thread to the destination thread.
     */
    void TranslateIPCMessage(Thread& source, Thread& dest);
};

const OS::Result RESULT_OK = 0;

}  // namespace OS

}  // namespace HLE

// Now that we're done with the definitions, include some definitions that are required to instantiate the above structs (e.g. due to unique_ptr being used on incomplete types)
#include "gdb_stub.h"
