#pragma once

#include <memory>

// TODO: namespace Memory

template<typename AddrType, AddrType Offset, AddrType Size>
struct RawMemory {
    static constexpr AddrType size   = Size;
    static constexpr AddrType offset = Offset;

    uint8_t data[size];
};

template<typename AddrType, AddrType Start, AddrType Size>
struct AbstractMemory {
    static constexpr AddrType size   = Size;
    static constexpr AddrType offset = Start;
};

struct PhysicalMemory {
    using mpcore_private = AbstractMemory<uint32_t, 0x17e00000, 0x00002000>;
    RawMemory<uint32_t, 0x18000000, 0x00600000> vram;
    RawMemory<uint32_t, 0x1ff00000, 0x00080000> dsp;
    RawMemory<uint32_t, 0x1ff80000, 0x00080000> axi;
    RawMemory<uint32_t, 0x20000000, 0x08000000> fcram;
};
static_assert(std::is_pod<PhysicalMemory>::value, "PhysicalMemory is not a POD type");
