#include "gdb_stub.h"
#include "interpreter.h"

#include <boost/asio.hpp>
#include <boost/endian/buffers.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/optional.hpp>
#include <boost/range/adaptor/indexed.hpp>
#include <boost/range/adaptor/sliced.hpp>
#include <boost/range/numeric.hpp>

#include <cstring>

#include <iomanip>
#include <iostream>
#include <memory>

namespace basio = boost::asio;


namespace Interpreter {

void GDBStub::WaitForNextCommand(basio::ip::tcp::iostream& stream) {
    for (;;) {
        auto c = stream.get();
        if (c == basio::ip::tcp::iostream::traits_type::eof())
            return;
        if (c == '$')
            break;
        if (c == '+') {
            std::cerr << "Stub" << GetCPUId() << " received ACK" << std::endl;
            continue;
        }
        if (c == '-') {
            std::cerr << "Stub" << GetCPUId() << " received NACK" << std::endl;
            continue;
        }

        // SIGINT
        if (c == 3) {
            std::stringstream ss;
            ss << std::hex << "T" << std::setw(2) << std::setfill('0') << c;
            SendPacket(stream, ss.str());
            continue;
        }

        std::cerr << "Stub" << GetCPUId() << " skipping char \'" << (char)c << "\' (" << std::hex << (unsigned)c << ")" << std::endl;
    }
}

static unsigned char GetChecksum(const std::string& buffer) {
    return boost::accumulate(buffer, (unsigned char)0);
}

void GDBStub::SendPacket(basio::ip::tcp::iostream& stream_, const std::string& message) {
    std::stringstream stream;
    stream << '$';
    stream << message;
    stream << '#';
    stream << std::setw(2) << std::hex << std::setfill('0') << (unsigned)GetChecksum(message) << std::flush;

    stream_ << stream.str() << std::flush;
    std::cout << "Stub" << GetCPUId() << " sending message \"" << stream.str() << "\" (length " << stream.str().length() << ")" << std::endl;
}

static void AckReply(basio::ip::tcp::iostream& stream) {
    stream << '+' << std::flush;
}

static void NackReply(basio::ip::tcp::iostream& stream) {
    stream << '-' << std::flush;
}

static boost::optional<unsigned> HexCharToInt(char ch) {
    if (ch >= 'a' && ch <= 'f')
        return ch - 'a' + 0xa;
    if (ch >= 'A' && ch <= 'F')
        return ch - 'A' + 0xa;
    if (ch >= '0' && ch <= '9')
        return ch - '0';

    return {};
}

// Optionally returns a number + the number of characters parsed
template<typename RangeType>
static boost::optional<std::pair<uint32_t, size_t>> ReadHexNumber(const RangeType& str) {
    uint32_t ret = 0;
    size_t index = 0;

    // Reject pathological inputs
    if (boost::empty(str) || !HexCharToInt(*boost::begin(str)))
        return {};

    for (const auto& c : str) {
        auto digit = HexCharToInt(c);
        // Stop once we reach a character which can't be converted
        if (!digit)
            return std::make_pair(ret, index);

        // Check if there is room for one more digit, return failure otherwise
        if (ret & 0xF0000000)
            return {};

        ret <<= 4;
        ret |= *digit;
        ++index;
    }

    return std::make_pair(ret, (size_t)boost::size(str));
}

/**
 * GDB expects these register indices:
 * reg_index = N < 16: rN
 * reg_index = 25: CPSR
 * reg_index = 58: FPSCR
 *
 * Returns no value if the corresponding register hasn't been implemented, yet.
 */
static boost::optional<uint32_t> GetRegisterValue(const ARM::State& cpu_state, size_t reg_index) {
    if (reg_index < 16)
        return cpu_state.reg[reg_index];

    // Unknown register
    return {};
}

void GDBStub::ReportSignal(basio::ip::tcp::iostream& stream, int signum) {
    std::stringstream ss;
    ss << "S" << std::hex << std::setfill('0') << std::setw(2) << signum;
    SendPacket(stream, ss.str());
}

GDBStub::GDBStub(CPUContext& ctx, unsigned port) : ctx(ctx), acceptor(io_service, basio::ip::tcp::endpoint(basio::ip::tcp::v4(), port)) {
    acceptor.accept(*stream.rdbuf());
    std::cout << "Stub" << GetCPUId() << " connected!" << std::endl;

    // Be paused after startup.
    paused = true;
    request_pause = true;

    // Create a thread that loops indefinitely to process events. TODO: This should be quittable!
    stubthread = std::make_unique<std::thread>([this](){ while (true) ProcessQueue(); });
}

// TODO: Get rid of this ugly global!
static bool process_event_queue_triggered_signal = false;

void GDBStub::Continue(basio::ip::tcp::iostream& stream, bool single_step) {
    process_event_queue_triggered_signal = false;
    try {
        if (single_step)
            RequestStep();
        else {
            do {
                // TODO: Keep receiving GDB packets for incoming SIGINTs
                RequestContinue();

                // TODO: Ideally, we would want to do this in the main loop..
                ProcessEventQueue();

                if (process_event_queue_triggered_signal)
                    single_step = true;
            } while (!single_step);

            if (!process_event_queue_triggered_signal)
                RequestPause();
        }
    } catch (std::runtime_error& e) {
        std::cerr << "Stub" << GetCPUId() << " reporting SIGABRT due to exception in CPU emulator: " << e.what() << std::endl;
        ReportSignal(stream, 6);
        return;
    }

    // Done => SIGTRAP
    if (!process_event_queue_triggered_signal)
        ReportSignal(stream, 5);
}

bool GDBStub::HandlePacket(basio::ip::tcp::iostream& stream, const std::string& command) {
    auto MatchesSubcmd = [&command](const char* subcmd) -> bool{
        return command.compare(1, std::strlen(subcmd), subcmd) == 0;
    };

    switch (command[0]) {
    case '?':
    {
        std::stringstream ss;
        ss << "T" << std::hex << std::setfill('0') << std::setw(2) << 5/*GDB_SIGNAL_TRAP*/ << "thread:" << std::setw(2) << 0 << ";";
        SendPacket(stream, ss.str());
        break;
    }

    case 'c':
    {
        // continue until we find a reason to stop
        Continue(stream);
        return true;
    }

    case 'C':
    {
        auto signum = (!!HexCharToInt(command[2]))
                     ? ((*HexCharToInt(command[1]) << 4) | *HexCharToInt(command[2]))
                     : *HexCharToInt(command[1]);
        // TODO: Actually handle this properly, for now we just do the same as if 'c' had been sent...
        Continue(stream);
        return true;
    }

    case 's':
    {
        // single step
        Continue(stream, true);
        return true; // Let the other CPU core advance by one instruction, too
    }

    case 'g':
    {
        // Report register values
        std::stringstream ss;
        for (unsigned i = 0; i < 38; ++i) {
            // Send each register to the server individually
            // NOTE: Register values need to be output in guest byte order!
            auto val = GetRegisterValue(ctx.cpu, i);
            if (val) {
                boost::endian::endian_buffer<boost::endian::order::little, uint32_t, 32> le_val(*val);
                for (auto digit_ptr = le_val.data(); digit_ptr < le_val.data() + sizeof(le_val); ++digit_ptr) {
                    ss << std::hex << std::setfill('0') << std::setw(2) << +*(unsigned char*)digit_ptr;
                }
            } else if (i == 37) { // TODO: Actually, CPSR should be register 25... ... although, if it's 0x25, that's equivalent to 37
                boost::endian::endian_buffer<boost::endian::order::little, uint32_t, 32> le_val(ctx.cpu.cpsr.ToNativeRaw32());
                for (auto digit_ptr = le_val.data(); digit_ptr < le_val.data() + sizeof(le_val); ++digit_ptr) {
                    ss << std::hex << std::setfill('0') << std::setw(2) << +*(unsigned char*)digit_ptr;
                }
            } else if (i >= 16 && i < 24) {
                // FP registers..
                ss << "xxxxxxxxxxxx"; // 12 byte
            } else {
                ss << "xxxxxxxx";
			}
        }
        if (ss.str().length())
            SendPacket(stream, ss.str());
        break;
    }

    case 'H':
    {
        // Set thread for subsequent operations
        std::cerr << "Stub" << GetCPUId() << " set thread for subsequent operations: " << command << std::endl;
        SendPacket(stream, "OK");
        break;
    }

    case 'm':
    {
        // Read length bytes from the given address
        std::stringstream ss(command, std::ios_base::in);
        uint32_t address;
        uint32_t length;
        ss.ignore(1, 'm');
        ss >> std::hex >> address;
        ss.ignore(1, ',');
        ss >> std::hex >> length;

        std::cerr << "Stub" << GetCPUId() << " reading from 0x" << std::hex << address << " to " << std::hex << (address+length) << std::endl;

        std::stringstream ss_out;
        try {
            for (; length;) {
// TODO: This is broken due to endianness issues!
/*                if (length < 4) {
                    if (length >= 2) {
                        uint16_t val = Interpreter::Processor::ReadVirtualMemory<uint16_t>(*proc.setup, address);
                        ss_out << std::hex << std::setfill('0') << std::setw(4) << val;
                        length -= 2;
                    }
                    if (length) {
                        uint8_t val = Interpreter::Processor::ReadVirtualMemory<uint8_t>(*proc.setup, address);
                        ss_out << std::hex << std::setfill('0') << std::setw(4) << val;
                    }
                    break;
                }

                uint32_t val = Interpreter::Processor::ReadVirtualMemory<uint32_t>(*proc.setup, address);
                ss_out << std::hex << std::setfill('0') << std::setw(8) << val;

                length -= 4;
                address += 4;*/
                uint8_t val = Interpreter::Processor::ReadVirtualMemory<uint8_t>(ctx, address);
                ss_out << std::hex << std::setfill('0') << std::setw(2) << +val;
                length--;
                address++;
            }
            SendPacket(stream, ss_out.str());
        } catch (...) {
            std::cerr << "Stub" << GetCPUId() << " invalid memory access" << std::flush;
            if (!ss_out.str().empty()) {
                std::cerr << " (returning partial region)" << std::endl;
                SendPacket(stream, ss_out.str());
            } else {
                std::cerr << " (returning error)" << std::endl;
                SendPacket(stream, "E00");
            }
        }
        break;
    }

    case 'p':
    {
        // Read value of register N
        auto index = (!!HexCharToInt(command[2]))
                     ? ((*HexCharToInt(command[1]) << 4) | *HexCharToInt(command[2]))
                     : *HexCharToInt(command[1]);
        std::stringstream ss;
        auto val = GetRegisterValue(ctx.cpu, index);
        if (val) {
            throw 5; // TODO: This should print the byte representation of the value instead!
            std::cerr << std::hex << std::setfill('0') << std::setw(8) << *val << std::endl;
        } else {
            ss << "xxxxxxxx";
            std::cerr << "xxxxxxxx" << std::endl;
        }
        SendPacket(stream, ss.str());

        break;
    }

    case 'q':
    {
        if (MatchesSubcmd("C")) {
            // Report currently active thread
            // Reporting a dummy ID for now.
            SendPacket(stream, "QC 133"); // TODO: Does this actually include a space?
        } else if (MatchesSubcmd("Supported")) {
            // We don't have any limit on the maximal packet size - hence report a really large value to speed up memory dumping.
            SendPacket(stream, "PacketSize=200000");
        } else if (MatchesSubcmd("Attached")) {
            // Pretend that remote server created a new process
            SendPacket(stream, "0");
        } else {
            // Send an empty reply
            SendPacket(stream, "");
        }

        break;
    }

    case 'v':
    {
        if (MatchesSubcmd("Cont?")) {
            // Return list of supported vCond actions
            SendPacket(stream, "vCont;c;s");
        } else if (MatchesSubcmd("Cont")) {
            // TODO: Make sure command[5] == ';'
            // Same as "c", "s", ...
            if (command[6] == 'c') {
                std::cerr << "Stub" << GetCPUId() << " continuing.." << std::endl;
                Continue(stream);
            } else if (command[6] == 's') {
                Continue(stream, true);
            }
        } else if (MatchesSubcmd("Attach")) {
            ReportSignal(stream, 5);
        }
        break;
    }

    case 'z':
    case 'Z':
    {
        // Add breakpoint at the given address
        // Zn,addr,kind
        // n = 0: memory breakpoint (replace instruction at given address with SIGTRAP), kind = size of the breakpoint instruction in bytes
        // n = 1: hardware breakpoint, kind = same as memory breakpoint
        // n = 2: write watchpoint, kind = number of bytes to watch
        // n = 3: read watchpoint, kind = same as write watchpoints
        // n = 4: access watchpoint, kind = same as write watchpoints

        size_t start = 1;
        uint32_t type;
        uint32_t addr;
        uint32_t length;

        // Read type
        auto hexnum = ReadHexNumber(command | boost::adaptors::sliced(start, command.length()));
        if (hexnum) {
            type = hexnum->first;
            start += hexnum->second;
        }
        if (!hexnum || command.length() < start || command[start] != ',') {
            // Malformed request, error
            SendPacket(stream, "E xx");
            break;
        }

        // Skip comma
        ++start;

        // Read address
        hexnum = ReadHexNumber(command | boost::adaptors::sliced(start, command.length()));
        if (hexnum) {
            addr = hexnum->first;
            start += hexnum->second;
        }
        if (!hexnum || command.length() < start || command[start] != ',') {
            // Malformed request, error
            SendPacket(stream, "E xx");
            break;
        }

        // Skip comma
        ++start;

        // Read length
        hexnum = ReadHexNumber(command | boost::adaptors::sliced(start, command.length()));
        if (hexnum) {
            length = hexnum->first;
        } else {
            // Malformed request, error
            SendPacket(stream, "E xx");
            break;
        }
        // Only accept hardware breakpoints
        if (type != 1) {
            // Empty reply = breakpoint type not supported
            SendPacket(stream, "");
            break;
        }

        if (command[0] == 'Z') {
            // Add breakpoint
            RequestAddBreakpoint({addr});
            std::cerr << "Added breakpoint " << std::hex << addr << std::endl;
        } else {
            // Remove breakpoint
            RequestRemoveBreakpoint({addr});
            std::cerr << "Removed breakpoint " << std::hex << addr << std::endl;
        }

        SendPacket(stream, "OK");
        break;
    }

    default:
        std::cerr << "Stub" << GetCPUId() << " received unknown command: \"" << command << "\"" << std::endl;
        SendPacket(stream, "E xx");
    }

    return true;
}

unsigned GDBStub::GetCPUId() {
    return ctx.cpu.cp15.CPUId().CPUID;
}

void GDBStub::Run() {
    for (;;) {
        WaitForNextCommand(stream);
        std::string command;
        std::getline(stream, command, '#');

        std::cerr << "Stub" << std::dec << GetCPUId() << " command received: " << command << std::endl;

        unsigned char expected_checksum = (*HexCharToInt(stream.get()) << 4);
        expected_checksum |= *HexCharToInt(stream.get());
        if (expected_checksum != GetChecksum(command)) {
            std::cerr << "Stub" << GetCPUId() << " checksum mismatch! Expected " << std::hex << std::setfill('0') << (unsigned)expected_checksum << ", got " << (unsigned)GetChecksum(command) << std::endl;
            NackReply(stream);
        } else {
            std::cerr << "Stub" << GetCPUId() << " sends ACK" << std::endl;
            AckReply(stream);
        }

        if (HandlePacket(stream, command))
            break;
    }
}

void GDBStub::ProcessQueue() {
    //if (stream.rdbuf()->available() != 0)
    //    std::cerr << "Stub" << GetCPUId() << " processing queue" << std::endl;

    for (;;) {
        // Continue if no data is available
        //if (stream.rdbuf()->available() == 0)
        //    break;

        WaitForNextCommand(stream);
        std::string command;
        std::getline(stream, command, '#');

        std::cerr << "Stub" << std::dec << GetCPUId() << " command received: " << command << std::endl;

        unsigned char expected_checksum = (*HexCharToInt(stream.get()) << 4);
        expected_checksum |= *HexCharToInt(stream.get());
        if (expected_checksum != GetChecksum(command)) {
            std::cerr << "Stub" << GetCPUId() << " checksum mismatch! Expected " << std::hex << std::setfill('0') << (unsigned)expected_checksum << ", got " << (unsigned)GetChecksum(command) << std::endl;
            NackReply(stream);
        } else {
            std::cerr << "Stub" << GetCPUId() << " sends ACK" << std::endl;
            AckReply(stream);
        }

        if (HandlePacket(stream, command) && command[0] == 's')
            break;
    }
}

void GDBStub::RequestAddBreakpoint(const Breakpoint& bp) const {
    ctx.setup->breakpoints.push_back(bp);
}

void GDBStub::RequestRemoveBreakpoint(const Breakpoint& bp) const {
    ctx.setup->breakpoints.remove(bp);
}

void GDBStub::OnSegfault() {
    ReportSignal(stream, 6);
    process_event_queue_triggered_signal = true;
}

void GDBStub::OnBreakpoint() {
    ReportSignal(stream, 5);
    process_event_queue_triggered_signal = true;
}

} // namespace
