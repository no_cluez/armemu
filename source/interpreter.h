#pragma once

#include "arm.h"
#include "memory.h"

#include <atomic>
#include <cstring>
#include <list>
#include <iostream>
#include <map>

namespace Interpreter {

struct Breakpoint {
    uint32_t address;

    bool operator==(const Breakpoint& bp) const {
        return address == bp.address;
    }
};

// Controls thread-safe stepping/running/pausing/quitting of a Processor
class ProcessorController {
    // Handlers called from ProcessEventQueue when a segfault has been reported
    virtual void OnSegfault() = 0;
    virtual void OnBreakpoint() = 0;

protected:
    std::atomic<bool> request_pause{false};
    std::atomic<bool> paused{false};
    std::atomic<bool> request_continue{false};

    friend class Processor;

    enum class Event {
        Segfault,
        Breakpoint
    };

    std::list<Event> events;

    void ProcessEventQueue() {
        while (!events.empty()) {
            auto& event = events.front();
            switch (event) {
            case Event::Segfault:
                OnSegfault();
                break;

            case Event::Breakpoint:
                OnBreakpoint();
                break;

            default:
                // Do nothing
                ;
            }

            events.pop_front();
        }
    }

public:
    // TODO: This is probably a mess and full of race conditions... :(

    // TODO: Need functions to signal quit requests

    /**
     * Request the emulation core to be paused and busy-waits until the request has been fulfilled.
     * @note The amount of CPU instructions processed between the request of the pause and the actual pausing is undefined.
     */
    void RequestPause() {
        request_pause = true;

        while (!paused) {
        }

        request_pause = false;
    }

    // Request emulation to be continued without any planned pause in the future. Does not return before emulation has continued.
    void RequestContinue() {
        request_continue = true;
        while (paused) {};
        request_continue = false;
    }

    // Request emulation to be continued and pause after the next instruction. Does not return before emulation has continued.
    void RequestStep() {
        request_pause = true;
        request_continue = true;
        while (paused) {};
        request_continue = false;
    }

    /**
     * Request the emulator to register the given breakpoint
     * @todo Can we un-virtual-ize this function?
     */
    virtual void RequestAddBreakpoint(const Breakpoint& bp) const = 0;

    /**
     * Request the emulator to unregister the given breakpoint
     * @todo Can we un-virtual-ize this function?
     */
    virtual void RequestRemoveBreakpoint(const Breakpoint& bp) const = 0;

    // Use this in the Processor when an internal (unrecoverable) error happens
    void NotifySegfault() {
        events.push_back(Event::Segfault);
    }

    void NotifyBreakpoint() {
        events.push_back(Event::Breakpoint);
    }
};

// Dummy ProcessorController that doesn't do anything
class DummyController : public ProcessorController {
    void OnSegfault() override;
    void OnBreakpoint() override;
    void RequestAddBreakpoint(const Breakpoint& bp) const override;
    void RequestRemoveBreakpoint(const Breakpoint& bp) const override;
};

}  // namespace Interpreter

// Stuff below this line may depend on os.hpp (while stuff below is needed for os.hpp
#include "os.hpp"

namespace Interpreter {

struct CPUContext {
    ARM::State cpu;

    /**
     * Fake VAddr->PAddr translation map.
     * Maps the beginning of a vritual address range to a pair of the physical address and its size.
     * @todo Implement proper MMU emulation instead.
     */
    struct PhysicalMemoryRange {
        uint32_t start;
        uint32_t size;  // in bytes
    };
    std::map<uint32_t, PhysicalMemoryRange> fake_addr_map;

    PhysicalMemory* mem;

    HLE::OS::OS* os;

    Setup* setup;
};

struct Setup {
    Setup() : cpus {
                {{}, {}, &mem},
                {{}, {}, &mem}} {

        for (auto i : {0,1}) {
            std::memset(&cpus[i].cpu, 0, sizeof(cpus[i].cpu));
            cpus[i].cpu.cp15.CPUId().CPUID = i;
            cpus[i].cpu.cpsr.mode = ARM::InternalProcessorMode::Supervisor;
            cpus[i].mem = &mem;
            cpus[i].os = &os;
            cpus[i].setup = this;
        }
    }

    CPUContext cpus[2];

    PhysicalMemory mem;

    HLE::OS::OS os;

    std::list<Breakpoint> breakpoints;
};

class TranslatorContext;

class Processor {
public:
    std::shared_ptr<Setup> setup;

    template<typename T>
    static void WritePhysicalMemory(CPUContext& ctx, uint32_t phys_address, const T value);

    template<typename T>
    static const T ReadPhysicalMemory(CPUContext& ctx, uint32_t phys_address);

    template<typename T>
    static void WriteVirtualMemory(CPUContext& ctx, uint32_t virt_address, const T value);

    template<typename T>
    static const T ReadVirtualMemory(CPUContext& ctx, uint32_t virt_address);

    // Translate the given virtual address to a physical one, if possible.
    static boost::optional<uint32_t> TranslateVirtualAddress(CPUContext& ctx, uint32_t vaddr);

    static void TranslateBlock(TranslatorContext& trans_ctx, CPUContext& ctx, uint32_t instr_offset);

    // Runs until shut down by controller
    static void Run(CPUContext& ctx, ProcessorController& controller);
};

}  // namespace Interpreter
