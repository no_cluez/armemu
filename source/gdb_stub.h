#pragma once

#include "interpreter.h"

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>

#include <set>
#include <string>

namespace std {
class thread;
}

namespace Interpreter {

class GDBStub final : public ProcessorController {
    CPUContext& ctx;

    boost::asio::io_service io_service;
    boost::asio::ip::tcp::acceptor acceptor;
    boost::asio::ip::tcp::iostream stream;

    std::unique_ptr<std::thread> stubthread;
    /**
     * Handle an incoming GDB command
     * @return true if we should give back control to the caller
     *
     */
    bool HandlePacket(boost::asio::ip::tcp::iostream& stream, const std::string& command);

    void WaitForNextCommand(boost::asio::ip::tcp::iostream& stream);

    void SendPacket(boost::asio::ip::tcp::iostream& stream_, const std::string& message);

    void ReportSignal(boost::asio::ip::tcp::iostream& stream, int signum);

    void Continue(boost::asio::ip::tcp::iostream& stream, bool single_step = false);

    /**
     * Get CPU Id of the controlled CPU
     */
    unsigned GetCPUId();

    void RequestAddBreakpoint(const Breakpoint& bp) const override;
    void RequestRemoveBreakpoint(const Breakpoint& bp) const override;

    void OnSegfault() override;
    void OnBreakpoint() override;

public:
    GDBStub(CPUContext& ctx, unsigned port);

    void Run();
    void ProcessQueue();
};

} // namespace
