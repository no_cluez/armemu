#include "ir.hpp"
#include "interpreter.h"

#include <array>

namespace IR {

const uint8_t scratch_regs[] = {
    16, 17, 18, 19, 20, 21, 22, 23,
    24, 25, 26, 27, 28, 29, 30, 31
};

struct HandlerTable;

using HandlerReturnType = void;

// TODO: Consider adding noexcept!
using HandlerFunc = std::add_pointer<HandlerReturnType(Interpreter::CPUContext&, const HandlerTable&, Instruction*)>::type;

struct HandlerTable {
    // TODO: Instead, have a constructor set up this table, since it will never change anyway!
    std::array<HandlerFunc, num_ops> handlers;

    HandlerFunc& operator [](const Op op) {
        return handlers[static_cast<uint16_t>(op)];
    }

    const HandlerFunc& operator [](const Op op) const {
        return handlers[static_cast<uint16_t>(op)];
    }
};

uint8_t GetNativeReg(uint8_t index) {
    // Use the first 16 internal registers to store the guest CPU's registers.
    return index;
}

uint8_t GetCPSR_Z() {
    return 32;
}

uint8_t GetCPSR_N() {
    return 33;
}

uint8_t GetCPSR_C() {
    return 34;
}

uint8_t GetCPSR_V() {
    return 35;
}

uint8_t GetThumb() {
    return 36;
}

template<Op op>
static HandlerReturnType Handler(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr);

// Utility function for invoking the next instruction handler
static HandlerReturnType NextInstr(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    ++instr;
    handlers[instr->format1.op](ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::Invalid>(Interpreter::CPUContext&, const HandlerTable&, Instruction*) {
    throw std::runtime_error("Unknown handler!");
}

template<>
HandlerReturnType Handler<Op::Return>(Interpreter::CPUContext&, const HandlerTable&, Instruction*) {
    // Do nothing, just return.
}

template<>
HandlerReturnType Handler<Op::ShiftLeftImm>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    ctx.cpu.reg[instr->format1.dst_reg] = ctx.cpu.reg[instr->format1.src1_reg] << instr->format1.immediate;
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::ShiftRightImm>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    ctx.cpu.reg[instr->format1.dst_reg] = ctx.cpu.reg[instr->format1.src1_reg] >> instr->format1.immediate;
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::ShiftLeft>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    ctx.cpu.reg[instr->format1.dst_reg] = ctx.cpu.reg[instr->format1.src1_reg] << ctx.cpu.reg[instr->format1.src2_reg];
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::ShiftRight>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    ctx.cpu.reg[instr->format1.dst_reg] = ctx.cpu.reg[instr->format1.src1_reg] >> ctx.cpu.reg[instr->format1.src2_reg];
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::BitwiseOr>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    ctx.cpu.reg[instr->format1.dst_reg] = ctx.cpu.reg[instr->format1.src1_reg] | ctx.cpu.reg[instr->format1.src2_reg];
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::BitwiseAnd>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    ctx.cpu.reg[instr->format1.dst_reg] = ctx.cpu.reg[instr->format1.src1_reg] & ctx.cpu.reg[instr->format1.src2_reg];
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::BitwiseXor>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    ctx.cpu.reg[instr->format1.dst_reg] = ctx.cpu.reg[instr->format1.src1_reg] ^ ctx.cpu.reg[instr->format1.src2_reg];
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::BitwiseNeg>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    ctx.cpu.reg[instr->format1.dst_reg] = ~ctx.cpu.reg[instr->format1.src1_reg];
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::EqualsImm>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    ctx.cpu.reg[instr->format1.dst_reg] = (ctx.cpu.reg[instr->format1.src1_reg] == instr->format1.immediate);
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::LessThanImm>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    ctx.cpu.reg[instr->format1.dst_reg] = (ctx.cpu.reg[instr->format1.src1_reg] < instr->format1.immediate);
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::Equal>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    ctx.cpu.reg[instr->format1.dst_reg] = (ctx.cpu.reg[instr->format1.src1_reg] == ctx.cpu.reg[instr->format1.src2_reg]);
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::NotEqual>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    ctx.cpu.reg[instr->format1.dst_reg] = (ctx.cpu.reg[instr->format1.src1_reg] != ctx.cpu.reg[instr->format1.src2_reg]);
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::GreaterEqual>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    ctx.cpu.reg[instr->format1.dst_reg] = (ctx.cpu.reg[instr->format1.src1_reg] >= ctx.cpu.reg[instr->format1.src2_reg]);
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::Mov>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    ctx.cpu.reg[instr->format1.dst_reg] = ctx.cpu.reg[instr->format1.src1_reg];
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::Add>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    ctx.cpu.reg[instr->format1.dst_reg] = ctx.cpu.reg[instr->format1.src1_reg] + ctx.cpu.reg[instr->format1.src2_reg];
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::Sub>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    ctx.cpu.reg[instr->format1.dst_reg] = ctx.cpu.reg[instr->format1.src1_reg] - ctx.cpu.reg[instr->format1.src2_reg];
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::Mul>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    ctx.cpu.reg[instr->format1.dst_reg] = ctx.cpu.reg[instr->format1.src1_reg] * ctx.cpu.reg[instr->format1.src2_reg];
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::MulTo64>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    auto result = static_cast<uint64_t>(ctx.cpu.reg[instr->format1.src1_reg]) * static_cast<uint64_t>(ctx.cpu.reg[instr->format1.src2_reg]);
    ctx.cpu.reg[instr->format1.dst_reg] = result & 0xFFFFFFFF;
    ctx.cpu.reg[instr->format1.src3_reg] = result >> 32;
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::SignedMulTo64>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    auto operand1 = static_cast<uint64_t>(static_cast<int32_t>(ctx.cpu.reg[instr->format1.src1_reg]));
    auto operand2 = static_cast<uint64_t>(static_cast<int32_t>(ctx.cpu.reg[instr->format1.src2_reg]));
    auto result = operand1 * operand2;
    ctx.cpu.reg[instr->format1.dst_reg] = result & 0xFFFFFFFF;
    ctx.cpu.reg[instr->format1.src3_reg] = result >> 32;
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::Load8>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    ctx.cpu.reg[instr->format1.dst_reg] = Interpreter::Processor::ReadVirtualMemory<uint8_t>(ctx, ctx.cpu.reg[instr->format1.src1_reg]);
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::Load16>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    ctx.cpu.reg[instr->format1.dst_reg] = Interpreter::Processor::ReadVirtualMemory<uint16_t>(ctx, ctx.cpu.reg[instr->format1.src1_reg]);
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::Load32>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    ctx.cpu.reg[instr->format1.dst_reg] = Interpreter::Processor::ReadVirtualMemory<uint32_t>(ctx, ctx.cpu.reg[instr->format1.src1_reg]);
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::Store8>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    Interpreter::Processor::WriteVirtualMemory<uint8_t>(ctx, ctx.cpu.reg[instr->format1.src1_reg], ctx.cpu.reg[instr->format1.src2_reg]);
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::Store16>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    Interpreter::Processor::WriteVirtualMemory<uint16_t>(ctx, ctx.cpu.reg[instr->format1.src1_reg], ctx.cpu.reg[instr->format1.src2_reg]);
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::Store32>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    Interpreter::Processor::WriteVirtualMemory<uint32_t>(ctx, ctx.cpu.reg[instr->format1.src1_reg], ctx.cpu.reg[instr->format1.src2_reg]);
    NextInstr(ctx, handlers, instr);
}

template<>
HandlerReturnType Handler<Op::Skip>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    NextInstr(ctx, handlers, instr + instr->format2.immediate32);
}

template<>
HandlerReturnType Handler<Op::If>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    if (ctx.cpu.reg[instr->format2.dst_reg]) {
        NextInstr(ctx, handlers, instr);
    } else {
        NextInstr(ctx, handlers, instr + instr->format2.immediate32);
    }
}

template<>
HandlerReturnType Handler<Op::LoadImm32>(Interpreter::CPUContext& ctx, const HandlerTable& handlers, Instruction* instr) {
    ctx.cpu.reg[instr->format2.dst_reg] = instr->format2.immediate32;
    NextInstr(ctx, handlers, instr);
}

static HandlerTable handlers = [] {
    HandlerTable ret;
    std::fill(std::begin(ret.handlers), std::end(ret.handlers), Handler<Op::Invalid>);

    ret[Op::Invalid] = Handler<Op::Invalid>;
//    ret[Op::Decode] = Handler<Op::Decode>;
    ret[Op::Return] = Handler<Op::Return>;
    ret[Op::ShiftRightImm] = Handler<Op::ShiftRightImm>;
    ret[Op::ShiftLeftImm] = Handler<Op::ShiftLeftImm>;
    ret[Op::ShiftLeft] = Handler<Op::ShiftLeft>;
    ret[Op::ShiftRight] = Handler<Op::ShiftRight>;
//    ret[Op::RotateRightImm] = Handler<Op::RotateRightImm>;
    ret[Op::BitwiseOr] = Handler<Op::BitwiseOr>;
    ret[Op::BitwiseAnd] = Handler<Op::BitwiseAnd>;
    ret[Op::BitwiseXor] = Handler<Op::BitwiseXor>;
    ret[Op::BitwiseNeg] = Handler<Op::BitwiseNeg>;
    ret[Op::EqualsImm] = Handler<Op::EqualsImm>;
    ret[Op::LessThanImm] = Handler<Op::LessThanImm>;
    ret[Op::Equal] = Handler<Op::Equal>;
    ret[Op::NotEqual] = Handler<Op::NotEqual>;
    ret[Op::GreaterEqual] = Handler<Op::GreaterEqual>;
    ret[Op::Mov] = Handler<Op::Mov>;
    ret[Op::Add] = Handler<Op::Add>;
    ret[Op::Sub] = Handler<Op::Sub>;
    ret[Op::Mul] = Handler<Op::Mul>;
    ret[Op::MulTo64] = Handler<Op::MulTo64>;
    ret[Op::SignedMulTo64] = Handler<Op::SignedMulTo64>;

    ret[Op::Load8] = Handler<Op::Load8>;
    ret[Op::Load16] = Handler<Op::Load16>;
    ret[Op::Load32] = Handler<Op::Load32>;
    ret[Op::Store8] = Handler<Op::Store8>;
    ret[Op::Store16] = Handler<Op::Store16>;
    ret[Op::Store32] = Handler<Op::Store32>;
    ret[Op::Skip] = Handler<Op::Skip>;
    ret[Op::If] = Handler<Op::If>;
    ret[Op::LoadImm32] = Handler<Op::LoadImm32>;

    return ret;
}();

void Block::Execute(Interpreter::CPUContext& ctx) {
    if (instructions.empty())
        return;

    // handlers should probably be a member of CPUContext
    handlers[instructions.front().format1.op](ctx, handlers, instructions.data());
}

void Block::Clear() {
    instructions.clear();
}

size_t Block::GetNumInstructions() {
    return instructions.size();
}

static Instruction MakeFormat1(Op op,  uint8_t dst,  uint8_t src1, uint8_t src2 = 0,  uint8_t src3 = 0,  uint8_t imm = 0) {
    Instruction instr;
    instr.format1.op = op;
    instr.format1.dst_reg = dst;
    instr.format1.src1_reg = src1;
    instr.format1.src2_reg = src2;
    instr.format1.src3_reg = src3;
    instr.format1.immediate = imm;
    return instr;
}

static Instruction MakeFormat2(Op op,  uint8_t dst,  uint32_t imm) {
    Instruction instr;
    instr.format2.op = op;
    instr.format2.dst_reg = dst;
    instr.format2.immediate32 = imm;
    return instr;
}

void Block::GenMov(uint8_t dst, uint8_t src) {
    instructions.emplace_back(MakeFormat1(Op::Mov,  dst,  src));
}

void Block::GenAdd(uint8_t dst, uint8_t src1, uint8_t src2) {
    instructions.emplace_back(MakeFormat1(Op::Add,  dst,  src1,  src2));
}

void Block::GenSub(uint8_t dst, uint8_t src1, uint8_t src2) {
    instructions.emplace_back(MakeFormat1(Op::Sub,  dst,  src1,  src2));
}

void Block::GenMul(RegIndex dst, RegIndex src1, RegIndex src2) {
    instructions.emplace_back(MakeFormat1(Op::Mul, dst, src1, src2));
}

void Block::GenMulTo64(RegIndex dst_low, RegIndex dst_high, RegIndex src1, RegIndex src2) {
    instructions.emplace_back(MakeFormat1(Op::MulTo64, dst_low, src1, src2, dst_high));
}

void Block::GenSignedMulTo64(RegIndex dst_low, RegIndex dst_high, RegIndex src1, RegIndex src2) {
    instructions.emplace_back(MakeFormat1(Op::SignedMulTo64, dst_low, src1, src2, dst_high));
}

void Block::GenBitwiseOr(uint8_t dst, uint8_t src1, uint8_t src2) {
    instructions.emplace_back(MakeFormat1(Op::BitwiseOr,  dst,  src1,  src2));
}

void Block::GenBitwiseXor(uint8_t dst, uint8_t src1, uint8_t src2) {
    instructions.emplace_back(MakeFormat1(Op::BitwiseXor,  dst,  src1,  src2));
}

void Block::GenBitwiseAnd(uint8_t dst, uint8_t src1, uint8_t src2) {
    instructions.emplace_back(MakeFormat1(Op::BitwiseAnd,  dst,  src1,  src2));
}

void Block::GenBitwiseNeg(uint8_t dst, uint8_t src) {
    instructions.emplace_back(MakeFormat1(Op::BitwiseNeg,  dst,  src));
}

void Block::GenEqualsImm(uint8_t dst, uint8_t src1, uint8_t imm) {
    instructions.emplace_back(MakeFormat1(Op::EqualsImm,  dst,  src1,  0,  0,  imm));
}

void Block::GenLessThanImm(uint8_t dst, uint8_t src1, uint8_t imm) {
    instructions.emplace_back(MakeFormat1(Op::LessThanImm,  dst,  src1,  0,  0,  imm));
}

void Block::GenEqual(uint8_t dst, uint8_t src1, uint8_t src2) {
    instructions.emplace_back(MakeFormat1(Op::Equal,  dst,  src1,  src2));
}

void Block::GenNotEqual(uint8_t dst, uint8_t src1, uint8_t src2) {
    instructions.emplace_back(MakeFormat1(Op::NotEqual,  dst,  src1,  src2));
}

void Block::GenGreaterEqual(uint8_t dst, uint8_t src1, uint8_t src2) {
    instructions.emplace_back(MakeFormat1(Op::GreaterEqual,  dst,  src1,  src2));
}

void Block::GenShiftLeftImm(uint8_t dst, uint8_t src1, uint8_t imm) {
    instructions.emplace_back(MakeFormat1(Op::ShiftLeftImm, dst, src1, 0, 0, imm));
}

void Block::GenShiftRightImm(uint8_t dst, uint8_t src1, uint8_t imm) {
    instructions.emplace_back(MakeFormat1(Op::ShiftRightImm, dst, src1, 0, 0, imm));
}

void Block::GenShiftLeft(uint8_t dst, uint8_t value_src, uint8_t shift_src) {
    instructions.emplace_back(MakeFormat1(Op::ShiftLeft, dst, value_src, shift_src));
}

void Block::GenShiftRight(uint8_t dst, uint8_t value_src, uint8_t shift_src) {
    instructions.emplace_back(MakeFormat1(Op::ShiftRight, dst, value_src, shift_src));
}

void Block::GenLoad8(uint8_t dst, uint8_t addr) {
    instructions.emplace_back(MakeFormat1(Op::Load8, dst, addr));
}

void Block::GenLoad16(uint8_t dst, uint8_t addr) {
    instructions.emplace_back(MakeFormat1(Op::Load16, dst, addr));
}

void Block::GenLoad32(uint8_t dst, uint8_t addr) {
    instructions.emplace_back(MakeFormat1(Op::Load32, dst, addr));
}

void Block::GenStore8(uint8_t addr, uint8_t src) {
    instructions.emplace_back(MakeFormat1(Op::Store8, 0, addr, src));
}

void Block::GenStore16(uint8_t addr, uint8_t src) {
    instructions.emplace_back(MakeFormat1(Op::Store16, 0, addr, src));
}

void Block::GenStore32(uint8_t addr, uint8_t src) {
    instructions.emplace_back(MakeFormat1(Op::Store32, 0, addr, src));
}

IfBlock Block::GenIf(uint8_t predicate_reg, uint32_t num_instrs) {
    instructions.emplace_back(MakeFormat2(Op::If, predicate_reg, num_instrs));
    return { static_cast<unsigned>(instructions.size() - 1) };
}

void Block::EndIf(IfBlock block) {
    // Update the number of instructions in the If-instruction
    instructions[block.index].format2.immediate32 = instructions.size() - 1 - block.index;
}


void Block::GenSkip(uint32_t num_instrs) {
    instructions.emplace_back(MakeFormat2(Op::Skip, 0, num_instrs));
}

void Block::GenLoadImm32(uint8_t dst, uint32_t imm) {
    instructions.emplace_back(MakeFormat2(Op::LoadImm32,  dst,  imm));
}


}  // namespace IR
