#pragma once

#include <istream>

namespace Loader {

/**
 * Returns true if the given stream contains a file that is compatible with the 3DS emulator engine
 * @note The stream read cursor is restored when this function returns
 */
bool IsSupportedElf(std::istream& str);

}  // namespace Loader
