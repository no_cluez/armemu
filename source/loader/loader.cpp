#include "elf.hpp"
#include "firm.hpp"
#include "loader.hpp"

namespace Loader {

File GetType(std::istream& str) {
    if (IsSupportedElf(str)) {
        return File::Elf;
    } else if (IsFirm(str)) {
        return File::Elf;
    } else {
        return File::Unknown;
    }
}

}  // namespace Loader
