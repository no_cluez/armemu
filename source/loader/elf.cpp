#include "elf.hpp"

#include <elfio/elfio.hpp>

namespace Loader {

bool IsSupportedElf(std::istream& str) {
    using namespace ELFIO;

    auto file_begin = str.tellg();

    // Read raw header
    unsigned char e_ident[EI_NIDENT];
    str.read(reinterpret_cast<char*>(e_ident), sizeof(e_ident));
    str.seekg(file_begin);

    // Check whether it is an ELF file
    if (str.gcount() != sizeof(e_ident) ||
        e_ident[EI_MAG0] != ELFMAG0 ||
        e_ident[EI_MAG1] != ELFMAG1 ||
        e_ident[EI_MAG2] != ELFMAG2 ||
        e_ident[EI_MAG3] != ELFMAG3) {
        return false;
    }

    // Make sure it's a 32-bit ELF file (which is the only one that should be used for the 3DS)
    if (e_ident[EI_CLASS] != ELFCLASS32)
        return false;

    // Initialize endianness converter and load header properly
    endianess_convertor converter;
    converter.setup(e_ident[EI_DATA]);

    elf_header_impl<Elf32_Ehdr> header(&converter, e_ident[EI_DATA]);
    bool success = header.load(str);
    str.seekg(file_begin);
    if (!success)
        return false;

    // Make sure it's actually an ARM binary
    if (header.get_machine() != EM_ARM)
        return false;

    // Seems to be fine. There's not much we could check anymore without trying to load the whole file!
    return true;
}

}  // namespace Loader
