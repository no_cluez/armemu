#pragma once

#include <istream>

/**
 * \file
 * Interface for loading program data into the emulated guest
 */

namespace Interpreter {
struct Setup;
}

namespace Loader {

enum class File {
    Elf,
    Firm,   // 3DS firmware
    Unknown
};

/**
 * Get file type described by the given istream.
 * @note Implementations of this function need not verify the file in any way other than making sure the first few bytes are valid for the returned file type.
 * @note The stream read position offset is restored when this function returns.
 */
File GetType(std::istream& str);

}  // namespace Loader
