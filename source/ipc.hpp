#pragma once

#include "bit_field.h"

#include <cstdint>

namespace HLE {

namespace IPC {

union CommandHeader {
    uint32_t raw;

    BitField< 0,  6, uint32_t> size_translate_params; // in words
    BitField< 6,  6, uint32_t> num_normal_params;
    BitField<16, 16, uint32_t> command_id;

    static CommandHeader Make(uint32_t command_id, uint32_t num_normal_params, uint32_t size_translate_params) {
        CommandHeader command = { 0 };
        command.command_id = command_id;
        command.num_normal_params = num_normal_params;
        command.size_translate_params = size_translate_params;
        return command;
    }
};

union TranslationDescriptor {
    uint32_t raw;

    enum Type : uint32_t {
        Handles = 0,
        StaticBuffer = 1,
    };

    BitField<1, 3, Type> type;

    union {
        // Close client handle
        BitField< 4, 1, uint32_t> close_handle;

        // Fill the following words with the process id
        BitField< 5, 1, uint32_t> fill_process_id;

        BitField<26, 6, uint32_t> num_handles_minus_one;

        uint32_t NumHandles() const {
            return num_handles_minus_one + 1;
        }
    } handles;

    union {
        // buffer id for the destination process to store data in
        BitField< 10,  4, uint32_t> id;

        // number of bytes to store in the destination process buffer
        BitField< 14, 18, uint32_t> size; // TODO: Maximal size unconfirmed
    } static_buffer;

    static TranslationDescriptor MakeHandles(uint32_t num_handles, bool close = false) {
        TranslationDescriptor desc = { 0 };
        desc.type = Type::Handles;
        desc.handles.num_handles_minus_one = num_handles - 1;
        desc.handles.close_handle = close;
        return desc;
    }

    static TranslationDescriptor MakeProcessHandle() {
        TranslationDescriptor desc = { 0 };
        desc.type = Type::Handles;
        desc.handles.fill_process_id = true;
        return desc;
    }

    static TranslationDescriptor MakeStaticBuffer(uint32_t id, uint32_t num_bytes) {
        TranslationDescriptor desc = { 0 };
        desc.type = Type::StaticBuffer;
        desc.static_buffer.id = id;
        desc.static_buffer.size = num_bytes;
        return desc;
    }
};

}  // namespace IPC

}  // namespace HLE
