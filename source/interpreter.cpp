#include "interpreter.h"
#include "ir.hpp"

#include <boost/endian/buffers.hpp>
#include <boost/function_types/result_type.hpp>
#include <boost/optional.hpp>
#include <boost/range/size.hpp>

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <type_traits>

namespace Interpreter {

// Explicit instantiations
template void Processor::WritePhysicalMemory(CPUContext& ctx, uint32_t address, const uint8_t value);
template void Processor::WritePhysicalMemory(CPUContext& ctx, uint32_t address, const uint16_t value);
template void Processor::WritePhysicalMemory(CPUContext& ctx, uint32_t address, const uint32_t value);
template const uint8_t Processor::ReadPhysicalMemory(CPUContext& ctx, uint32_t address);
template const uint16_t Processor::ReadPhysicalMemory(CPUContext& ctx, uint32_t address);
template const uint32_t Processor::ReadPhysicalMemory(CPUContext& ctx, uint32_t address);

template void Processor::WriteVirtualMemory(CPUContext& ctx, uint32_t address, const uint8_t value);
template void Processor::WriteVirtualMemory(CPUContext& ctx, uint32_t address, const uint16_t value);
template void Processor::WriteVirtualMemory(CPUContext& ctx, uint32_t address, const uint32_t value);
template const uint8_t Processor::ReadVirtualMemory(CPUContext& ctx, uint32_t address);
template const uint16_t Processor::ReadVirtualMemory(CPUContext& ctx, uint32_t address);
template const uint32_t Processor::ReadVirtualMemory(CPUContext& ctx, uint32_t address);

template<typename RawMemoryBlockType>
static constexpr bool IsInside(const RawMemoryBlockType& block, uint32_t address) {
    return address >= block.offset && address < block.offset + block.size;
}

template<typename RawMemoryBlockType, typename T>
static void Write(RawMemoryBlockType& block, uint32_t address, const T& value) {
    boost::endian::endian_buffer<boost::endian::order::little, T, sizeof(T)*8> val(value);
    std::memcpy(&block.data[address - block.offset], &val, sizeof(val));
}

template<typename T, typename RawMemoryBlockType>
static T ReadMemBlock(RawMemoryBlockType& block, uint32_t address) {
    boost::endian::endian_buffer<boost::endian::order::little, T, sizeof(T)*8> val;
    std::memcpy(&val, &block.data[address - block.offset], sizeof(val));
    return val.value();
}

template<typename T>
void Processor::WritePhysicalMemory(CPUContext& ctx, uint32_t address, const T value) {
    if (IsInside(ctx.mem->dsp, address)) {
        Write(ctx.mem->dsp, address, value);
    } else if (IsInside(ctx.mem->axi, address)) {
        Write(ctx.mem->axi, address, value);
    } else if (IsInside(ctx.mem->fcram, address)) {
        Write(ctx.mem->fcram, address, value);
    } else if (IsInside(ctx.mem->vram, address)) {
        Write(ctx.mem->vram, address, value);
    } else {
        std::stringstream ss;
        ss << "Write to unknown address 0x" << std::hex << std::setw(8) << std::setfill('0') << address << std::endl;
        throw std::runtime_error(ss.str());
    }
}

template<typename T>
const T Processor::ReadPhysicalMemory(CPUContext& ctx, uint32_t address) {
    if (IsInside(ctx.mem->dsp, address)) {
        return ReadMemBlock<T>(ctx.mem->dsp, address);
    } else if (IsInside(ctx.mem->axi, address)) {
        return ReadMemBlock<T>(ctx.mem->axi, address);
    } else if (IsInside(ctx.mem->fcram, address)) {
        return ReadMemBlock<T>(ctx.mem->fcram, address);
    } else if (IsInside(ctx.mem->vram, address)) {
        return ReadMemBlock<T>(ctx.mem->vram, address);
    } else if (IsInside(PhysicalMemory::mpcore_private{}, address)) {
        switch ((address - PhysicalMemory::mpcore_private{}.offset) & ~3) {
        case 0x4: // Configuration Register
            // Only return the (hardcoded) number of available CPUs (minus 1) for now
            return 1;

        default:
            throw std::runtime_error("Read from unknown address within MPCore private region");
        }
    } else {
       throw std::runtime_error("Read from unknown address");
//return 0;
    }
}

boost::optional<uint32_t> Processor::TranslateVirtualAddress(CPUContext& ctx, uint32_t vaddr) {
    // if MMU is disabled, virtual address = physical address
    if (!ctx.cpu.cp15.Control().M)
        return vaddr;

// Fake VAddr->PAddr translation code
#if 1
    auto it = ctx.fake_addr_map.rbegin();
    for (; it != ctx.fake_addr_map.rend(); ++it) { // TODO: Figure out if we can use some <algorithm> instead of this
        if (it->first <= vaddr)
            break;
    }
    if (it == ctx.fake_addr_map.rend()) {
        std::cerr << "Couldn't find any lower bound!" << std::endl;
        return {};
    }

    auto vaddr_start = it->first;
    auto paddr_range = it->second;

    if (vaddr - vaddr_start > paddr_range.size) {
        std::stringstream ss;
        ss << "Given virtual address 0x" << std::hex << std::setw(8) << std::setfill('0') << vaddr << " is not mapped!" << std::endl;
        throw std::runtime_error(ss.str());
        return {};
    }

    return paddr_range.start + (vaddr - vaddr_start);

#elif
    // TODO: Emulate a TLB! For now, we just always perform a full translation table walk. Yay!

    auto& ttbc = ctx.cpu.cp15.TranslationTableBaseControl();
    if (ttbc.N == 0 || address != ((address << ttbc.N) >> ttbc.N)) {
        // Use TTBR0
    } else {
        // Use TTBR1: upper N bits of "address" were all zero
    }

    if (ctx.cpu.cp15.Control().XP) {
        TODO: armv6 address translation
    } else {
        // subpage AP bits are enabled => backwards-compatible page table formats
        throw std::runtime_error("cp15.Control().XP=0 not supported, currently");
    }

    // if access to page table (cf. 1st level descriptor): fetch 2nd level descriptor

    // for both 1st and 2nd level descriptors: bits [0:1] zero => corresponding virtual addresses are unmapped => translation fault
#endif
}

template<typename T>
void Processor::WriteVirtualMemory(CPUContext& ctx, uint32_t address, const T value) {
    auto opt_paddr = TranslateVirtualAddress(ctx, address);
    if (!opt_paddr) {
        std::stringstream ss;
        ss << "Invalid virtual address 0x" << std::setw(8) << std::setfill('0') << address;
        throw std::runtime_error(ss.str());
    }

    WritePhysicalMemory(ctx, *opt_paddr, value);
}

template<typename T>
const T Processor::ReadVirtualMemory(CPUContext& ctx, uint32_t address) {
    auto opt_paddr = TranslateVirtualAddress(ctx, address);
    if (!opt_paddr) {
        std::stringstream ss;
        ss << "Invalid virtual address 0x" << std::setw(8) << std::setfill('0') << address;
        throw std::runtime_error(ss.str());
    }

    return ReadPhysicalMemory<T>(ctx, *opt_paddr);
}

using InterpreterARMHandler = std::add_pointer<uint32_t(CPUContext&, ARM::ARMInstr)>::type;

static uint32_t HandlerStubWithMessage(CPUContext& ctx, ARM::ARMInstr instr, const std::string& message) {
    std::cerr << "Unknown instruction 0x" << std::hex << std::setw(8) << std::setfill('0') << instr.raw;
    if (!message.empty())
        std::cerr << ": " << message;
    std::cerr << std::endl;

    throw std::runtime_error("Unknown instruction");
}

static uint32_t HandlerStubAnnotated(CPUContext& ctx, ARM::ARMInstr instr, unsigned line) {
    return HandlerStubWithMessage(ctx, instr, "(at line " + std::to_string(line) + ")");
}

static uint32_t NextInstr(CPUContext& ctx) {
    return ctx.cpu.PC() + 4;
}

// Copies the MSB of the given value to the CPSR N flag
static void UpdateCPSR_N(CPUContext& ctx, uint32_t val) {
    ctx.cpu.cpsr.neg = (val >> 31);
}

// Updates the CPSR Z flag with the contents of the given value (sets the flag if the value is zero, unsets it otherwise)
static void UpdateCPSR_Z(CPUContext& ctx, uint32_t val) {
    ctx.cpu.cpsr.zero = (val == 0);
}

static void TranslatorUpdateCPSR_N(IR::Block& block, uint8_t result_reg) {
    block.GenShiftRightImm(IR::GetCPSR_N(), result_reg, 31);
}

// Updates the CPSR Z flag with the contents of the given value (sets the flag if the value is zero, unsets it otherwise)
static void TranslatorUpdateCPSR_Z(IR::Block& block, uint8_t result_reg) {
    block.GenEqualsImm(IR::GetCPSR_Z(), result_reg, 0);
}

// Makes use of scratch registers 5 and 6.
static void TranslatorUpdateCPSR_C_FromCarry(IR::Block& block, uint8_t operand1_reg, uint8_t operand2_reg, uint8_t result_reg) {
    block.GenBitwiseOr(IR::scratch_regs[5], operand1_reg, operand2_reg);
    block.GenBitwiseNeg(IR::scratch_regs[6], result_reg);
    block.GenBitwiseAnd(IR::scratch_regs[5], IR::scratch_regs[5], IR::scratch_regs[6]);
    block.GenShiftRightImm(IR::GetCPSR_C(), IR::scratch_regs[5], 31);
}

static void TranslatorUpdateCPSR_C_FromBorrow(IR::Block& block, uint8_t operand1_reg, uint8_t operand2_reg) {
    block.GenGreaterEqual(IR::GetCPSR_C(), operand1_reg, operand2_reg);
}

// Makes use of scratch registers 5 and 6.
static void TranslatorUpdateCPSR_V_FromAdd(IR::Block& block, uint32_t operand1_reg, uint32_t operand2_reg, uint32_t result_reg) {
    // a = ~(left ^ right)
    block.GenBitwiseXor(IR::scratch_regs[5], operand1_reg, operand2_reg);
    block.GenBitwiseNeg(IR::scratch_regs[5], IR::scratch_regs[5]);

    // b = left ^ result
    block.GenBitwiseXor(IR::scratch_regs[6], operand1_reg, result_reg);

    // (a & b) >> 31
    block.GenBitwiseAnd(IR::scratch_regs[6], IR::scratch_regs[5], IR::scratch_regs[6]);
    block.GenShiftRightImm(IR::GetCPSR_V(), IR::scratch_regs[6], 31);
}

// Makes use of scratch registers 5 and 6.
static void TranslatorUpdateCPSR_V_FromSub(IR::Block& block, uint32_t operand1_reg, uint32_t operand2_reg, uint32_t result_reg) {
    // a = ~(left ^ ~right) = left ^ right
    block.GenBitwiseXor(IR::scratch_regs[5], operand1_reg, operand2_reg);

    // b = left ^ result
    block.GenBitwiseXor(IR::scratch_regs[6], operand1_reg, result_reg);

    // (a & b) >> 31
    block.GenBitwiseAnd(IR::scratch_regs[6], IR::scratch_regs[5], IR::scratch_regs[6]);
    block.GenShiftRightImm(IR::GetCPSR_V(), IR::scratch_regs[6], 31);
}

class TranslatorContext {
    // Each field specifies whether we need to initialize the corresponding
    // internal CPSR scratch register (in the order NZCV) before executing
    // this block.
public: // TODO: Should be private, too!
    std::array<bool, 4> cpsr_request_load = { false, false, false, false };

    // TODO: Privatize this one: Instruction offset of the guest instruction following this block
    uint32_t next_instr;
private:
    // Each field specifies whether the corresponding internal CPSR scratch
    // register (in the order NZCV) has been initialized yet (either before
    // or during executing this block).
    std::array<bool,4> cpsr_loaded = { false, false, false, false };

    // Each field specifies whether the corresponding internal CPSR scratch
    // register (in the order NZCV) has been written to and hence needs to be
    // mirrored to the guest CPU state after executing this block.
    std::array<bool,4> cpsr_dirty = { false, false, false, false };  // Have the internal CPSR scratch registers been written to? - N,Z,C,V

    // Signals that the generated code will modify the PC in a non-trivial way
    // (hence marking the end of the block)
    bool wrote_pc = false;

public:
    IR::Block block;

    // Makes sure the given CPSR condition flag (indexed in the order NZCV) is
    // loaded by the time the following IR code handlers are executed.
    // This may either happen by explicitly loading it before executing the
    // block or by executing an handler that updates the flags.
    void PrepareCPSR(unsigned flag_index) {
        if (cpsr_loaded[flag_index])
            return;

        cpsr_request_load[flag_index] = true;
    }

    void SetCPSRDirty(unsigned flag_index) {
        cpsr_dirty[flag_index] = true;
        cpsr_loaded[flag_index] = true;
    }

    void SetPCWritten() {
        wrote_pc = true;
    }

    bool IsCPSRDirty(unsigned flag_index) const {
        return cpsr_dirty[flag_index];
    }

    bool IsPCDirty() const {
        return wrote_pc;
    }
};

// Evaluates the given condition based on CPSR
static void TranslateEvalCond(TranslatorContext& ctx, CPUContext& ctx2, const BitField<28, 4, uint32_t>& cond, IR::RegIndex out_predicate) {
    if (cond == 0xE/* || cond == 0xF*/) { // always (0xF apparently is never?)
        ctx.block.GenLoadImm32(out_predicate, 1);
    } else if (cond == 0x0) { // Equal
        ctx.PrepareCPSR(1);
        ctx.block.GenEqualsImm(out_predicate, IR::GetCPSR_Z(), 1);
    } else if (cond == 0x1) { // Not Equal
        ctx.PrepareCPSR(1);
        ctx.block.GenEqualsImm(out_predicate, IR::GetCPSR_Z(), 0);
    } else if (cond == 0x2) { // Greater Equal (unsigned)
        ctx.PrepareCPSR(2);
        ctx.block.GenEqualsImm(out_predicate, IR::GetCPSR_C(), 1);
    } else if (cond == 0x3) { // Less Than (unsigned)
        ctx.PrepareCPSR(2);
        ctx.block.GenEqualsImm(out_predicate, IR::GetCPSR_C(), 0);
    } else if (cond == 0x4) { // Negative
        ctx.PrepareCPSR(0);
        ctx.block.GenEqualsImm(out_predicate, IR::GetCPSR_N(), 1);
    } else if (cond == 0x5) { // Positive or Zero
        ctx.PrepareCPSR(0);
        ctx.block.GenEqualsImm(out_predicate, IR::GetCPSR_N(), 0);
    } else if (cond == 0x8) { // Greater (unsigned)
        // C == 1 && Z == 0
        ctx.PrepareCPSR(1);
        ctx.PrepareCPSR(2);
        ctx.block.GenEqualsImm(out_predicate, IR::GetCPSR_Z(), 0);
        ctx.block.GenBitwiseAnd(out_predicate, out_predicate, IR::GetCPSR_C());
    } else if (cond == 0x9) { // Less Equal (unsigned)
        // C == 0 || Z == 1
        ctx.PrepareCPSR(1);
        ctx.PrepareCPSR(2);
        ctx.block.GenEqualsImm(out_predicate, IR::GetCPSR_C(), 0);
        ctx.block.GenBitwiseOr(out_predicate, out_predicate, IR::GetCPSR_Z());
    } else if (cond == 0xa) { // Greater Equal (signed)
        // N == V
        ctx.PrepareCPSR(0);
        ctx.PrepareCPSR(3);
        ctx.block.GenEqual(out_predicate, IR::GetCPSR_N(), IR::GetCPSR_V());
    } else if (cond == 0xb) { // Less Than (signed)
        // N != V
        ctx.PrepareCPSR(0);
        ctx.PrepareCPSR(3);
        ctx.block.GenNotEqual(out_predicate, IR::GetCPSR_N(), IR::GetCPSR_V());
    } else if (cond == 0xc) { // Greater Than (signed)
        // Z == 0 && N == V

        ctx.PrepareCPSR(0);
        ctx.PrepareCPSR(1);
        ctx.PrepareCPSR(3);
        // Hack to get around having to allocate a scratch register here..
        ctx.block.GenEqual(out_predicate, IR::GetCPSR_N(), IR::GetCPSR_V());
        ctx.block.GenBitwiseNeg(IR::GetCPSR_Z(), IR::GetCPSR_Z());
        ctx.block.GenBitwiseAnd(out_predicate, IR::GetCPSR_Z(), out_predicate);
        ctx.block.GenBitwiseNeg(IR::GetCPSR_Z(), IR::GetCPSR_Z());
    } else if (cond == 0xd) { // Less Equal (signed)
        // Z == 1 || N != V

        ctx.PrepareCPSR(0);
        ctx.PrepareCPSR(1);
        ctx.PrepareCPSR(3);
        ctx.block.GenNotEqual(out_predicate, IR::GetCPSR_N(), IR::GetCPSR_V());
        ctx.block.GenBitwiseOr(out_predicate, IR::GetCPSR_Z(), out_predicate);
    } else {
        throw std::runtime_error("Condition not implemented");
    }
}

static void TranslateLink(TranslatorContext& ctx, uint32_t instr_addr) {
    ctx.block.GenLoadImm32(IR::GetNativeReg(ARM::Regs::LR), instr_addr + 4);
}

static void TranslateBranch(TranslatorContext& ctx, ARM::ARMInstr instr, uint32_t instr_addr) {
    if (1 == ViewBitField<24, 1, uint32_t>(instr))
        TranslateLink(ctx, instr_addr);

     // TODO: Did we rely on signed overflow here? Casts have been made explicit now to make sure we don't run into this issue!
    uint32_t offset = instr_addr + 8u + static_cast<uint32_t>(4 * instr.branch_target.Value());
    ctx.block.GenLoadImm32(IR::GetNativeReg(ARM::Regs::PC), offset);
    ctx.SetPCWritten();
}

static void TranslateBranchExchange(TranslatorContext& ctx, ARM::ARMInstr instr, uint32_t instr_addr) {
    if (1 == ViewBitField<5, 1, uint32_t>(instr))
        TranslateLink(ctx, instr_addr);

    if (instr.idx_rm == ARM::Regs::PC)
        throw std::runtime_error("Unimplemented corner case");

    ctx.block.GenLoadImm32(IR::scratch_regs[0], 1);
    ctx.block.GenBitwiseAnd(IR::GetThumb(), IR::GetNativeReg(instr.idx_rm), IR::scratch_regs[0]);
    ctx.block.GenLoadImm32(IR::scratch_regs[0], 0xFFFFFFFE);
    ctx.block.GenBitwiseAnd(IR::GetNativeReg(ARM::Regs::PC), IR::GetNativeReg(instr.idx_rm), IR::scratch_regs[0]);
    ctx.SetPCWritten();
}

static uint32_t RotateRight(uint32_t value, uint32_t bits) {
    return (value >> bits) | (value << (32 - bits));
}

// Precondition: 0 < contents of shift_reg < 32
// Return value: Number of generated instructions (guaranteed to be 8, currently!)
static std::integral_constant<uint32_t, 8> TranslateArithmeticShift(TranslatorContext& ctx, uint8_t out_reg, uint8_t val_reg, uint8_t shift_reg, uint8_t scratch_reg1, uint8_t scratch_reg2) {
    // out = (val >> shift)
    ctx.block.GenShiftRight(out_reg, val_reg, shift_reg);

    // if (val >> 31), i.e. if negative then sign-extend output value
    ctx.block.GenShiftRightImm(scratch_reg1, val_reg, 31);
    auto block = ctx.block.GenIf(scratch_reg1, 0);
    {
        // out_reg |= (0xFFFFFFFF << (32 - bits));
        ctx.block.GenLoadImm32(scratch_reg1, 0xFFFFFFFF);
        ctx.block.GenLoadImm32(scratch_reg2, 32);
        ctx.block.GenSub(scratch_reg2, scratch_reg2, shift_reg);
        ctx.block.GenShiftLeft(scratch_reg2, scratch_reg1, scratch_reg2);
        ctx.block.GenBitwiseOr(out_reg, out_reg, scratch_reg2);
        ctx.block.EndIf(block);
    }

    return {};
}

// precondition: 0 < shift_value < 32
// Return value: Number of generated instructions (guaranteed to be 5, currently!)
static std::integral_constant<uint32_t, 5> TranslateRotateRight(TranslatorContext& ctx, IR::RegIndex result_reg, IR::RegIndex scratch_reg, IR::RegIndex input_reg, IR::RegIndex shift_value_reg) {
    // result = (input << (32-shift_value)) | (input >> shift_value)
    // TODO: We probably should introduce an IR instruction for this

    ctx.block.GenLoadImm32(scratch_reg, 32);
    ctx.block.GenSub(scratch_reg, scratch_reg, shift_value_reg);
    ctx.block.GenShiftLeft(scratch_reg, input_reg, scratch_reg);

    ctx.block.GenShiftRight(result_reg, input_reg, shift_value_reg);

    ctx.block.GenBitwiseOr(result_reg, result_reg, scratch_reg);

    return {};
}

// NOTE: For immediate shifts, use CalcShifterOperandFromImmediate instead!
// NOTE: ROR_RRX is always executed as a rotate in this function.
// NOTE: Only the least significant 8 bits of shift_value are considered
// NOTE: The caller assumes that the input shift value is in the range 0..255
// TODO: Provide a version of this function that is optimized for compile-time constants!
static void CalcShifterOperandNew(TranslatorContext& trans_ctx, IR::RegIndex value_reg, IR::RegIndex shift_value_reg, ARM::OperandShifterMode mode, IR::RegIndex outreg_value, IR::RegIndex outreg_carry, IR::RegIndex scratch1_reg, IR::RegIndex scratch2_reg) {
    // Mask out upper bits
    // shift_value &= 0xFF;

    switch (mode) {
    case ARM::OperandShifterMode::LSL:
    {
        // Make sure the input values are not the PC register
        assert(value_reg != 15);
        assert(shift_value_reg != 15);

        const int length_if_branch = 3;
        const int length_elseif_branch = 6;
        const int length_else_branch = 7;

        // if (shift_value == 0)
        trans_ctx.block.GenEqualsImm(outreg_value, shift_value_reg, 0);
        trans_ctx.block.GenIf(outreg_value, length_if_branch); // number of instructions in the if-branch
        {
            // result = value << shift_value = value
            trans_ctx.block.GenMov(outreg_value, value_reg);

            // carry_out = CPSR_C
            trans_ctx.PrepareCPSR(2); // Make sure CPSR_C is loaded
            trans_ctx.block.GenMov(outreg_carry, IR::GetCPSR_C());

            // Skip elseif condition check and the elseif/else branch bodies
            trans_ctx.block.GenSkip(2 + length_elseif_branch + length_else_branch);
        }
        // else if (shift_value < 32)
        trans_ctx.block.GenLessThanImm(outreg_value, shift_value_reg, 32);
        trans_ctx.block.GenIf(outreg_value, length_elseif_branch); // number of instructions in the if-branch
        {
            // result = value << shift_value
            trans_ctx.block.GenShiftLeft(outreg_value, value_reg, shift_value_reg);

            // carry = (value << (shift_value - 1)) >> 31
            trans_ctx.block.GenLoadImm32(outreg_carry, 1);
            trans_ctx.block.GenSub(outreg_carry, shift_value_reg, outreg_carry);
            trans_ctx.block.GenShiftLeft(outreg_carry, value_reg, outreg_carry);
            trans_ctx.block.GenShiftRightImm(outreg_carry, outreg_carry, 31);

            trans_ctx.block.GenSkip(length_else_branch); // number of instructions in the else-branch
        }
        // else
        {
            // result = 0
            trans_ctx.block.GenLoadImm32(outreg_value, 0);

            // if (shift_value == 32) carry = value & 1 (special case of the carry computation in the "<32" branch)
            trans_ctx.block.GenEqualsImm(outreg_carry, shift_value_reg, 32);
            trans_ctx.block.GenIf(outreg_carry, 3); // number of instructions in the if-branch
            {
                trans_ctx.block.GenLoadImm32(outreg_carry, 1);
                trans_ctx.block.GenBitwiseAnd(outreg_carry, value_reg, outreg_carry);
                trans_ctx.block.GenSkip(1);
            }
            // else carry = 0
            {
                trans_ctx.block.GenLoadImm32(outreg_carry, 0);
            }
        }
        break;
    }

    case ARM::OperandShifterMode::LSR:
    {
        const int length_if_branch = 3;
        const int length_elseif_branch = 6;
        const int length_else_branch = 6;

        // if (shift_value == 0)
        trans_ctx.block.GenEqualsImm(outreg_value, shift_value_reg, 0);
        trans_ctx.block.GenIf(outreg_value, length_if_branch);
        {
            // result = value >> shift_value = value
            trans_ctx.block.GenMov(outreg_value, value_reg);

            // carry_out = CPSR_C
            trans_ctx.PrepareCPSR(2); // Make sure CPSR_C is loaded
            trans_ctx.block.GenMov(outreg_carry, IR::GetCPSR_C());

            // Skip elseif condition check and the elseif/else branch bodies
            trans_ctx.block.GenSkip(2 + length_elseif_branch + length_else_branch);
        }
        // else if (shift_value < 32)
        trans_ctx.block.GenLessThanImm(outreg_value, shift_value_reg, 32);
        trans_ctx.block.GenIf(outreg_value, length_elseif_branch);
        {
            // result = value >> shift_value
            trans_ctx.block.GenShiftRight(outreg_value, value_reg, shift_value_reg);

            // carry = (value >> (shift_value - 1)) & 1
            trans_ctx.block.GenLoadImm32(scratch1_reg, 1);
            trans_ctx.block.GenSub(outreg_carry, shift_value_reg, scratch1_reg);
            trans_ctx.block.GenShiftRight(outreg_carry, value_reg, outreg_carry);
            trans_ctx.block.GenBitwiseAnd(outreg_carry, outreg_carry, scratch1_reg);

            trans_ctx.block.GenSkip(length_else_branch); // number of instructions in the else-branch
        }
        // else
        {
            // result = 0
            trans_ctx.block.GenLoadImm32(outreg_value, 0);

            // if (shift_value_reg == 32) carry = (value >> 31) (special case of the computation in the "<32" branch)
            trans_ctx.block.GenEqualsImm(outreg_carry, shift_value_reg, 32);
            trans_ctx.block.GenIf(outreg_carry, 2); // number of instructions in the if-branch
            {
                trans_ctx.block.GenShiftRightImm(outreg_carry, value_reg, 31);
                trans_ctx.block.GenSkip(1);
            }
            // else carry = 0
            {
                trans_ctx.block.GenLoadImm32(outreg_carry, 0);
            }
        }
        break;
    }

    case ARM::OperandShifterMode::ASR:
    {
        // TODO: Get rid of this hardcoded instruction counting!
        const uint32_t if_instrs = 3;
        const uint32_t elseif_instrs = 5 + boost::function_types::result_type<decltype(TranslateArithmeticShift)>::type::value;
        const uint32_t else_instrs = 5;
        // if (shift_value == 0) {
        trans_ctx.block.GenEqualsImm(outreg_value, shift_value_reg, 0);
        trans_ctx.block.GenIf(outreg_value, if_instrs);
        {
            // result = value
            trans_ctx.block.GenMov(outreg_value, value_reg);

            // carry = CPSR_C
            trans_ctx.PrepareCPSR(2); // Make sure CPSR_C is loaded
            trans_ctx.block.GenMov(outreg_carry, IR::GetCPSR_C());

            // Count two instructions for the else-if branch condition evaluation.
            const uint32_t skipped_instrs = elseif_instrs + 2 + else_instrs;

            trans_ctx.block.GenSkip(skipped_instrs);
        }
        // } else if (shift_value < 32) {
        trans_ctx.block.GenLessThanImm(outreg_value, shift_value_reg, 32);
        trans_ctx.block.GenIf(outreg_value, elseif_instrs);
        {
            // result = ASR(value, shift_value)
            uint32_t instrs = 5;
            instrs += TranslateArithmeticShift(trans_ctx, outreg_value, value_reg, shift_value_reg, scratch1_reg, scratch2_reg).value;

            // carry = ASR(value, shift_value - 1) & 1 = (value >> (shift_value - 1)) & 1
            trans_ctx.block.GenLoadImm32(scratch1_reg, 1);
            trans_ctx.block.GenSub(outreg_carry, shift_value_reg, scratch1_reg);
            trans_ctx.block.GenShiftRight(outreg_carry, value_reg, outreg_carry);
            trans_ctx.block.GenBitwiseAnd(outreg_carry, outreg_carry, scratch1_reg);

            trans_ctx.block.GenSkip(else_instrs);

            assert(instrs == elseif_instrs);
        }
        // } else {
        {
            // Special case for shifts by 32 bit

            // carry = value >> 31
            trans_ctx.block.GenShiftRightImm(outreg_carry, value_reg, 31);

            // if (value >> 31), i.e. if value is negative,
            // TODO: This would probably be much shorter if we were just using (value >> 31) * 0xFFFFFFFF instead
            trans_ctx.block.GenIf(outreg_carry, 2);
            {
                // result = 0xFFFFFFFF (as per sign-extension)
                trans_ctx.block.GenLoadImm32(outreg_value, 0xFFFFFFFF);
                trans_ctx.block.GenSkip(1);
            }
            // else
            {
                // result = 0 (as per sign-extension)
                trans_ctx.block.GenLoadImm32(outreg_value, 0);
            }
        }
        break;
    }

    case ARM::OperandShifterMode::ROR_RRX:
    {
        // This mode only considers the least significant 5 bits in shift_value
        // The upper 3 bits are *only* considered for the carry computation here.
        trans_ctx.block.GenLoadImm32(scratch1_reg, 0x1F);
        trans_ctx.block.GenBitwiseAnd(scratch1_reg, shift_value_reg, scratch1_reg);
        // if ((shift_value & 0x1f) != 0
        trans_ctx.block.GenIf(scratch1_reg, 6); // 6 = number of instructions in the if-body
        {
            // result = value
            trans_ctx.block.GenMov(outreg_value, value_reg);

            // carry = RotateRight(value, (shift_val & 0x1f) - 1) & 1 = (value >> ((shift_val & 0x1f) - 1)) & 1
            trans_ctx.block.GenLoadImm32(scratch2_reg, 1);
            trans_ctx.block.GenSub(outreg_carry, scratch1_reg, scratch2_reg);
            trans_ctx.block.GenShiftRight(outreg_carry, value_reg, outreg_carry);
            trans_ctx.block.GenBitwiseAnd(outreg_carry, outreg_carry, scratch2_reg);

            trans_ctx.block.GenSkip(4 + boost::function_types::result_type<decltype(TranslateArithmeticShift)>::type::value);
        }
        // else
        {
            // result = RotateRight(value, shift_value & 0x1f);
            (void)TranslateRotateRight(trans_ctx, outreg_value, scratch2_reg, value_reg, scratch1_reg);

            // Check upper 3 bits to decide how to compute the carry output
            // if (shift_value != 0) carry = value >> 31
            trans_ctx.block.GenIf(shift_value_reg, 2);
            {
                trans_ctx.block.GenShiftRightImm(outreg_carry, value_reg, 31);
                trans_ctx.block.GenSkip(1);
            }
            // else carry = CPSR_C
            {
                trans_ctx.PrepareCPSR(2); // Make sure CPSR_C is loaded
                trans_ctx.block.GenMov(outreg_carry, IR::GetCPSR_C());
            }
        }
        break;
    }

    default:
        throw std::runtime_error("Invalid shift mode");
    }
}

// NOTE: the given shift_value must be within the interval [0..255]
static void CalcShifterOperandFromImmediateNew(TranslatorContext& trans_ctx, IR::RegIndex value_reg, uint32_t shift_value, ARM::OperandShifterMode mode, IR::RegIndex outreg_value, IR::RegIndex outreg_carry, IR::RegIndex scratch, IR::RegIndex scratch2, IR::RegIndex scratch3) {
    switch (mode) {
    case ARM::OperandShifterMode::LSL:
        trans_ctx.block.GenLoadImm32(scratch, shift_value);
        CalcShifterOperandNew(trans_ctx, value_reg, scratch, mode, outreg_value, outreg_carry, scratch2, scratch3);
        break;

    case ARM::OperandShifterMode::LSR:
    case ARM::OperandShifterMode::ASR:
        // LSR/ASR with shift_value 0 is treated like a shift by 32 bits
        trans_ctx.block.GenLoadImm32(scratch, shift_value ? shift_value : 32);
        CalcShifterOperandNew(trans_ctx, value_reg, scratch, mode, outreg_value, outreg_carry, scratch2, scratch3);
        break;

    case ARM::OperandShifterMode::ROR_RRX:
        if (shift_value != 0) {
            trans_ctx.block.GenLoadImm32(scratch, shift_value);
            CalcShifterOperandNew(trans_ctx, value_reg, scratch, mode, outreg_value, outreg_carry, scratch2, scratch3);
        } else {
            trans_ctx.PrepareCPSR(2); // Make sure CPSR_C is loaded
            // value = (carry << 31) | (value >> 1)
            trans_ctx.block.GenShiftLeftImm(outreg_value, IR::GetCPSR_C(), 31); // Copy to out register as scratch space
            trans_ctx.block.GenShiftRightImm(outreg_carry, value_reg, 1); // Copy to out register as scratch space
            trans_ctx.block.GenBitwiseOr(outreg_value, outreg_value, outreg_carry);

            // carry_out = value & 1
            trans_ctx.block.GenLoad32(outreg_carry, 1);
            trans_ctx.block.GenBitwiseAnd(outreg_carry, value_reg, outreg_carry);
        }
        break;

    default:
        throw std::runtime_error("Called with invalid parameters");
    }
}

static void GetAddr1ShifterOperand(TranslatorContext& trans_ctx, ARM::ARMInstr instr, uint32_t instr_addr, IR::RegIndex outreg_value, IR::RegIndex outreg_carry) {
    switch (ARM::GetAddrMode1Encoding(instr)) {
    case ARM::AddrMode1Encoding::Imm:
    {
        // Rotate immediate by an even amount of bits
        // The results of this operation are (almost) entirely determined at compile-time!

        auto result = RotateRight(instr.immed_8, 2 * instr.rotate_imm);
        trans_ctx.block.GenLoadImm32(outreg_value, result);

        if (instr.rotate_imm) {
            trans_ctx.block.GenLoadImm32(outreg_carry, result >> 31);
        } else {
            trans_ctx.PrepareCPSR(2);
            trans_ctx.block.GenMov(outreg_carry, IR::GetCPSR_C());
        }
        break;
    }

    case ARM::AddrMode1Encoding::ShiftByImm:
    {
        auto working_reg = (instr.idx_rm != 15) ? IR::GetNativeReg(instr.idx_rm) : IR::scratch_regs[11];
        if (instr.idx_rm == 15) {
            trans_ctx.block.GenLoadImm32(working_reg, instr_addr+8);
        }

        CalcShifterOperandFromImmediateNew(trans_ctx, working_reg, instr.addr1_shift_imm, instr.addr1_shift, outreg_value, outreg_carry, IR::scratch_regs[7], IR::scratch_regs[9], IR::scratch_regs[10]);
        break;
    }

    case ARM::AddrMode1Encoding::ShiftByReg:
    {
        if (instr.idx_rd == ARM::Regs::PC ||
            instr.idx_rm == ARM::Regs::PC ||
            instr.idx_rn == ARM::Regs::PC ||
            instr.idx_rs == ARM::Regs::PC)
            throw std::runtime_error("Unpredictable configuration");

        // Get lower 8 bits from shift value register (Rs)
        auto shift_value_reg = IR::scratch_regs[10];
        trans_ctx.block.GenLoadImm32(shift_value_reg, 0xff);
        trans_ctx.block.GenBitwiseAnd(shift_value_reg, IR::GetNativeReg(instr.idx_rs), shift_value_reg);

        CalcShifterOperandNew(trans_ctx, IR::GetNativeReg(instr.idx_rm), shift_value_reg, instr.addr1_shift, outreg_value, outreg_carry, IR::scratch_regs[7], IR::scratch_regs[9]);
        break;
    }

    default:
        throw std::runtime_error("Invalid inputs");
    }
}

static void TranslateMov(TranslatorContext& ctx, ARM::ARMInstr instr, IR::RegIndex shifter_operand_reg, IR::RegIndex shifter_operand_carry_reg) {
    uint8_t dest_reg = IR::GetNativeReg(instr.idx_rd);

    // Load shifter operand into dest register
    // TODO: We are wasting one IR instruction here by not directly loading the value into the destination register!
    ctx.block.GenMov(dest_reg, shifter_operand_reg);

    if (instr.addr1_S) {
        TranslatorUpdateCPSR_N(ctx.block, dest_reg);
        TranslatorUpdateCPSR_Z(ctx.block, dest_reg);
        ctx.block.GenMov(IR::GetCPSR_C(), shifter_operand_carry_reg);
        // V unaffected

        ctx.SetCPSRDirty(0);
        ctx.SetCPSRDirty(1);
        ctx.SetCPSRDirty(2);
    }

    if (instr.idx_rd == ARM::Regs::PC)
        ctx.SetPCWritten();
}

// Move Not
static void TranslateMvn(TranslatorContext& ctx, ARM::ARMInstr instr, IR::RegIndex shifter_operand_reg, IR::RegIndex shifter_operand_carry_reg) {
    // Allocate a scratch register if we need the original operand values for computing flags below
    uint8_t dest_reg = IR::GetNativeReg(instr.idx_rd);

    // Load negated shifter operand into dest register
    ctx.block.GenBitwiseNeg(dest_reg, shifter_operand_reg);

    if (instr.addr1_S) {
        TranslatorUpdateCPSR_N(ctx.block, dest_reg);
        TranslatorUpdateCPSR_Z(ctx.block, dest_reg);
        ctx.block.GenMov(IR::GetCPSR_C(), shifter_operand_carry_reg);
        // V unaffected

        ctx.SetCPSRDirty(0);
        ctx.SetCPSRDirty(1);
        ctx.SetCPSRDirty(2);
    }

    if (instr.idx_rd == ARM::Regs::PC)
        ctx.SetPCWritten();
}

// Bit Clear
static void TranslateBic(TranslatorContext& ctx, ARM::ARMInstr instr, IR::RegIndex shifter_operand_reg, IR::RegIndex shifter_operand_carry_reg) {
    // Allocate a scratch register if we need the original operand values for computing flags below
    bool dest_in_scratch = instr.addr1_S && (instr.idx_rn == instr.idx_rd);
    uint8_t dest_reg = dest_in_scratch ? IR::scratch_regs[0] : IR::GetNativeReg(instr.idx_rd);

    // Load negated shifter operand into scratch register
    ctx.block.GenBitwiseNeg(IR::scratch_regs[1], shifter_operand_reg);

    // Perform operation: dest_reg = Rn & ~shifter_operand
    ctx.block.GenBitwiseAnd(dest_reg, IR::GetNativeReg(instr.idx_rn), IR::scratch_regs[1]);

    if (instr.addr1_S) {
        TranslatorUpdateCPSR_N(ctx.block, dest_reg);
        TranslatorUpdateCPSR_Z(ctx.block, dest_reg);
        ctx.block.GenMov(IR::GetCPSR_C(), shifter_operand_carry_reg);
        // V unaffected

        ctx.SetCPSRDirty(0);
        ctx.SetCPSRDirty(1);
        ctx.SetCPSRDirty(2);

        // Finally, move result to destination register if necessary
        if (dest_in_scratch)
            ctx.block.GenMov(IR::GetNativeReg(instr.idx_rd), dest_reg);
    }

    if (instr.idx_rd == ARM::Regs::PC)
        ctx.SetPCWritten();
}

// Exclusive OR
static void TranslateEor(TranslatorContext& ctx, ARM::ARMInstr instr, IR::RegIndex shifter_operand_reg, IR::RegIndex shifter_operand_carry_reg) {
    // Allocate a scratch register if we need the original operand values for computing flags below
    bool dest_in_scratch = instr.addr1_S && (instr.idx_rn == instr.idx_rd);
    uint8_t dest_reg = dest_in_scratch ? IR::scratch_regs[0] : IR::GetNativeReg(instr.idx_rd);

    // Perform operation: dest_reg = Rn ^ shifter_operand
    ctx.block.GenBitwiseXor(dest_reg, IR::GetNativeReg(instr.idx_rn), shifter_operand_reg);

    if (instr.addr1_S) {
        TranslatorUpdateCPSR_N(ctx.block, dest_reg);
        TranslatorUpdateCPSR_Z(ctx.block, dest_reg);
        ctx.block.GenMov(IR::GetCPSR_C(), shifter_operand_carry_reg);
        // V unaffected

        ctx.SetCPSRDirty(0);
        ctx.SetCPSRDirty(1);
        ctx.SetCPSRDirty(2);

        // Finally, move result to destination register if necessary
        if (dest_in_scratch)
            ctx.block.GenMov(IR::GetNativeReg(instr.idx_rd), dest_reg);
    }

    if (instr.idx_rd == ARM::Regs::PC)
        ctx.SetPCWritten();
}

static void TranslateMul(TranslatorContext& ctx, ARM::ARMInstr instr) {
    if (instr.idx_rn == ARM::Regs::PC)
        throw std::runtime_error("Using PC as Rd yields unpredictable behavior in some cases");

    // NOTE: Actually, this might even be unpredictable.
    if (instr.idx_rm == ARM::Regs::PC || instr.idx_rs == ARM::Regs::PC)
        throw std::runtime_error("Using PC as Rs or Rm is not implemented, yet");

    // sic: Rn defines the output here.
    auto dest_reg = IR::GetNativeReg(instr.idx_rn);
    ctx.block.GenMul(dest_reg, IR::GetNativeReg(instr.idx_rm), IR::GetNativeReg(instr.idx_rs));

    if (instr.addr1_S) {
        TranslatorUpdateCPSR_N(ctx.block, dest_reg);
        TranslatorUpdateCPSR_Z(ctx.block, dest_reg);
        // On ARMv4 and earlier, C is unpredictable, while on newer ISAs it's unaffected.
        // V unaffected

        ctx.SetCPSRDirty(0);
        ctx.SetCPSRDirty(1);
    }
}

static uint32_t HandlerMla(CPUContext& ctx, ARM::ARMInstr instr) {
    if (instr.idx_rn == ARM::Regs::PC)
        return HandlerStubWithMessage(ctx, instr, "Using PC as Rd yields unpredictable behavior in some cases");

    // sic: Rn defines the output here, while Rd is an input
    ctx.cpu.reg[instr.idx_rn] = ctx.cpu.FetchReg(instr.idx_rm) * ctx.cpu.FetchReg(instr.idx_rs) + ctx.cpu.FetchReg(instr.idx_rd);

    if (instr.addr1_S) {
        UpdateCPSR_N(ctx, ctx.cpu.reg[instr.idx_rd]);
        UpdateCPSR_Z(ctx, ctx.cpu.reg[instr.idx_rd]);
        // On ARMv4 and earlier, C is unpredictable, while on newer ISAs it's unaffected.
        // V unaffected
    }

    return NextInstr(ctx);
}

static void TranslateAnd(TranslatorContext& ctx, ARM::ARMInstr instr, IR::RegIndex shifter_operand_reg, IR::RegIndex shifter_operand_carry_reg) {
    // Allocate a scratch register if we need the original operand values for computing flags below
    bool dest_in_scratch = instr.addr1_S && (instr.idx_rn == instr.idx_rd);
    uint8_t dest_reg = dest_in_scratch ? IR::scratch_regs[0] : IR::GetNativeReg(instr.idx_rd);

    // Perform operation: dest_reg = Rn & shifter_operand
    ctx.block.GenBitwiseAnd(dest_reg, IR::GetNativeReg(instr.idx_rn), shifter_operand_reg);

    if (instr.addr1_S) {
        TranslatorUpdateCPSR_N(ctx.block, dest_reg);
        TranslatorUpdateCPSR_Z(ctx.block, dest_reg);
        ctx.block.GenMov(IR::GetCPSR_C(), shifter_operand_carry_reg);
        // V unaffected

        ctx.SetCPSRDirty(0);
        ctx.SetCPSRDirty(1);
        ctx.SetCPSRDirty(2);

        // Finally, move result to destination register if necessary
        if (dest_in_scratch)
            ctx.block.GenMov(IR::GetNativeReg(instr.idx_rd), dest_reg);
    }

    if (instr.idx_rd == ARM::Regs::PC)
        ctx.SetPCWritten();
}

// Logical OR
static void TranslateOrr(TranslatorContext& ctx, ARM::ARMInstr instr, IR::RegIndex shifter_operand_reg, IR::RegIndex shifter_operand_carry_reg) {
    // Allocate a scratch register if we need the original operand values for computing flags below
    bool dest_in_scratch = instr.addr1_S && (instr.idx_rn == instr.idx_rd);
    uint8_t dest_reg = dest_in_scratch ? IR::scratch_regs[0] : IR::GetNativeReg(instr.idx_rd);

    // Perform operation: dest_reg = Rn | shifter_operand
    ctx.block.GenBitwiseOr(dest_reg, IR::GetNativeReg(instr.idx_rn), shifter_operand_reg);

    if (instr.addr1_S) {
        TranslatorUpdateCPSR_N(ctx.block, dest_reg);
        TranslatorUpdateCPSR_Z(ctx.block, dest_reg);
        ctx.block.GenMov(IR::GetCPSR_C(), shifter_operand_carry_reg);
        // V unaffected

        ctx.SetCPSRDirty(0);
        ctx.SetCPSRDirty(1);
        ctx.SetCPSRDirty(2);

        // Finally, move result to destination register if necessary
        if (dest_in_scratch)
            ctx.block.GenMov(IR::GetNativeReg(instr.idx_rd), dest_reg);
    }

    if (instr.idx_rd == ARM::Regs::PC)
        ctx.SetPCWritten();
}

// Test Equivalence
static void TranslateTeq(TranslatorContext& ctx, ARM::ARMInstr instr, IR::RegIndex shifter_operand_reg, IR::RegIndex shifter_operand_carry_reg) {
    // Allocate a scratch register for the output (TEQ doesn't write the result to any guest register)
    uint8_t dest_reg = IR::scratch_regs[0];

    // Perform operation: dest_reg = Rn ^ shifter_operand
    ctx.block.GenBitwiseXor(dest_reg, IR::GetNativeReg(instr.idx_rn), shifter_operand_reg);

    if (instr.addr1_S) {
        TranslatorUpdateCPSR_N(ctx.block, dest_reg);
        TranslatorUpdateCPSR_Z(ctx.block, dest_reg);
        ctx.block.GenMov(IR::GetCPSR_C(), shifter_operand_carry_reg);
        // V unaffected

        ctx.SetCPSRDirty(0);
        ctx.SetCPSRDirty(1);
        ctx.SetCPSRDirty(2);
    }
}

// Test
static void TranslateTst(TranslatorContext& ctx, ARM::ARMInstr instr, IR::RegIndex shifter_operand_reg, IR::RegIndex shifter_operand_carry_reg) {
    // Allocate a scratch register for the output (TST doesn't write the result to any guest register)
    uint8_t dest_reg = IR::scratch_regs[0];

    // Perform operation: dest_reg = Rn & shifter_operand
    ctx.block.GenBitwiseAnd(dest_reg, IR::GetNativeReg(instr.idx_rn), shifter_operand_reg);

    if (instr.addr1_S) {
        TranslatorUpdateCPSR_N(ctx.block, dest_reg);
        TranslatorUpdateCPSR_Z(ctx.block, dest_reg);
        ctx.block.GenMov(IR::GetCPSR_C(), shifter_operand_carry_reg);
        // V unaffected

        ctx.SetCPSRDirty(0);
        ctx.SetCPSRDirty(1);
        ctx.SetCPSRDirty(2);
    }
}

static void TranslateAdd(TranslatorContext& ctx, ARM::ARMInstr instr, IR::RegIndex shifter_operand_reg) {
    // Allocate a scratch register if we need the original operand values for computing flags below
    bool dest_in_scratch = instr.addr1_S && (instr.idx_rn == instr.idx_rd);
    uint8_t dest_reg = dest_in_scratch ? IR::scratch_regs[0] : IR::GetNativeReg(instr.idx_rd);

    // Perform addition: dest_reg = Rn + shifter_operand
    ctx.block.GenAdd(dest_reg, IR::GetNativeReg(instr.idx_rn), shifter_operand_reg);

    if (instr.addr1_S) {
        TranslatorUpdateCPSR_N(ctx.block, dest_reg);
        TranslatorUpdateCPSR_Z(ctx.block, dest_reg);
        TranslatorUpdateCPSR_C_FromCarry(ctx.block, IR::GetNativeReg(instr.idx_rn), shifter_operand_reg, dest_reg);
        TranslatorUpdateCPSR_V_FromAdd(ctx.block, IR::GetNativeReg(instr.idx_rn), shifter_operand_reg, dest_reg);

        ctx.SetCPSRDirty(0);
        ctx.SetCPSRDirty(1);
        ctx.SetCPSRDirty(2);
        ctx.SetCPSRDirty(3);

        // Finally, move result to destination register if necessary
        if (dest_in_scratch)
            ctx.block.GenMov(IR::GetNativeReg(instr.idx_rd), dest_reg);
    }

    if (instr.idx_rd == ARM::Regs::PC)
        ctx.SetPCWritten();
}

// Add with Carry
static void TranslateAdc(TranslatorContext& ctx, ARM::ARMInstr instr, IR::RegIndex shifter_operand_reg, IR::RegIndex shifter_operand_carry_reg) {
    // Allocate a scratch register if we need the original operand values for computing flags below
    bool dest_in_scratch = instr.addr1_S && (instr.idx_rn == instr.idx_rd);
    uint8_t dest_reg = dest_in_scratch ? IR::scratch_regs[0] : IR::GetNativeReg(instr.idx_rd);

    // Make sure the CPSR_C flag is loaded
    ctx.PrepareCPSR(2);

    // Perform addition: dest_reg = Rn + shifter_operand + carry
    ctx.block.GenAdd(dest_reg, shifter_operand_reg, IR::GetCPSR_C());
    ctx.block.GenAdd(dest_reg, dest_reg, IR::GetNativeReg(instr.idx_rn));

    if (instr.addr1_S) {
        // TODO: Implement
        throw std::runtime_error("Unsupported configuration: Cannot update condition flags in ADC");
//        HandlerStubAnnotated(ctx, instr, __LINE__);

        // Finally, move result to destination register if necessary
        if (dest_in_scratch)
            ctx.block.GenMov(IR::GetNativeReg(instr.idx_rd), dest_reg);
    }

    if (instr.idx_rd == ARM::Regs::PC)
        ctx.SetPCWritten();
}

static void TranslateSub(TranslatorContext& ctx, ARM::ARMInstr instr, IR::RegIndex shifter_operand_reg) {
    // Allocate a scratch register if we need the original operand values for computing flags below
    bool dest_in_scratch = instr.addr1_S && (instr.idx_rn == instr.idx_rd);
    uint8_t dest_reg = dest_in_scratch ? IR::scratch_regs[0] : IR::GetNativeReg(instr.idx_rd);

    // Perform subtraction: dest_reg = Rn - shifter_operand
    ctx.block.GenSub(dest_reg, IR::GetNativeReg(instr.idx_rn), shifter_operand_reg);

    if (instr.addr1_S) {
        TranslatorUpdateCPSR_N(ctx.block, dest_reg);
        TranslatorUpdateCPSR_Z(ctx.block, dest_reg);
        TranslatorUpdateCPSR_C_FromBorrow(ctx.block, IR::GetNativeReg(instr.idx_rn), shifter_operand_reg);
        TranslatorUpdateCPSR_V_FromSub(ctx.block, IR::GetNativeReg(instr.idx_rn), shifter_operand_reg, dest_reg);

        ctx.SetCPSRDirty(0);
        ctx.SetCPSRDirty(1);
        ctx.SetCPSRDirty(2);
        ctx.SetCPSRDirty(3);

        // Finally, move result to destination register if necessary
        if (dest_in_scratch)
            ctx.block.GenMov(IR::GetNativeReg(instr.idx_rd), dest_reg);
    }

    if (instr.idx_rd == ARM::Regs::PC)
        ctx.SetPCWritten();
}

// Reverse Subtract
static void TranslateRsb(TranslatorContext& ctx, ARM::ARMInstr instr, IR::RegIndex shifter_operand_reg) {
    // Allocate a scratch register if we need the original operand values for computing flags below
    bool dest_in_scratch = instr.addr1_S && (instr.idx_rn == instr.idx_rd);
    uint8_t dest_reg = dest_in_scratch ? IR::scratch_regs[0] : IR::GetNativeReg(instr.idx_rd);

    // Perform subtraction: dest_reg = shifter_operand - Rn
    ctx.block.GenSub(dest_reg, shifter_operand_reg, IR::GetNativeReg(instr.idx_rn));

    if (instr.addr1_S) {
        TranslatorUpdateCPSR_N(ctx.block, dest_reg);
        TranslatorUpdateCPSR_Z(ctx.block, dest_reg);
        TranslatorUpdateCPSR_C_FromBorrow(ctx.block, shifter_operand_reg, IR::GetNativeReg(instr.idx_rn));
        TranslatorUpdateCPSR_V_FromSub(ctx.block, shifter_operand_reg, IR::GetNativeReg(instr.idx_rn), dest_reg);

        ctx.SetCPSRDirty(0);
        ctx.SetCPSRDirty(1);
        ctx.SetCPSRDirty(2);
        ctx.SetCPSRDirty(3);

        // Finally, move result to destination register if necessary
        if (dest_in_scratch)
            ctx.block.GenMov(IR::GetNativeReg(instr.idx_rd), dest_reg);
    }

    if (instr.idx_rd == ARM::Regs::PC)
        ctx.SetPCWritten();
}

/**
 * Insert an instruction sequence that sign-extends the given register (possibly inplace)
 * @pre Assumes the contents of the source register do not exceed (1<<src_bits)-1
 * @pre Up to two of the three input registers may be equal. src and scratch may never be equal.
 */
template<unsigned src_bits, unsigned dest_bits>
void TranslateSignExtend(TranslatorContext& ctx, IR::RegIndex src, IR::RegIndex dst, IR::RegIndex scratch) {
    static_assert(src_bits > 0 && src_bits < 32, "Invalid number of source bits given for sign-extension");
    static_assert(dest_bits > src_bits, "Invalid number of dest bits given for sign-extension");
    static_assert(dest_bits == 32, "Currently only sign extension to 32 bits is supported");

    assert(scratch != src);

    // if (src >> (src_bits - 1))
    ctx.block.GenShiftRightImm(scratch, src, src_bits - 1);
    ctx.block.GenIf(scratch, (dst != src) ? 3 : 2);
    {
        // dst = src | (0xFFFFFFFF << src_bits)
        ctx.block.GenLoadImm32(scratch, 0xFFFFFFFF << src_bits);
        ctx.block.GenBitwiseOr(dst, src, scratch);
        if (dst != src)
            ctx.block.GenSkip(1);
    }
    // else
    {
        // dst = src (nop if they refer to the same register)
        if (dst != src)
            ctx.block.GenMov(dst, src);
    }
}

/**
 * Insert an instruction sequence that sign-extends the given register (possibly inplace)
 * @pre Assumes the contents of the source register do not exceed (1<<src_bits)-1
 * @pre None of the three input registers may be equal.
 * @note Currently, sign-extension is only supported when the input operand has 32 bits or less
 * @todo Actually, we could change this to have similar restrictions as the 32 bit version ("dst_low and scratch" or "src and dst_low" might overlap).
 */
template<unsigned src_bits, unsigned dest_bits>
void TranslateSignExtend(TranslatorContext& ctx, IR::RegIndex src, IR::RegIndex dst_low, IR::RegIndex dst_high, IR::RegIndex scratch) {
    static_assert(src_bits > 0 && src_bits <= 32, "Invalid number of source bits given for sign-extension");
    static_assert(dest_bits > src_bits, "Invalid number of dest bits given for sign-extension");
    static_assert(dest_bits == 64, "Currently only sign extension to 64 bits is supported");

    assert(scratch != src);
    assert(scratch != dst_low);
    assert(scratch != dst_high);
    assert(src != dst_low);
    assert(src != dst_high);
    assert(dst_low != dst_high);

    // if (src >> (src_bits - 1))
    ctx.block.GenShiftRightImm(scratch, src, src_bits - 1);
    ctx.block.GenIf(scratch, 4);
    {
        // dst_low = src | (0xFFFFFFFF << src_bits)
        // dst_high = 0xFFFFFFFF
        ctx.block.GenLoadImm32(scratch, 0xFFFFFFFF << src_bits);
        ctx.block.GenBitwiseOr(dst_low, src, scratch);
        ctx.block.GenLoadImm32(dst_high, 0xFFFFFFFF);
        ctx.block.GenSkip(2);
    }
    // else
    {
        // dst_low = src
        // dst_high = 0
        ctx.block.GenMov(dst_low, src);
        ctx.block.GenLoadImm32(dst_high, 0);
    }
}

static void TranslateAddrMode3(TranslatorContext& ctx, ARM::ARMInstr instr, IR::RegIndex offset_reg, IR::RegIndex scratch_reg, uint32_t instr_offset) {
    if (instr.idx_rd == 15)
        throw std::runtime_error("Configuration not implemented");

    // P=0: Memory access using base register; after the access, the base register has the offset applied to it (post-indexed addressing)
    // P=0, W=0: normal memory access using base register
    // P=0, W=1: unpredictable
    // P=1, W=0: memory access using base register with applied offset (base register remains unchanged).
    // P=1, W=1: memory access using base register with applied offset (base register will be updated).

    if (instr.idx_rn == instr.idx_rd && (!instr.ldr_P && instr.ldr_W)) {
        // Unknown instruction behavior for Rd == Rn: Which of the register writes has higher priority?
        throw std::runtime_error("Memory access: Unknown instruction behavior for Rd==Rn");
    }

    // If necessary, update the stored PC value
    // TODO: Drop the need for saving and restoring the old PC value!
    ctx.block.GenMov(IR::scratch_regs[15], IR::GetNativeReg(ARM::Regs::PC));
    if (instr.idx_rn == ARM::Regs::PC)
        ctx.block.GenLoadImm32(IR::GetNativeReg(ARM::Regs::PC), instr_offset + 8);

    // base = rN + instr.ldr_P * offset
    uint32_t base_reg = IR::scratch_regs[0];

    if (instr.ldr_P) {
        ctx.block.GenAdd(base_reg, IR::GetNativeReg(instr.idx_rn), offset_reg);
    } else {
        ctx.block.GenMov(base_reg, IR::GetNativeReg(instr.idx_rn));
    }

    auto rd_reg = IR::GetNativeReg(instr.idx_rd);
    auto rd_reg2 = IR::GetNativeReg(instr.idx_rd + 1);

    // TODO: Need to take care of shared memory magic for store instructions!
    switch (ARM::GetAddrMode3AccessType(instr)) {
    case ARM::AddrMode3AccessType::LoadSignedByte:
        // Load with sign extend
        ctx.block.GenLoad8(rd_reg, base_reg);
        TranslateSignExtend<8, 32>(ctx, rd_reg, rd_reg, scratch_reg);
        if (instr.idx_rd == ARM::Regs::PC)
            ctx.SetPCWritten();
        break;

    case ARM::AddrMode3AccessType::StoreByte:
        // TODO: StoreByte, but we write a uint16_t??
        ctx.block.GenStore16(base_reg, rd_reg);
        //ctx.block.GenStore8(base_reg, rd_reg);
        break;

    case ARM::AddrMode3AccessType::LoadSignedHalfword:
        // Load with sign extend

        // TODO: If CP15 is configured appropriately, bit0 of address may be non-zero
        // TODO: Translate this runtime check!
        //if ((address & 0x1) != 0)
        //    return HandlerStubWithMessage(ctx, instr, "Unpredictable configuration");

        ctx.block.GenLoad16(rd_reg, base_reg);
        TranslateSignExtend<16, 32>(ctx, rd_reg, rd_reg, scratch_reg);
        if (instr.idx_rd == ARM::Regs::PC)
            ctx.SetPCWritten();
        break;

    case ARM::AddrMode3AccessType::LoadUnsignedHalfword:
        // TODO: If CP15 is configured appropriately, bit0 of address may be non-zero
        // TODO: Translate this runtime check!
        //if ((address & 0x1) != 0)
        //    return HandlerStubWithMessage(ctx, instr, "Unpredictable configuration");

        ctx.block.GenLoad16(rd_reg, base_reg);
        if (instr.idx_rd == ARM::Regs::PC)
            ctx.SetPCWritten();
        break;

    case ARM::AddrMode3AccessType::StoreHalfword:
        // TODO: If CP15 is configured appropriately, bit0 of address may be non-zero
        // TODO: Translate this runtime check!
        //if ((address & 0x1) != 0)
        //    return HandlerStubWithMessage(ctx, instr, "Unpredictable configuration");

        ctx.block.GenStore16(base_reg, rd_reg);
        break;

    case ARM::AddrMode3AccessType::LoadDoubleword:
        // TODO: If CP15 is configured appropriately, bit2 of address may be non-zero
        // TODO: Translate this runtime check!
        //if ((instr.idx_rd % 2) && instr.idx_rd != 14 && (address & 0x7) != 0)
        //    return HandlerStubWithMessage(ctx, instr, "Unpredictable configuration");

        ctx.block.GenLoad32(rd_reg, base_reg);
        ctx.block.GenLoadImm32(scratch_reg, 4);
        ctx.block.GenAdd(scratch_reg, base_reg, scratch_reg);
        ctx.block.GenLoad32(rd_reg2, scratch_reg);
        if (instr.idx_rd == ARM::Regs::PC)
            ctx.SetPCWritten();
        if (instr.idx_rd + 1 == ARM::Regs::PC)
            ctx.SetPCWritten();
        break;

    case ARM::AddrMode3AccessType::StoreDoubleword:
        // TODO: If CP15 is configured appropriately, bit2 of address may be non-zero
        // TODO: Translate this runtime check!
        //if ((instr.idx_rd % 2) && instr.idx_rd != 14 && (address & 0x7) != 0)
        //    return HandlerStubWithMessage(ctx, instr, "Unpredictable configuration");

        ctx.block.GenStore32(base_reg, rd_reg);
        ctx.block.GenLoadImm32(scratch_reg, 4);
        ctx.block.GenAdd(scratch_reg, base_reg, scratch_reg);
        ctx.block.GenStore32(scratch_reg, rd_reg2);
        break;

    default:
        throw std::runtime_error("Not an addressing mode 3 instruction - configuration not implemented");
    }

    if (!instr.ldr_P || instr.ldr_W) {
        // rN = rN + offset
        if (!instr.ldr_P) {
            ctx.block.GenAdd(IR::GetNativeReg(instr.idx_rn), base_reg, offset_reg);
        } else {
            ctx.block.GenMov(IR::GetNativeReg(instr.idx_rn), base_reg);
        }

        if (instr.idx_rn == 15) {
            // TODO: Unknown behavior for PC
            throw std::runtime_error("Unsupported configuration: Memory access with writeback and rN=15.");
        }
    }

    // TODO: Remove
    if (!ctx.IsPCDirty())
        ctx.block.GenMov(IR::GetNativeReg(ARM::Regs::PC), IR::scratch_regs[15]);
}

// Signed Multiply Long
static void TranslateSmull(TranslatorContext& ctx, ARM::ARMInstr instr) {
    // If either of the destination registers are PC, abort - this is unpredictable behavior!
    if (instr.idx_rd == 15 || instr.idx_rn == 15)
        throw std::runtime_error("Unpredictable behavior");

    // Using PC for either of the source registers is not supported, currently.
    if (instr.idx_rs == 15 || instr.idx_rm == 15)
        throw std::runtime_error("Not implemented");

    // NOTE: Check if this is actually unpredictable.
    if (instr.idx_rd == instr.idx_rn)
        throw std::runtime_error("Unimplemented configuration");

    ctx.block.GenSignedMulTo64(IR::GetNativeReg(instr.idx_rd), IR::GetNativeReg(instr.idx_rn), IR::GetNativeReg(instr.idx_rs), IR::GetNativeReg(instr.idx_rm));

    if (instr.addr1_S) {
        TranslatorUpdateCPSR_N(ctx.block, IR::GetNativeReg(instr.idx_rn));
        TranslatorUpdateCPSR_Z(ctx.block, IR::GetNativeReg(instr.idx_rd)); // TODO: This should actually be set to "Rn == 0 && Rd == 0"
        // C and V are unpredictable on ARMv4 and earlier (otherwise, they are unaffected)

        ctx.SetCPSRDirty(0);
        ctx.SetCPSRDirty(1);
    }
}

// Unsigned Multiply Long
static void TranslateUmull(TranslatorContext& ctx, ARM::ARMInstr instr) {
    // If either of the destination registers are PC, abort - this is unpredictable behavior!
    if (instr.idx_rd == 15 || instr.idx_rn == 15)
        throw std::runtime_error("Unpredictable behavior");

    // Using PC for either of the source registers is not supported, currently.
    if (instr.idx_rs == 15 || instr.idx_rm == 15)
        throw std::runtime_error("Not implemented");

    // NOTE: This is actually not unpredictable, apparently.
    if (instr.idx_rd == instr.idx_rn)
        throw std::runtime_error("Unimplemented configuration");

    ctx.block.GenMulTo64(IR::GetNativeReg(instr.idx_rd), IR::GetNativeReg(instr.idx_rn), IR::GetNativeReg(instr.idx_rs), IR::GetNativeReg(instr.idx_rm));

    if (instr.addr1_S) {
        TranslatorUpdateCPSR_N(ctx.block, IR::GetNativeReg(instr.idx_rn));
        TranslatorUpdateCPSR_Z(ctx.block, IR::GetNativeReg(instr.idx_rd)); // TODO: This should actually be set to "Rn == 0 && Rd == 0"
        // C and V are unpredictable on ARMv4 and earlier (otherwise, they are unaffected)

        ctx.SetCPSRDirty(0);
        ctx.SetCPSRDirty(1);
    }
}

static void TranslateCmp(TranslatorContext& ctx, ARM::ARMInstr instr, IR::RegIndex shifter_operand_reg) {
    // Allocate a scratch register for the output (CMP doesn't write the result to any guest register)
    uint8_t dest_reg = IR::scratch_regs[0];

    // Perform operation: dest_reg = Rn - shifter_operand
    ctx.block.GenSub(dest_reg, IR::GetNativeReg(instr.idx_rn), shifter_operand_reg);

    if (instr.addr1_S) {
        TranslatorUpdateCPSR_N(ctx.block, dest_reg);
        TranslatorUpdateCPSR_Z(ctx.block, dest_reg);
        TranslatorUpdateCPSR_C_FromBorrow(ctx.block, IR::GetNativeReg(instr.idx_rn), shifter_operand_reg);
        TranslatorUpdateCPSR_V_FromSub(ctx.block, IR::GetNativeReg(instr.idx_rn), shifter_operand_reg, dest_reg);

        ctx.SetCPSRDirty(0);
        ctx.SetCPSRDirty(1);
        ctx.SetCPSRDirty(2);
        ctx.SetCPSRDirty(3);
    }
}

// Compare Negative
static void TranslateCmn(TranslatorContext& ctx, ARM::ARMInstr instr, IR::RegIndex shifter_operand_reg) {
    // Allocate a scratch register for the output (CMP doesn't write the result to any guest register)
    uint8_t dest_reg = IR::scratch_regs[0];

    // Perform operation: dest_reg = Rn + shifter_operand
    ctx.block.GenAdd(dest_reg, IR::GetNativeReg(instr.idx_rn), shifter_operand_reg);

    if (instr.addr1_S) {
        TranslatorUpdateCPSR_N(ctx.block, dest_reg);
        TranslatorUpdateCPSR_Z(ctx.block, dest_reg);
        TranslatorUpdateCPSR_C_FromCarry(ctx.block, IR::GetNativeReg(instr.idx_rn), shifter_operand_reg, dest_reg);
        TranslatorUpdateCPSR_V_FromAdd(ctx.block, IR::GetNativeReg(instr.idx_rn), shifter_operand_reg, dest_reg);

        ctx.SetCPSRDirty(0);
        ctx.SetCPSRDirty(1);
        ctx.SetCPSRDirty(2);
        ctx.SetCPSRDirty(3);
    }
}

static void TranslateMemoryAccess(TranslatorContext& ctx, ARM::ARMInstr instr, IR::RegIndex offset_reg, uint32_t instr_offset) {
    // P=0: Memory access using base register; after the access, the base register has the offset applied to it (post-indexed addressing)
    // P=0, W=0: normal memory access (LDR, LDRB, STR, STRB) using base register
    // P=0, W=1: unpriviliged memory access (LDRBT, LDRT, STRBT, STRT)
    // P=1, W=0: memory access using base register with applied offset (base register remains unchanged).
    // P=1, W=1: memory access using base register with applied offset (base register will be updated).

    if (instr.idx_rn == instr.idx_rd && (!instr.ldr_P || instr.ldr_W)) {
        // Unknown instruction behavior for Rd == Rn: Which of the register writes has higher priority?
        throw std::runtime_error("Memory access: Unknown instruction behavior for Rd==Rn");
    }

    // If necessary (i.e. if loading from address determined by PC OR when storing the PC), update the stored PC value
    // TODO: Drop the need for saving and restoring the old PC value!
    ctx.block.GenMov(IR::scratch_regs[15], IR::GetNativeReg(ARM::Regs::PC));
    if (instr.idx_rn == ARM::Regs::PC ||
        (!instr.addr4_L && instr.idx_rd == ARM::Regs::PC))
        ctx.block.GenLoadImm32(IR::GetNativeReg(ARM::Regs::PC), instr_offset + 8);

    // base = rN + instr.ldr_P * offset
    uint32_t base_reg = IR::scratch_regs[0];

    if (instr.ldr_P) {
        ctx.block.GenAdd(base_reg, IR::GetNativeReg(instr.idx_rn), offset_reg);
    } else {
        ctx.block.GenMov(base_reg, IR::GetNativeReg(instr.idx_rn));
    }

    bool byte_access = ViewBitField<22, 1, uint32_t>(instr);
    if (!instr.addr4_L) {
        // Store memory
        if (byte_access) {
            ctx.block.GenStore8(base_reg, IR::GetNativeReg(instr.idx_rd));
        } else {
            ctx.block.GenStore32(base_reg, IR::GetNativeReg(instr.idx_rd));
        }

        // TODO: Magic for shared memory??
    } else {
        // Load memory
        if (byte_access)
            ctx.block.GenLoad8(IR::GetNativeReg(instr.idx_rd), base_reg);
        else
            ctx.block.GenLoad32(IR::GetNativeReg(instr.idx_rd), base_reg);

        // TODO: Make sure the lowest two bits are zero when loading to PC.
        /*if (instr.idx_rd == 15 && (ctx.cpu.reg[instr.idx_rd] & 3)) {
            // TODO: If loading to PC, ignore the lower 2 bits. Also, switch to thumb if bit 0 is set!
            return HandlerStubAnnotated(ctx, instr, __LINE__);
        }*/
    }

    if (!instr.ldr_P || instr.ldr_W) {
        // rN = rN + offset
        if (!instr.ldr_P) {
            ctx.block.GenAdd(IR::GetNativeReg(instr.idx_rn), base_reg, offset_reg);
        } else {
            ctx.block.GenMov(IR::GetNativeReg(instr.idx_rn), base_reg);
        }

        if (instr.idx_rn == 15) {
            // TODO: Unknown behavior for PC
            throw std::runtime_error("Unsupported configuration: Memory access with writeback and rN=15.");
        }
    }

    if (instr.addr4_L && instr.idx_rd == ARM::Regs::PC)
        ctx.SetPCWritten();

    if (!ctx.IsPCDirty()) {
        ctx.block.GenMov(IR::GetNativeReg(ARM::Regs::PC), IR::scratch_regs[15]);
    }
}

template<bool Load>
static void TranslateLoadStoreMultiple(TranslatorContext& ctx, ARM::ARMInstr instr, uint32_t instr_offset) {

    // If necessary, update the stored PC value
    // TODO: Drop the need for saving and restoring the old PC value!
    ctx.block.GenMov(IR::scratch_regs[15], IR::GetNativeReg(ARM::Regs::PC));
    if (instr.idx_rn == ARM::Regs::PC)
        ctx.block.GenLoadImm32(IR::GetNativeReg(ARM::Regs::PC), instr_offset + 8);

    uint32_t registers_accessed = [&]{
        uint32_t ret = 0;
        for (unsigned i = 0; i < 16; ++i)
            ret += ((instr.addr4_registers >> i) & 1);
        return ret;
    }();

    uint32_t addr_reg = IR::GetNativeReg(instr.idx_rn);
    uint32_t cur_addr_reg = IR::scratch_regs[0];

    // Compute offset from the base address to the lowest address that is loaded from/stored to
    const uint32_t address_offset = ((instr.addr4_U ? 0 : 1) * 4 * (registers_accessed - 1)) // Subtract full amount of bytes if we are transferring "downwards" (U=0)
                                    - ((instr.addr4_U ? 4 : -4) * instr.addr4_P); // exclude the base address if P=1

    ctx.block.GenLoadImm32(IR::scratch_regs[1], address_offset);
    ctx.block.GenSub(cur_addr_reg, addr_reg, IR::scratch_regs[1]);
    // NOTE: Registers are always accessed starting from the lowest address, regardless of whether we are "increasing" or "decreasing"".

    // Constant used to increment the address after every register read/write
    // TODO: Add support for indexed loads/stores to eliminate the extra instructions caused by modifying cur_addr_reg after each register!
    ctx.block.GenLoadImm32(IR::scratch_regs[2], 4);

    for (unsigned i = 0; i < 16; ++i) {
        if (((instr.addr4_registers >> i) & 1) == 0)
            continue;

        // No idea how this behaves - Would the register first get loaded (regular operation) or first be de/incremented (W bit)?
        if (instr.addr4_W && i == instr.idx_rn)
            throw std::runtime_error("Unknown configuration in LDM/STM");

        if (i == ARM::Regs::PC) {
            if (Load) {
                ctx.block.GenLoad32(IR::GetNativeReg(i), cur_addr_reg);
                ctx.SetPCWritten();

                // TODO: Set CPSR.T to val & 1
                //if (read_value & 1)
                //    throw std::runtime_error("Unimplemented configuration: Should switch to thumb mode since the new PC value has its LSB set");

                // Move SPSR to CPSR
                // TODO: Re-enable. For usermode emulation, we don't need to support this anyway.
                //if (Load && instr.addr4_S) {
                //    ctx.cpu.ReplaceCPSR(ctx.cpu.GetSPSR(ctx.cpu.cpsr.mode));
                //}
            } else {
                // Stored value is ImplementationDefined
                throw std::runtime_error("Implementation defined configuration: STM from PC");
            }
        } else {
            if (Load) {
                ctx.block.GenLoad32(IR::GetNativeReg(i), cur_addr_reg);
            } else {
                // if !Load and in privileged mode, use user mode banked registers instead
                // TODO: Restore this functionality once usermode emulation is complete.
                /*if (i >= 8 && !Load && ctx.cpu.InPrivilegedMode()) {
                    Processor::WriteVirtualMemory<uint32_t>(ctx, cur_addr_reg, ctx.cpu.banked_regs_user[i-8]);
                } else */{
                    ctx.block.GenStore32(cur_addr_reg, IR::GetNativeReg(i));
                }
            }
        }

        // current address += 4
        ctx.block.GenAdd(cur_addr_reg, cur_addr_reg, IR::scratch_regs[2]);
    }

    // TODO: Is this really updated *after* the transfer?
    // NOTE: Actually, it doesn't matter too much - the case that rn is enabled in the register list is unpredictable!
    if (instr.addr4_W) {
        uint32_t offset_to_end = registers_accessed * 4 * (instr.addr4_U ? 1 : -1);
        ctx.block.GenLoadImm32(IR::scratch_regs[1], offset_to_end);
        ctx.block.GenAdd(addr_reg, addr_reg, IR::scratch_regs[1]);
        if (addr_reg == IR::GetNativeReg(ARM::Regs::PC))
            throw std::runtime_error("Not supported, yet");
    }

    if (!ctx.IsPCDirty())
        ctx.block.GenMov(IR::GetNativeReg(ARM::Regs::PC), IR::scratch_regs[15]);
}

static uint32_t HandlerUxtb(CPUContext& ctx, ARM::ARMInstr instr) {
    // Using PC is Unpredictable
    if (instr.idx_rd == 15 || instr.idx_rm == 15)
        return HandlerStubAnnotated(ctx, instr, __LINE__);

    ctx.cpu.reg[instr.idx_rd] = RotateRight(ctx.cpu.FetchReg(instr.idx_rm), 8 * instr.uxtb_rotate) & 0xFF;

    return NextInstr(ctx);
}

static uint32_t HandlerUxth(CPUContext& ctx, ARM::ARMInstr instr) {
    // Using PC is Unpredictable
    if (instr.idx_rd == 15 || instr.idx_rm == 15)
        return HandlerStubAnnotated(ctx, instr, __LINE__);

    ctx.cpu.reg[instr.idx_rd] = RotateRight(ctx.cpu.FetchReg(instr.idx_rm), 8 * instr.uxtb_rotate) & 0xFFFF;

    return NextInstr(ctx);
}

/*static uint32_t HandlerSxth(CPUContext& ctx, ARM::ARMInstr instr) {
    // Using PC is Unpredictable
    if (instr.idx_rd == 15 || instr.idx_rm == 15)
        return HandlerStubAnnotated(ctx, instr, __LINE__);

    // Rotate and sign extend from 16 to 32 bits
    ctx.cpu.reg[instr.idx_rd] = (int16_t)(uint16_t)(RotateRight(ctx.cpu.FetchReg(instr.idx_rm), 8 * instr.uxtb_rotate));

    return NextInstr(ctx);
}*/

static void TranslateDataProcessingInstr(TranslatorContext& ctx, ARM::ARMInstr instr, IR::RegIndex shifter_operand_reg, IR::RegIndex shifter_operand_carry_reg, uint32_t instr_offset) {
    // If the instruction reads from PC, update the register with the proper value
    // TODO: This is a compile time constant, which should be exploited! (at least, we should be using the internal "current PC+8 value" register instead)
/*    if (instr.idx_rn == 15 && instr.identifier_21_24 != 0b1111 && instr.identifier_21_24 != 0b1101) {
        ctx.block.GenLoadImm32(IR::GetNativeReg(ARM::Regs::PC), instr_offset + 8);
    }*/
// TODO: Cleanup
    // TODO: Drop the need for saving and restoring the old PC value!
    ctx.block.GenMov(IR::scratch_regs[15], IR::GetNativeReg(ARM::Regs::PC));
//    if (instr.idx_rn == 15 && instr.identifier_21_24 != 0b1111 && instr.identifier_21_24 != 0b1101)
        ctx.block.GenLoadImm32(IR::GetNativeReg(ARM::Regs::PC), instr_offset + 8);

    switch (instr.identifier_21_24) {
    case 0b0000:
        TranslateAnd(ctx, instr, shifter_operand_reg, shifter_operand_carry_reg);
        break;

    case 0b0001:
        TranslateEor(ctx, instr, shifter_operand_reg, shifter_operand_carry_reg);
        break;

    case 0b0010:
        TranslateSub(ctx, instr, shifter_operand_reg);
        break;

    case 0b0011:
        TranslateRsb(ctx, instr, shifter_operand_reg);
        break;

    case 0b0100:
        TranslateAdd(ctx, instr, shifter_operand_reg);
        break;

    case 0b0101:
        TranslateAdc(ctx, instr, shifter_operand_reg, shifter_operand_carry_reg);
        break;

    // TODO: SBC

    // TODO: RSC

    // TODO: Rd should be zero for TST, TEQ, CMP, and CMN
    case 0b1000:
        TranslateTst(ctx, instr, shifter_operand_reg, shifter_operand_carry_reg);
        break;

    case 0b1001:
        TranslateTeq(ctx, instr, shifter_operand_reg, shifter_operand_carry_reg);
        break;

    case 0b1010:
        TranslateCmp(ctx, instr, shifter_operand_reg);
        break;

    case 0b1011:
        TranslateCmn(ctx, instr, shifter_operand_reg);
        break;

    case 0b1100:
        TranslateOrr(ctx, instr, shifter_operand_reg, shifter_operand_carry_reg);
        break;

    case 0b1101:
        // TODO: Rn should be zero!
        TranslateMov(ctx, instr, shifter_operand_reg, shifter_operand_carry_reg);
        break;

    case 0b1110:
        TranslateBic(ctx, instr, shifter_operand_reg, shifter_operand_carry_reg);
        break;

    case 0b1111:
        // TODO: Rn should be zero!
        TranslateMvn(ctx, instr, shifter_operand_reg, shifter_operand_carry_reg);
        break;

    default:
        throw std::runtime_error("Unknown data processing instruction subopcode");
    }

    if (!ctx.IsPCDirty())
        ctx.block.GenMov(IR::GetNativeReg(ARM::Regs::PC), IR::scratch_regs[15]);
}

// SWI/SVC - software interrupt / supervisor call
static uint32_t HandlerSWI(CPUContext& ctx, ARM::ARMInstr instr) {
    // TODO: Actually, we should be moving to supervisor mode before deferring to the OS
    ctx.os->SVCRaw(*ctx.os->active_thread, instr.raw & 0xFFFFFF, ctx);

    return NextInstr(ctx);
}

void Processor::TranslateBlock(TranslatorContext& trans_ctx, CPUContext& ctx, uint32_t instr_offset) {
    // TODO: This should check the internal thumb register instead!
    if (ctx.cpu.cpsr.thumb)
        throw std::runtime_error("Thumb state not implemented, yet");

    ARM::ARMInstr instr = { ReadVirtualMemory<uint32_t>(ctx, instr_offset) };

    if (instr.cond == 0xF) {
        throw std::runtime_error("Special case instructions not implemented, yet");
    } else {
        TranslateEvalCond(trans_ctx, ctx, instr.cond, IR::scratch_regs[12]);
        auto guest_instr_block = trans_ctx.block.GenIf(IR::scratch_regs[12], 0);

        // Properties of data processing instructions
        bool is_immediate_shift = (0b000 == instr.identifier_25_27) && (0 == ViewBitField<4, 1, uint32_t>(instr));
        bool is_register_shift = (0b000 == instr.identifier_25_27
                                    && 1 == ViewBitField<4, 1, uint32_t>(instr)
                                    && 0 == ViewBitField<7, 1, uint32_t>(instr));
        bool is_immediate = (0b001 == instr.identifier_25_27);

        bool is_data_processing_instr = (is_immediate_shift || is_register_shift || is_immediate);

        // If the following is false, the instruction fields indicate a
        // condition-flag-updating operation without the S bit being set.
        // The instruction encoding hence is actually used for a different
        // operation.
        bool is_valid_data_processing_opcode = (instr.addr1_S || 0b1000 != (instr.identifier_21_24 & ~0x3));

        if (is_data_processing_instr && is_valid_data_processing_opcode) {
            if (!instr.addr1_S && (instr.identifier_21_24 & ~0x3) == 0b1000) {
                // TODO
                HandlerStubAnnotated(ctx, instr, __LINE__);
            } else {
                auto barrel_shifter_value_reg = IR::scratch_regs[3];
                auto barrel_shifter_carry_reg = IR::scratch_regs[4];

                // TODO: Handle exceptions?
                GetAddr1ShifterOperand(trans_ctx, instr, instr_offset, barrel_shifter_value_reg, barrel_shifter_carry_reg);

                if (instr.addr1_S && instr.idx_rd == ARM::Regs::PC)
                    HandlerStubWithMessage(ctx, instr, "Using PC as Rd yields unpredictable behavior in some cases");

                TranslateDataProcessingInstr(trans_ctx, instr, barrel_shifter_value_reg, barrel_shifter_carry_reg, instr_offset);
            }
        } else if (0b010 == instr.identifier_25_27) {
            // Load/Store with immediate offset

            uint32_t offset = (instr.ldr_U ? 1 : -1) * instr.ldr_offset.Value();

            IR::RegIndex offset_reg = IR::scratch_regs[1];
            trans_ctx.block.GenLoadImm32(offset_reg, offset);
            TranslateMemoryAccess(trans_ctx, instr, offset_reg, instr_offset);
        } else if (0b011 == instr.identifier_25_27) {
            // Load/Store with register offset

            // Not actually a memory access instruction in this case!
            if (ViewBitField<4, 1, uint32_t>(instr) != 0)
                HandlerStubAnnotated(ctx, instr, __LINE__);

            // Prepare input register (load PC if necessary)
            auto working_value_reg = (instr.idx_rm != 15) ? IR::GetNativeReg(instr.idx_rm) : IR::scratch_regs[11];
            if (instr.idx_rm == 15) {
                // TODO: We might as well load to the PC register here instead
                trans_ctx.block.GenLoadImm32(working_value_reg, instr_offset + 8);
            }

            IR::RegIndex offset_reg = IR::scratch_regs[3];
            auto barrel_shifter_carry_reg = IR::scratch_regs[4]; // This will actually just be ignored

            // TODO: Catch exceptions?
            CalcShifterOperandFromImmediateNew(trans_ctx, working_value_reg, instr.ldr_shift_imm, instr.ldr_shift, offset_reg, barrel_shifter_carry_reg, IR::scratch_regs[7], IR::scratch_regs[9], IR::scratch_regs[10]);

            if (!instr.ldr_U) {
                trans_ctx.block.GenLoadImm32(IR::scratch_regs[0], 0);
                trans_ctx.block.GenSub(offset_reg, IR::scratch_regs[0], offset_reg);
            }

            TranslateMemoryAccess(trans_ctx, instr, offset_reg, instr_offset);
        } else if (0b100 == instr.identifier_25_27) {
            // Load/Store multiple

            // Unpredictable
            if (instr.addr4_registers == 0)
                HandlerStubAnnotated(ctx, instr, __LINE__);

            // TODO: if L and S and User/System mode: Unpredictable
            if (instr.addr4_L && instr.addr4_S && !ctx.cpu.HasSPSR())
                HandlerStubAnnotated(ctx, instr, __LINE__);

            if (instr.addr4_L) {
                TranslateLoadStoreMultiple<true>(trans_ctx, instr, instr_offset);
            } else {
                TranslateLoadStoreMultiple<false>(trans_ctx, instr, instr_offset);
            }
        } else if (0b101 == instr.identifier_25_27) {
            // Branch
            TranslateBranch(trans_ctx, instr, instr_offset);
        } else if (0b011 == instr.identifier_25_27 && 1 == ViewBitField<4, 1, uint32_t>(instr)) {
            // Media instruction space

            switch (instr.raw & 0x1f0000e0) {
            /*case (0b01110<<20)|0b01100000:
                // TODO: Bits 8-9 should be zero
                // TODO: Bits 16-19 should be one
                ctx.cpu.PC() = HandlerUxtb(ctx, instr);
                break;*/

            /*case (0b01111<<20)|0b01100000:
                // TODO: Bits 8-9 should be zero
                // TODO: Bits 16-19 should be one
                ctx.cpu.PC() = HandlerUxth(ctx, instr);
                break;*/

            /*case (0b01011<<20)|0b01100000:
                // TODO: Bits 8-9 should be zero
                // TODO: Bits 16-19 should be one
                ctx.cpu.PC() = HandlerUxtb(ctx, instr);
                break;*/

            default:
                HandlerStubAnnotated(ctx, instr, __LINE__);
            }
        } else if (0b111 == instr.identifier_25_27
            && 1 == ViewBitField<24, 1, uint32_t>(instr)) {
            // Software interrupt
            // TODO: Should insert an artificial branch to ourself here so that
            //       the interpreter flushes all pending instructions and then
            //       only processes this single instruction.
            //       In the long run we should just implement actual
            //       exception handling ;)
            ctx.cpu.PC() = HandlerSWI(ctx, instr);
        } else if (0b000 == instr.identifier_25_27
            && 1 == ViewBitField<4, 1, uint32_t>(instr)
            && 1 == ViewBitField<7, 1, uint32_t>(instr)
            && !(0 == ViewBitField<24, 1, uint32_t>(instr) && 0b00 == ViewBitField<5, 2, uint32_t>(instr)) // Otherwise it's a multiply instruction
            ) {
            // Media instruction space

            if (instr.addr3_H == 0 && instr.addr3_S == 0) {
                // Special instruction: SWP/SWPB

                // TODO
                HandlerStubAnnotated(ctx, instr, __LINE__);
            } else {
                // Addressing Mode 3 instruction: Load/Store Byte/Halfword/Doubleword
                // TODO: P==0 and W==1 is Unpredictable!

                // If true, this an immediate offset is used; otherwise, it's a register offset.
                bool immediate = ViewBitField<22, 1, uint32_t>(instr);

                auto offset_reg = IR::scratch_regs[3];
                if (immediate) {
                    uint32_t imm_val = ((instr.addr3_immed_hi << 4) | instr.addr3_immed_lo) * (instr.ldr_U ? 1 : -1);
                    trans_ctx.block.GenLoadImm32(offset_reg, imm_val);
                } else {
                    // TODO: For register-based offsets, bits 8-11 should be zero.

                    if (instr.ldr_U) {
                        // offset = +Rm
                        if (instr.idx_rm == ARM::Regs::PC)
                            throw std::runtime_error("Cannot load from PC here (unimplemented)!");
                        offset_reg = IR::GetNativeReg(instr.idx_rm);
                    } else {
                        // offset = -Rm
                        if (instr.idx_rm == ARM::Regs::PC)
                            throw std::runtime_error("Cannot load from PC here (unimplemented)!");
                        trans_ctx.block.GenLoadImm32(offset_reg, 0);
                        trans_ctx.block.GenSub(offset_reg, offset_reg, IR::GetNativeReg(instr.idx_rm));
                    }
                }
                TranslateAddrMode3(trans_ctx, instr, offset_reg, IR::scratch_regs[1], instr_offset);
            }
        } else if (0b0000 == ViewBitField<24, 4, uint32_t>(instr)
            && 0b1001 == ViewBitField<4, 4, uint32_t>(instr)) {
            // Multiply instruction extension space

            // Check suboperation
            switch (ViewBitField<20, 4, uint32_t>(instr)) {
            case 0b0000:
            case 0b0001:
                TranslateMul(trans_ctx, instr);
                break;

            /*case 0b0010:
            case 0b0011:
                ctx.cpu.PC() = HandlerMla(ctx, instr);
                break;*/

            case 0b1000:
            case 0b1001:
                TranslateUmull(trans_ctx, instr);
                break;

            case 0b1100:
            case 0b1101:
                TranslateSmull(trans_ctx, instr);
                break;

            default:
                HandlerStubAnnotated(ctx, instr, __LINE__);
            }
        } else if (instr.identifier_20_27 == 0b00010010
                && (ViewBitField<4, 4, uint32_t>(instr) == 0b0001
                    || ViewBitField<4, 4, uint32_t>(instr) == 0b0011)) {
            // Branch and Exchange - TODO: Not sure how to categorize this properly.

            // TODO: Clean up this check - it's redundant
            if ((instr.identifier_4_23 & ~0b10) != 0b0010'1111'1111'1111'0001)
                throw std::runtime_error("Unexpected instruction pattern");

            TranslateBranchExchange(trans_ctx, instr, instr_offset);
        } else {
            // Unknown instruction format
            HandlerStubAnnotated(ctx, instr, __LINE__);
        }

        // We're done generating IR code for this guest instruction:
        // Update the conditional execution check with the length of this microblock.
        trans_ctx.block.EndIf(guest_instr_block);
    }
}

static std::unordered_map<uint32_t, TranslatorContext> blocks;

void Processor::Run(CPUContext& ctx, ProcessorController& controller) {
    for (;;) {
        for (auto& bp : ctx.setup->breakpoints) {
            if (bp.address == ctx.cpu.PC()) {
                controller.NotifyBreakpoint();
                while (!controller.request_continue) {
                }
                break;
            }
        }

        if (controller.request_pause) {
            controller.paused = true;

            // Wait until we are requested to continue, then unpause, then wait untilt he unpausing has been noticed
            while (!controller.request_continue) {
            }
            controller.paused = false;
            while (controller.request_continue) {
            }
        }

        // Lookup translated block of guest instructions (or compile it if necessary)
        uint32_t offset = ctx.cpu.reg[IR::GetNativeReg(ARM::Regs::PC)];
        auto block_it = blocks.find(offset);

        TranslatorContext& trans_ctx = [&]() -> TranslatorContext& {
            if (block_it != blocks.end()) {
                return block_it->second;
            } else {
                TranslatorContext& new_ctx = blocks[offset];
                uint32_t count = 0;
                uint32_t compile_offset = offset;
                do {
                    TranslateBlock(new_ctx, ctx, compile_offset);
                    compile_offset += 4;
                    ++count;

                    // TODO: Make sure we don't keep going forever (and then run out of stack space or fail to check global events)
                } while (!new_ctx.IsPCDirty());

                new_ctx.next_instr = compile_offset;

                // End current block
                IR::Instruction ret;
                ret.format1.op = IR::Op::Return;
                new_ctx.block.instructions.push_back(ret);

                std::cerr << "Compiled " << std::dec << count << " instruction block at offset 0x" << std::hex << offset << std::endl;
                return new_ctx;
            }
        }();

        // always update the PC *before* executing the block
        // If the block *may* update the PC (as opposed to always doing so) but happens not to
        // (e.g. due to failed conditional execution), this will make sure there is a "fallback"
        // value.
        ctx.cpu.reg[IR::GetNativeReg(ARM::Regs::PC)] = trans_ctx.next_instr;

        // Initialize CPSR condition flags if one of the handlers needs it
        if (trans_ctx.cpsr_request_load[0]) {
            ctx.cpu.reg[IR::GetCPSR_N()] = ctx.cpu.cpsr.neg;
        } else if (trans_ctx.cpsr_request_load[1]) {
            ctx.cpu.reg[IR::GetCPSR_Z()] = ctx.cpu.cpsr.zero;
        } else if (trans_ctx.cpsr_request_load[2]) {
            ctx.cpu.reg[IR::GetCPSR_C()] = ctx.cpu.cpsr.carry;
        } else if (trans_ctx.cpsr_request_load[3]) {
            ctx.cpu.reg[IR::GetCPSR_V()] = ctx.cpu.cpsr.overflow;
        }

        trans_ctx.block.Execute(ctx);

        // The PC may have been modified for temporary purposes (i.e. used as a scratch register);
        // If the value wasn't intended to actually persist, make sure it has the proper value now:
        // TODO: This is a slight design weakness, which should be fixed in the future if possible.
        if (!trans_ctx.IsPCDirty())
            ctx.cpu.reg[IR::GetNativeReg(ARM::Regs::PC)] = trans_ctx.next_instr;

        // Update guest CPSR state if necessary
        // TODO: This shouldn't actually be necessary anymore (unless e.g. the GDB stub asks for the guest values),
        //       since the emulator core only accesses the internal registers anyway.
        if (trans_ctx.IsCPSRDirty(0))
            ctx.cpu.cpsr.neg = ctx.cpu.reg[IR::GetCPSR_N()];
        if (trans_ctx.IsCPSRDirty(1))
            ctx.cpu.cpsr.zero = ctx.cpu.reg[IR::GetCPSR_Z()];
        if (trans_ctx.IsCPSRDirty(2))
            ctx.cpu.cpsr.carry = ctx.cpu.reg[IR::GetCPSR_C()];
        if (trans_ctx.IsCPSRDirty(3))
            ctx.cpu.cpsr.overflow = ctx.cpu.reg[IR::GetCPSR_V()];
    }
}

void DummyController::OnBreakpoint() {
}
void DummyController::OnSegfault() {
}
void DummyController::RequestAddBreakpoint(const Interpreter::Breakpoint& bp) const {
}
void DummyController::RequestRemoveBreakpoint(const Interpreter::Breakpoint& bp) const {
}

}  // namespace Interpreter
