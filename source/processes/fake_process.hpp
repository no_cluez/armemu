#pragma once

#include "ipc.hpp"
#include "os.hpp"

namespace HLE {

namespace OS {

class FakePort : public FakeThread {
    const std::string name;

    // TODO: Implement functionality related to limiting the maximal number of sessions!
    uint32_t max_sessions;

    HandleTable::Entry<ServerPort> port;

    virtual HandleTable::Entry<ServerPort> Setup();

    virtual void OnIPCRequest(Handle sender, const IPC::CommandHeader& header) = 0;

protected:
    // Use this for internal logging
    virtual std::string GetInternalName() const;

    // Use this in OnIPCRequest implementations when encountering unknown/unimplemented requests
    void UnknownRequest(const IPC::CommandHeader& header);

    void LogStub(const IPC::CommandHeader& header);

    void LogReturnedHandle(Handle handle);

public:
    FakePort(const std::string& name, uint32_t max_sessions);

    virtual ~FakePort() = default;

    void Run() override;
};

class FakeService : public FakePort {
    const std::string name;

    uint32_t max_sessions;

    virtual HandleTable::Entry<ServerPort> Setup() override;

protected:
    virtual std::string GetInternalName() const override;

public:
    // Create a fake port with empty public name
    FakeService(const std::string& name, uint32_t max_sessions);

    virtual ~FakeService() = default;
};

}  // namespace OS

}  // namespace HLE
