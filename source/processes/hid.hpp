#pragma once

#include "fake_process.hpp"

namespace HLE {

namespace OS {

class FakeHID : public FakeService {
public:
    FakeHID();

    virtual ~FakeHID() = default;

    void OnIPCRequest(Handle sender, const IPC::CommandHeader& header) override;
};

}  // namespace OS

}  // namespace HLE
