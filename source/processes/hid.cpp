#include "hid.hpp"

namespace HLE {

namespace OS {

FakeHID::FakeHID() : FakeService("hid:USER", 1) {
}

void FakeHID::OnIPCRequest(Handle sender, const IPC::CommandHeader& header) {
    switch (header.command_id) {
    case 0xa: // GetIPCHandles
    {
        ThreadLogger(this) << GetInternalName() << " received GetIPCHandles" << std::endl;

        // Validate command
        if (header.raw != 0x000A0000)
            GetOS().SVCBreak(*this, OS::BreakReason::Panic);

        // Create shared memory block;
        // Size is hardcoded to 0x1000, but ideally should be adjusted according to the number of HID client threads
        OS::Result result;
        VAddr shared_mem_vaddr;
        const uint32_t shared_mem_size = 0x1000;
        std::tie(result,shared_mem_vaddr) = GetOS().SVCControlMemory(*this, 0, 0, shared_mem_size, 3/*ALLOC*/, 0x3/*RW*/);
        if (result != RESULT_OK)
            GetOS().SVCBreak(*this, OS::BreakReason::Panic);

        HandleTable::Entry<SharedMemoryBlock> shared_memory;
        std::tie(result,shared_memory) = GetOS().SVCCreateMemoryBlock(*this, shared_mem_vaddr, shared_mem_size, 0x3/*RW*/, 0x3/*RW*/);

        LogStub(header);
        WriteTLS(0x80, IPC::CommandHeader::Make(0, 1, 7).raw);
        WriteTLS(0x84, RESULT_OK);
        WriteTLS(0x88, IPC::TranslationDescriptor::MakeHandles(6).raw);
        WriteTLS(0x8c, shared_memory.first);
        // The following are actually Events used to signal updates to the various shared memory regions
        WriteTLS(0x90, GetHandleTable().CreateHandle<Object>(std::make_shared<Object>()).first);
        WriteTLS(0x94, GetHandleTable().CreateHandle<Object>(std::make_shared<Object>()).first);
        WriteTLS(0x98, GetHandleTable().CreateHandle<Object>(std::make_shared<Object>()).first);
        WriteTLS(0x9c, GetHandleTable().CreateHandle<Object>(std::make_shared<Object>()).first);
        WriteTLS(0xa0, GetHandleTable().CreateHandle<Object>(std::make_shared<Object>()).first);

        break;
    }

    case 0x11: // EnableAccelerometer
        ThreadLogger(this) << GetInternalName() << " received GetIPCHandles" << std::endl;

        // Sure, whatever
        LogStub(header);
        WriteTLS(0x80, IPC::CommandHeader::Make(0, 1, 0).raw);
        WriteTLS(0x84, RESULT_OK);

        break;

    default:
        UnknownRequest(header);
    }
}

}  // namespace OS

}  // namespace HLE
