#include "arm.h"
#include "interpreter.h"
#include "gdb_stub.h"
#include "memory.h"

#include "loader/firm.hpp"
#include "loader/loader.hpp"

#include <elfio/elfio.hpp>

#include <boost/program_options.hpp>

#include <cstdint>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <thread>

using boost::endian::big_uint32_t;

extern std::atomic<unsigned> g_cycles;

namespace bpo = boost::program_options;

namespace {

// TODO: This function should take an already opened file instead!
//       Otherwise, we might run into a race condition, where the file changed since we checked that it's an FIRM file.
template<typename CPUType>
void LoadFIRM(const std::string& filename, PhysicalMemory& mem, CPUType& cpu1, CPUType& cpu2) {
    std::ifstream file;
    file.exceptions(std::ifstream::failbit | std::ifstream::badbit | std::ifstream::eofbit);
    file.open(filename, std::ios_base::in | std::ios_base::binary);

    FIRM::Header header;
    file.read((char*)&header, sizeof(header));

    std::cout << "Loading " << filename << std::endl << std::endl;
    for (auto section : header.sections) {
        if (section.size == 0)
            continue;

        if (section.type == 0)
            continue;

        std::cout << "Section: Offset 0x" << std::hex << section.file_offset.value() << ", load address 0x" << section.load_address.value() << ", size 0x" << section.size << std::endl;

        // TODO: Handle partial overlaps..

        if (section.load_address >= mem.dsp.offset && section.load_address + section.size <= mem.dsp.offset + mem.dsp.size) {
            file.seekg(section.file_offset.value());
            file.read((char*)&mem.dsp.data[section.load_address - mem.dsp.offset], section.size);
        } else if (section.load_address >= mem.axi.offset && section.load_address + section.size <= mem.axi.offset + mem.axi.size) {
            file.seekg(section.file_offset.value());
            file.read((char*)&mem.axi.data[section.load_address - mem.axi.offset], section.size);
        } else {
            throw std::runtime_error("Unknown memory range");
        }
    }

    std::cout << std::endl << "ARM11 entry point: 0x" << header.entry_arm11 << std::endl;
    cpu1.PC() = header.entry_arm11;
    cpu2.PC() = header.entry_arm11;
}

// TODO: This function should take an already opened file instead!
//       Otherwise, we might run into a race condition, where the file changed since we checked that it's an ELF file.
template<typename CPUContextType>
void LoadElf(const std::string& filename, PhysicalMemory& mem, CPUContextType& ctx) {
    ELFIO::elfio elf;

    if (!elf.load(filename))
        throw std::runtime_error("Failed to load ELF file \"" + filename + "\"");

    for (auto& segment_ptr : elf.segments) {
        if (segment_ptr->get_type() != PT_LOAD)
            throw std::runtime_error("Unknown ELF segment type");

        // TODO: Setup memory region flags (RWX)

        ctx.fake_addr_map.insert({(uint32_t)segment_ptr->get_virtual_address(),
                                  {(uint32_t)segment_ptr->get_physical_address() + 0x20000000, // HACKS.... move to FCRAM
                                   (uint32_t)segment_ptr->get_memory_size()}});
    }

    // Insert a fake application heap for use in libc's malloc()
    ctx.fake_addr_map.insert({(uint32_t)0x65434,
                              {(uint32_t)0x200654D4, // HACKS.... move to FCRAM
                               (uint32_t)0x00200000}});


    ctx.cpu.cp15.Control().M = 1;

    for (auto& section_ptr : elf.sections) {
        if (section_ptr->get_type() == SHT_PROGBITS && (section_ptr->get_flags() & SHF_ALLOC)) {
            for (uint32_t offset = 0; offset < section_ptr->get_size(); ++offset)
                Interpreter::Processor::WriteVirtualMemory<uint8_t>(ctx, section_ptr->get_address() + offset, *(section_ptr->get_data() + offset));
        } else if (section_ptr->get_type() == SHT_NOBITS) {
            for (uint32_t offset = 0; offset < section_ptr->get_size(); ++offset)
                Interpreter::Processor::WriteVirtualMemory<uint8_t>(ctx, section_ptr->get_address() + offset, 0);
        } else {
            // Ignore section
        }

        std::cerr << "Section: " << std::hex << std::setw(8) << section_ptr->get_address() << std::endl;
    }

    ctx.cpu.PC() = elf.get_entry(); // TODO: Is this correct? Wondering why elf.entry is always a 64 bit value...
    std::cerr << "Entry point: " << std::hex << std::setw(8) << ctx.cpu.PC() << std::endl;

    // TODO: Disable other CPUs

    // Setup stack; let's guess a stack size of 0x10000 bytes for now
    ctx.cpu.reg[13] = 0x10000000;
    ctx.fake_addr_map.insert({ctx.cpu.reg[13] - 0x10000, {0x27000000,0x10000}});

    // Some TLS regions... yolo, but TODO..
    ctx.fake_addr_map.insert({0x27f00000, {0x27f00000,0x10000}});
}

} // anonymous namespace


int main(int argc, char* argv[]) {
    std::string filename;
    bool enable_debugging;

    bpo::options_description desc("Allowed options");
    desc.add_options()
        ("help", "produce help message")
        ("input", bpo::value<std::string>(&filename)->required(), "Input file to load")
        ("debug", bpo::bool_switch(&enable_debugging), "Connect to GDB upon startup")
        ;

    boost::program_options::positional_options_description p;
    p.add("input", -1);

    bpo::variables_map vm;
    try {
        bpo::store(bpo::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
        bpo::notify(vm);
    } catch (bpo::required_option& e) {
        std::cerr << "ERROR: " << e.what() << std::endl;
        return 1;
    } catch (bpo::invalid_command_line_syntax& e) {
        std::cerr << "ERROR, invalid command line syntax: " << e.what() << std::endl;
        return 1;
    } catch (bpo::multiple_occurrences& e) {
        std::cerr << "ERROR: " << e.what() << std::endl;
        return 1;
    }

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 0;
    }

    // Setup machine state and load input executable
    auto setup = std::make_shared<Interpreter::Setup>();


//    LoadFIRM(filename, setup->mem, setup->cpus[0].cpu, setup->cpus[1].cpu);
    LoadElf(filename, setup->mem, setup->cpus[0]);

    // If ELF, assume we're loading a user process, and then:
    // - only emulate 1 ARM11 CPU
    // - Initialize the TTB register and the TTB data (probably using TTB0, since that's supposed to be used for process-specific addresses)

//    Interpreter::Processor proc{setup};
    // TODO: Make this ELF-only
    setup->cpus[0].cpu.cpsr.mode = ARM::InternalProcessorMode::User;

    auto gdbstub1 = enable_debugging
                        ? std::unique_ptr<Interpreter::ProcessorController>(new Interpreter::GDBStub(setup->cpus[0], 12345))
                        : std::unique_ptr<Interpreter::ProcessorController>(new Interpreter::DummyController());
    // TODO: Running two GDB stubs is broken for some reason.
    //auto gdbstub2 = std::unique_ptr<Interpreter::GDBStub>(new Interpreter::GDBStub(setup->cpus[1], 12346));

    // Loop indefinitely in the GDB stub thread. TODO: This should be quittable!
    //std::thread gdb_thread([&gdbstub1](){ while (true) gdbstub1->ProcessQueue(); });
    setup->os.Initialize();
    //setup->os.CreateProcess(setup->cpus[0].cpu.PC(), setup->cpus[0]);

    setup->os.Run(setup, std::move(gdbstub1));
//	for (;;) {
//        Interpreter::Processor::RunUntilSVC(setup->cpus[0], *gdbstub1);
//        gdbstub1->ProcessQueue();
//        gdbstub2->ProcessQueue();
//        Interpreter::Processor::Step(setup->cpus[1]);
//    }
}
