#pragma once

#include <cstdint>
#include <vector>

namespace Interpreter {
struct CPUContext;
}

namespace IR {

enum class Op : uint16_t {
    Invalid,
//    Decode,
    Return,
    ShiftRightImm,
    ShiftLeftImm,

    ShiftLeft,
    ShiftRight,
//    RotateRightImm,
    BitwiseOr,
    BitwiseAnd,
    BitwiseXor,
    BitwiseNeg,
    EqualsImm,
    LessThanImm,
    Equal,
    NotEqual,
    GreaterEqual,
    Mov,
    Add,
    Sub,
    Mul,
    MulTo64,
    SignedMulTo64, // TODO: This is fairly ARM-specific...

    Load8,
    Load16,
    Load32,
    Store8,
    Store16,
    Store32,

    // Internal flow control
    Skip, // skip following n instructions
    If,   // run following code if condition is true, otherwise skip n instructions

    // TODO: The below operations are just for transition purposes
    LoadImm32,
};
const uint16_t num_ops = 1 + static_cast<uint32_t>(Op::LoadImm32);

// An elementary instruction in our intermediate representation bytecode
struct Instruction {
    union {
        struct {
            Op op;

            uint8_t dst_reg;
            uint8_t src1_reg;

            uint8_t src2_reg;
            uint8_t src3_reg;
            uint8_t src4_reg;
            uint8_t immediate;
        } format1;

        struct {
            Op op;

            uint8_t dst_reg;
            uint8_t pad;
            uint32_t immediate32;
        } format2;
    };
};
static_assert(sizeof(Instruction) == 8, "Instruction structure is bigger than intended");

// Index into the internal 32-bit register pool
using RegIndex = uint8_t;

// Used to manage if expressions
// TODO: Come up with a nicer interface for this!
struct IfBlock {
    unsigned index;
};

// A block of compiled instructions
class Block {
public: // TODO: Make this private again
    std::vector<Instruction> instructions;

public:
    void GenMov(uint8_t dst, uint8_t src);

    void GenAdd(uint8_t dst, uint8_t src1, uint8_t src2);

    void GenSub(uint8_t dst, uint8_t src1, uint8_t src2);

    // Multiply the given source registers and store the lower 32 bits of the result in `dst`
    void GenMul(RegIndex dst, RegIndex src1, RegIndex src2);

    // Multiply the given source registers and store the lower 32 bits of the result in `dst_low` and the upper 32 bits in `dst_high`.
    void GenMulTo64(RegIndex dst_low, RegIndex dst_high, RegIndex src1, RegIndex src2);

    // Multiply the given source registers (after sign-extension to 64 bit) and store the lower 32 bits of the result in `dst_low` and the upper 32 bits in `dst_high`.
    void GenSignedMulTo64(RegIndex dst_low, RegIndex dst_high, RegIndex src1, RegIndex src2);

    void GenBitwiseOr(uint8_t dst, uint8_t src1, uint8_t src2);

    void GenBitwiseXor(uint8_t dst, uint8_t src1, uint8_t src2);

    void GenBitwiseAnd(uint8_t dst, uint8_t src1, uint8_t src2);

    void GenBitwiseNeg(uint8_t dst, uint8_t src);

    void GenEqualsImm(uint8_t dst, uint8_t src, uint8_t imm);
    void GenLessThanImm(uint8_t dst, uint8_t src, uint8_t imm);

    void GenEqual(uint8_t dst, uint8_t src1, uint8_t src2);
    void GenNotEqual(uint8_t dst, uint8_t src1, uint8_t src2);
    void GenGreaterEqual(uint8_t dst, uint8_t src1, uint8_t src2);

    void GenShiftRightImm(uint8_t dst, uint8_t src, uint8_t imm);

    void GenShiftLeftImm(uint8_t dst, uint8_t src, uint8_t imm);

    void GenShiftRight(uint8_t dst, uint8_t value_src, uint8_t shift_src);

    void GenShiftLeft(uint8_t dst, uint8_t value_src, uint8_t shift_src);

    void GenLoad8(uint8_t dst, uint8_t addr);

    void GenLoad16(uint8_t dst, uint8_t addr);

    void GenLoad32(uint8_t dst, uint8_t addr);

    void GenStore8(uint8_t addr, uint8_t src);

    void GenStore16(uint8_t addr, uint8_t src);

    void GenStore32(uint8_t addr, uint8_t src);

    void GenLoadImm32(uint8_t dst, uint32_t imm32);

    /**
     * Skip the next num_instrs IR instructions.
     * @note Combined with GenIf, this can be used to implement an if-else-statement.
     */
    void GenSkip(uint32_t num_instrs);

    /**
     * Skip num_instrs instructions (following the generated instruction) if predicate register contains 0, continue otherwise.
     */
    IfBlock GenIf(uint8_t predicate_reg, uint32_t num_instrs);

    void EndIf(IfBlock block);

    // Executes the block
    void Execute(Interpreter::CPUContext& ctx);

    // Resets the block - TODO: Remove this method once transition is complete
    void Clear();

    std::size_t GetNumInstructions();
};

Block CompileBlock(uint32_t vaddr);

// Returns an index to the internal register pool corresponding to the given native register
uint8_t GetNativeReg(uint8_t index);

uint8_t GetCPSR_N();
uint8_t GetCPSR_Z();
uint8_t GetCPSR_C();
uint8_t GetCPSR_V();
uint8_t GetThumb();

// Table of scratch register indices into the internal register pool
// TODO: Should specify the number of array elements in the header.. use std::array instead!
// Current scratch register allocation:
// 0-2: Reserved for instruction handlers
// 5-6: Reserved for CPSR arithmetic flag emulation
// 3-4+7-10: Reserved for barrel shifter computations (8: PC value)
// 11: Current PC+8 value
// 12: Conditional execution predicate
extern const uint8_t scratch_regs[];

}  // namespace IR
