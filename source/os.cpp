#include "ipc.hpp"
#include "os.hpp"

#include "processes/hid.hpp"
#include "processes/fake_process.hpp"

#include <boost/range/algorithm/copy.hpp>

#include <fstream>
#include <iomanip>

namespace HLE {

namespace OS {

const char* GetThreadObjectName(Thread& thread) {
    return typeid(thread).name();
}

// Logs the current thread object name to std::cout and returns the stream object.
std::ostream& ThreadLogger(Thread* thread) {
    std::cout << GetThreadObjectName(*thread);
    if (auto* emuthread = dynamic_cast<EmuThread*>(thread)) {
        std::cout << ", " << emuthread;
    }
    std::cout << ": ";
    return std::cout;
}

/*class IdleThread : FakeThread {
    // TODO! Although, not sure if we actually need this thread.
};*/

// TODO: Get rid of this global...
Interpreter::ProcessorController* gdbstub = nullptr;

// TODO: Get rid of this global...
Interpreter::CPUContext* glob_ctx = nullptr;

Thread::Thread()
    : coroutine([this](boost::coroutines::symmetric_coroutine<void>::yield_type& yield) {
                    try {
                        coroutine_yield = &yield;
                        Run();
                    } catch (const std::runtime_error& err) {
                        ThreadLogger(this) << err.what() << std::endl;
                        gdbstub->NotifySegfault();

                        // TODO: Exit more gracefully?
                        // NOTE: Exiting immediately may prevent the GDB stub from catching the error!
                        std::exit(1);
                    }
                }) {
}

void Thread::SetOwningProcess(std::shared_ptr<Process> process) {
    parent_process = process;
}

std::shared_ptr<Thread> Thread::GetPointer() {
    return GetParentProcess()->GetThreadPointer(*this);
}

HandleTable& Thread::GetHandleTable() {
    return GetParentProcess()->handle_table;
}

void Thread::SignalResourceReady() {
    sleeping = false;
}

OS& Thread::GetOS() {
    return GetParentProcess()->GetOS();
}

boost::optional<uint32_t> Thread::ResolveVirtualAddr(VAddr addr) {
    // Find the first address mapping larger than addr, then move one step back to find the one containing addr
    // Possible corner cases:
    // * Empty ranges will return a begin iterator (=> abort)
    // * If the upper bound is an end iterator, moving one step back will be a valid iterator for non-empty maps
    auto it = virtual_memory.upper_bound(addr);
    if (it == virtual_memory.begin())
        return boost::none;

    // Finally, make sure the address is mapped at all
    --it;
    if (it->first + it->second.size <= addr)
        return boost::none;

    return it->second.phys_start;
}

boost::optional<uint32_t> Thread::FindAvailableVirtualMemory(uint32_t size, VAddr vaddr_start, VAddr vaddr_end) {

    VAddr candidate_range_start = vaddr_start;

    // Iterate existing mappings with increasing base address
    for (auto& addr_mapping : virtual_memory) {
        auto mapping_vaddr_start = addr_mapping.first;
        auto mapping_size = addr_mapping.second.size;

        // Ignore any mappings which don't even intersect with the candidate range
        if (mapping_vaddr_start + mapping_size < candidate_range_start)
            continue;

        // If the mapped range starts before our candidate range ends, move "start" behind and check the next mapping
        if (mapping_vaddr_start < candidate_range_start + size) {
            candidate_range_start = mapping_vaddr_start + mapping_size;
            continue;
        }

        // Otherwise, this is the one we're looking for!
        break;
    }

    if (candidate_range_start + size >= vaddr_end)
        return boost::none;

    return candidate_range_start;
}

bool Thread::MapVirtualMemory(PAddr phys_addr, uint32_t size, VAddr vaddr) {
    // First off, make sure the given range is actually valid:
    // - The first existing mapping starting at vaddr must be "far away" for "size" bytes to fit in
    // - The mapping before that one must end before or at "vaddr".
    auto it = virtual_memory.lower_bound(vaddr);
    if (it != virtual_memory.end() && it->first < vaddr + size)
        return false;

    if (it != virtual_memory.begin() && std::prev(it)->first + std::prev(it)->second.size > vaddr)
        return false;

    // Insert new mapping and invoke implementation-specific behavior
    virtual_memory.insert({vaddr, {phys_addr,size}});

    ThreadLogger(this) << "Mapped VAddr [0x" << std::hex << std::setw(8) << std::setfill('0') << vaddr
              << ";0x" << std::hex << std::setw(8) << std::setfill('0') << vaddr + size
              << "] to PAddr [0x" << std::hex << std::setw(8) << std::setfill('0') << phys_addr
              << ";0x" << std::hex << std::setw(8) << std::setfill('0') << phys_addr + size << "]" << std::endl;

    OnVirtualMemoryMapped(phys_addr, size, vaddr);
    return true;
}

EmuThread::EmuThread(Interpreter::CPUContext& ctx, VAddr tls) : ctx(ctx), tls(tls) {
    // Initialize virtual memory mappings according to ctx
    for (auto& fake_mapping : ctx.fake_addr_map)
        MapVirtualMemory(fake_mapping.second.start, fake_mapping.second.size, fake_mapping.first);

    SaveContext(ctx);
}

void EmuThread::SaveContext(const Interpreter::CPUContext& ctx2) {
    boost::copy(ctx.cpu.reg, std::begin(regs));
    // TODO: There will be more state to save in the future!
}

void EmuThread::RestoreContext(Interpreter::CPUContext& ctx2) {
    boost::copy(regs, std::begin(ctx.cpu.reg));
    // TODO: There will be more state to save in the future!

    ctx.cpu.cp15.ThreadLocalStorage().virtual_addr = tls;
}

uint32_t EmuThread::ReadTLS(uint32_t offset) {
    return Interpreter::Processor::ReadVirtualMemory<uint32_t>(ctx, tls + offset);
}

void EmuThread::WriteTLS(uint32_t offset, uint32_t value) {
    Interpreter::Processor::WriteVirtualMemory<uint32_t>(ctx, tls + offset, value);
}

void EmuThread::WriteMemory(VAddr addr, uint8_t value) {
    Interpreter::Processor::WriteVirtualMemory<uint8_t>(ctx, addr, value);
}

uint8_t EmuThread::ReadMemory(VAddr addr) {
    return Interpreter::Processor::ReadVirtualMemory<uint8_t>(ctx, addr);
}

void EmuThread::OnVirtualMemoryMapped(PAddr phys_addr, uint32_t size, VAddr vaddr) {
    ctx.fake_addr_map.insert({vaddr, {phys_addr,size}});
}

void EmuThread::Run() {
    Interpreter::Processor::Run(ctx, *gdbstub);
}

uint32_t FakeThread::AllocateStaticBuffer(uint32_t size) {
    StaticBuffer buffer;
    buffer.data.resize(size);

    // Place the first buffer at address 0, and append new buffers at the end of the previous one
    uint32_t start_addr = static_buffers.empty() ? 0 :
                                (static_buffers.rbegin()->first + static_buffers.rbegin()->second.data.size());

    static_buffers.insert({start_addr, buffer});
    return start_addr;
}

void FakeThread::FreeStaticBuffer(uint32_t addr) {
    static_buffers.erase(addr);
}

void FakeThread::WriteMemory(VAddr addr, uint8_t value) {
    for (auto static_buffer : static_buffers) {
        if (addr >= static_buffer.first &&
                addr < static_buffer.first + static_buffer.second.data.size()) {
            static_buffer.second.data[addr - static_buffer.first] = value;
            return;
        }
    }
    throw std::runtime_error("Tried to write outside FakeThread's fake address range");
}

uint8_t FakeThread::ReadMemory(VAddr addr) {
    for (auto static_buffer : static_buffers) {
        if (addr >= static_buffer.first &&
                addr < static_buffer.first + static_buffer.second.data.size()) {
            return static_buffer.second.data[addr - static_buffer.first];
        }
    }
    throw std::runtime_error("Tried to read outside FakeThread's fake address range");
}

void FakeThread::OnVirtualMemoryMapped(PAddr phys_addr, uint32_t size, VAddr vaddr) {
    // Do nothing
}

FakePort::FakePort(const std::string& name, uint32_t max_sessions) : name(name), max_sessions(max_sessions) {
}

HandleTable::Entry<ServerPort> FakePort::Setup() {
    HandleTable::Entry<ServerPort> server_port;
    HandleTable::Entry<ClientPort> client_port;
    OS::Result result;
    std::tie(result,server_port,client_port) = GetOS().SVCCreatePort(*this, name, OS::MAX_SESSIONS);
    if (result != RESULT_OK)
        GetOS().SVCBreak(*this, OS::BreakReason::Panic);

    // Close the client port, since we don't need it
    result = GetOS().SVCCloseHandle(*this, client_port.first);
    if (result != RESULT_OK)
        GetOS().SVCBreak(*this, OS::BreakReason::Panic);

    return server_port;
}

std::string FakePort::GetInternalName() const {
    return "FAKEPORT \"" + name + "\"";
}

void FakePort::UnknownRequest(const IPC::CommandHeader& header) {
    // Return error
    WriteTLS(0x80, IPC::CommandHeader::Make(0, 1, 0).raw);
    WriteTLS(0x84, -1);
    ThreadLogger(this) << GetInternalName() << " received unknown command id 0x" << std::hex << header.command_id.Value() << std::endl;

    // We could just continue, but apparently most programs do not actually check for errors appropriately
    // TODO: Once we reach sufficient level of implementation completeness, consider removing this panic.
    GetOS().SVCBreak(*this, OS::BreakReason::Panic);
}

void FakePort::LogStub(const IPC::CommandHeader& header) {
    ThreadLogger(this) << GetInternalName() << " sending stub reply for command id 0x" << std::hex << header.command_id.Value() << std::endl;
}

void FakePort::LogReturnedHandle(Handle handle) {
    ThreadLogger(this) << GetInternalName() << " returning handle " << std::dec << handle << std::endl;
}

void FakePort::Run() {
    port = Setup();

    ThreadLogger(this) << GetInternalName() << " using server port handle " << port.first << std::endl;

    // List of handles to synchronize against
    std::vector<Handle> handle_table = { port.first };
    Handle last_signalled = HANDLE_INVALID;

    for (;;) {
        OS::Result result;
        int32_t index;
        ThreadLogger(this) << GetInternalName() << " ReplyAndReceive" << std::endl;
        std::tie(result,index) = GetOS().SVCReplyAndReceive(*this, handle_table.data(), handle_table.size(), last_signalled);
        last_signalled = HANDLE_INVALID;
        if (result != RESULT_OK)
            GetOS().SVCBreak(*this, OS::BreakReason::Panic);

        if (index == 0) {
            // ServerPort: Incoming client connection

            std::shared_ptr<ServerSession> session;
            Handle session_handle;
            // TODO: Does forward_as_tuple work as we expect?
            ThreadLogger(this) << GetInternalName() << " AcceptSession" << std::endl;
            std::forward_as_tuple(result,std::tie(session_handle,session)) = GetOS().SVCAcceptSession(*this, *port.second);
            if (result != RESULT_OK)
                GetOS().SVCBreak(*this, OS::BreakReason::Panic);

            // If maximum number of sessions is exhausted, close handle again.
            if (handle_table.size() < OS::MAX_SESSIONS+1) {
                handle_table.push_back(session_handle);
            } else {
                GetOS().SVCCloseHandle(*this, session_handle);
            }
        } else {
            // server_session: Incoming IPC command from the indexed client
            ThreadLogger(this) << GetInternalName() << " received IPC request" << std::endl;
            IPC::CommandHeader header = { ReadTLS(0x80) };
            OnIPCRequest(handle_table[index], header);
            last_signalled = handle_table[index];
        }
    }

    // Clean up
    for (auto handle : handle_table)
        GetOS().SVCCloseHandle(*this, handle);

    GetOS().SVCExitThread(*this);
}

class ServiceManager final : public FakePort {
    // Clients registered through RegisterClient
    // Maps ServiceManager-client ServerSession handles to client process handles
    // TODO: if PID is larger than 5 (or so), not all services may be accessed!
    std::map<Handle,Handle> registered_clients;

    // Services registered through RegisterService
    // Maps service names (UTF-8) to ServerPorts.
    std::map<std::string,HandleTable::Entry<ClientPort>> registered_services;

    void OnIPCRequest(Handle sender, const IPC::CommandHeader& header) override {
        switch (header.command_id) {
        case 1:  // RegisterClient
        {
            ThreadLogger(this) << GetInternalName() + " received RegisterClient" << std::endl;

            // Validate command
            if (header.num_normal_params != 0 ||
                    header.size_translate_params != 2 ||
                    ReadTLS(0x84) != IPC::TranslationDescriptor::MakeProcessHandle().raw) {
                GetOS().SVCBreak(*this, OS::BreakReason::Panic);
            }

            Handle process_handle = ReadTLS(0x88);
            registered_clients.insert({sender, process_handle});
            WriteTLS(0x80, IPC::CommandHeader::Make(0, 1, 0).raw);
            WriteTLS(0x84, RESULT_OK);

            break;
        }

        case 3: // RegisterService
        {
            ThreadLogger(this) << GetInternalName() + " received RegisterService" << std::endl;

            uint32_t name_length = ReadTLS(0x8c);

            // Validate command
            if (header.num_normal_params != 4 ||
                    header.size_translate_params != 0 ||
                    name_length > 8) {
                GetOS().SVCBreak(*this, OS::BreakReason::Panic);
            }

            // TODO: Clean this up by introducing a ReadTLS<uint8_t> variant!
            std::string name;
            for (unsigned i = 0; i < name_length; ++i) {
                name.push_back(ReadTLS(0x84 + (i/4)*4) >> (8*(i%4)));
            }
            ThreadLogger(this) << GetInternalName() + " registering service \"" << name << "\" (";
            for (auto c : name)
                std::cout << std::hex << std::setw(2) << std::setfill('0') << (unsigned)c;
            std::cout << "; length " << name_length << ")" << std::endl;

            uint32_t max_sessions = ReadTLS(0x90);

            OS::Result result;
            HandleTable::Entry<ServerPort> server_port;
            HandleTable::Entry<ClientPort> client_port;
            std::tie(result, server_port, client_port) = GetOS().SVCCreatePort(*this, "", max_sessions);
            if (result != RESULT_OK)
                GetOS().SVCBreak(*this, OS::BreakReason::Panic);

            registered_services.insert({name, client_port });

            // Write reply - have the kernel close our ServerPort handle since we won't need it anymore.
            WriteTLS(0x80, IPC::CommandHeader::Make(0, 1, 2).raw);
            WriteTLS(0x84, RESULT_OK);
            WriteTLS(0x88, IPC::TranslationDescriptor::MakeHandles(1, true).raw);
            WriteTLS(0x8c, server_port.first);

            break;
        }

        case 5: // GetServiceHandle
        {
            ThreadLogger(this) << GetInternalName() + " received GetServiceHandle" << std::endl;
            uint32_t name_length = ReadTLS(0x8c);

            // Validate command - TODO: bit0 of word 4 allows for returning the port if a session isn't available
            if (header.num_normal_params != 4 ||
                    header.size_translate_params != 0 ||
                    name_length > 8) {
                GetOS().SVCBreak(*this, OS::BreakReason::Panic);
            }

            // TODO: Clean this up by introducing a ReadTLS<uint8_t> variant!
            std::string name;
            for (unsigned i = 0; i < name_length; ++i) {
                char c = ReadTLS(0x84 + (i/4)*4) >> (8*(i%4));
                if (c == 0)
                    break;
                name.push_back(c);
            }
            ThreadLogger(this) << GetInternalName() + " getting handle of service \"" << name << "\" (";
            for (auto c : name)
                std::cout << std::hex << std::setw(2) << std::setfill('0') << (unsigned)c;
            std::cout << "; length " << name_length << ")" << std::endl;

            // Look up service name
            if (!registered_services.count(name)) {
                ThreadLogger(this) << GetInternalName() + " -- service not registered!" << std::endl;
                GetOS().SVCBreak(*this, OS::BreakReason::Panic);
            }

            // Create a new session to the service
            auto client_port_handle = registered_services.at(name).first;
            OS::Result result;
            HandleTable::Entry<ClientSession> client_session;
            std::tie(result, client_session) = GetOS().SVCCreateSessionToPort(*this, client_port_handle);
            if (result != RESULT_OK)
                GetOS().SVCBreak(*this, OS::BreakReason::Panic);

            // Write reply - have the kernel close the session handle since we won't need it anymore
            WriteTLS(0x80, IPC::CommandHeader::Make(0, 1, 2).raw);
            WriteTLS(0x84, RESULT_OK);
            WriteTLS(0x88, IPC::TranslationDescriptor::MakeHandles(1, true).raw);
            WriteTLS(0x8c, client_session.first);

            break;
        }

        default:
            UnknownRequest(header);
        }
    }

public:
    ServiceManager() : FakePort("srv:", OS::MAX_SESSIONS) {
    }

    virtual ~ServiceManager() = default;
};

FakeService::FakeService(const std::string& name, uint32_t max_sessions) : FakePort("", max_sessions), name(name), max_sessions(max_sessions) {
}

HandleTable::Entry<ServerPort> FakeService::Setup() {
    ThreadLogger(this) << GetInternalName() << " ConnectToPort \"srv:\"" << std::endl;

    OS::Result result;
    Handle srv_handle;
    std::shared_ptr<ClientSession> srv_session;
    std::forward_as_tuple(result,std::tie(srv_handle,srv_session)) = GetOS().SVCConnectToPort(*this, "srv:");
    if (result != RESULT_OK)
        GetOS().SVCBreak(*this, OS::BreakReason::Panic);

    // Send RegisterService request
    // TODO: Currently, we never request UnregisterService!
    WriteTLS(0x80, IPC::CommandHeader::Make(3, 4, 0).raw);

    // TODO: Clean this up by introducing WriteTLS<uint8_t> variants
    // TODO: This isn't even portable :(
    uint32_t value = 0;
    for (int i = 0; i < 8; ++i) {
        char c = (i < name.length()) ? name[i] : 0;
        value = (c << 24) | (value >> 8);
        if (i == 3 || i == 7) {
            WriteTLS(0x84 + (i / 4) * 4, value);
            value = 0;
        }
    }

    WriteTLS(0x8c, std::min<size_t>(8, name.length()));
    WriteTLS(0x90, max_sessions);

    ThreadLogger(this) << GetInternalName() << " SendSyncRequest RegisterService" << std::endl;
    result = GetOS().SVCSendSyncRequest(*this, srv_handle);
    if (result != RESULT_OK)
        GetOS().SVCBreak(*this, OS::BreakReason::Panic);

    GetOS().SVCCloseHandle(*this, srv_handle);

    // Read RegisterService reply
    result = ReadTLS(0x84);
    if (result != RESULT_OK)
        GetOS().SVCBreak(*this, OS::BreakReason::Panic);

    Handle service_handle = ReadTLS(0x8c);
    auto service_port = GetHandleTable().FindObject<ServerPort>(service_handle);

    ThreadLogger(this) << GetInternalName() << " setup done" << std::endl;
    return { service_handle, service_port };
}

std::string FakeService::GetInternalName() const {
    return "FAKESERVICE \"" + name + "\"";
}

class FakeAPT : public FakeService {
public:
    FakeAPT() : FakeService("APT:U", 1) {
    }

    virtual ~FakeAPT() = default;

    void OnIPCRequest(Handle sender, const IPC::CommandHeader& header) override {
        switch (header.command_id) {
        case 0x1:  // GetLockHandle
        {
            ThreadLogger(this) << GetInternalName() + " received GetLockHandle" << std::endl;

            OS::Result result;
            HandleTable::Entry<Mutex> apt_mutex;
            std::tie(result,apt_mutex) = GetOS().SVCCreateMutex(*this, false);
            if (result != RESULT_OK)
                GetOS().SVCBreak(*this, OS::BreakReason::Panic);

            LogStub(header);
            WriteTLS(0x80, IPC::CommandHeader::Make(0, 3, 2).raw);
            WriteTLS(0x84, RESULT_OK);
            WriteTLS(0x88, 0); // 3dbrew: AppletAttr
            WriteTLS(0x8c, 0); // 3dbrew: APT State

            // Return a dummy handle: Allows the client to close the handle, but any other operation will fail
            WriteTLS(0x90, IPC::TranslationDescriptor::MakeHandles(1, true).raw);
            WriteTLS(0x94, apt_mutex.first);
            break;
        }

        case 0x2: // Initialize
        {
            uint32_t app_id = ReadTLS(0x84);
            uint32_t applet_attr = ReadTLS(0x88);
            ThreadLogger(this) << GetInternalName() + " received Initialize with AppID=0x" << std::hex << app_id << ", AppletAttr=0x" << std::hex << applet_attr << std::endl;

            LogStub(header);
            WriteTLS(0x80, IPC::CommandHeader::Make(0, 1, 3).raw);
            WriteTLS(0x84, RESULT_OK);

            // For now, we create these events but never report them. TODO!
            auto notification_event = GetOS().SVCCreateEvent(*this, ResetType::OneShot);
            auto resume_event = GetOS().SVCCreateEvent(*this, ResetType::OneShot);

            if (notification_event.first != RESULT_OK || resume_event.first != RESULT_OK)
                GetOS().SVCBreak(*this, OS::BreakReason::Panic);

            // Return dummy handles: Allows the client to close the handles, but any other operation will fail
            WriteTLS(0x88, IPC::TranslationDescriptor::MakeHandles(2, true).raw);
            WriteTLS(0x8c, notification_event.second.first);
            WriteTLS(0x90, resume_event.second.first);
            break;
        }

        case 0x3: // Enable
        {
            uint32_t applet_attr = ReadTLS(0x84);
            ThreadLogger(this) << GetInternalName() + " received Enable with AppletAttr=0x" << std::hex << applet_attr << std::endl;

            LogStub(header);
            WriteTLS(0x80, IPC::CommandHeader::Make(0, 1, 0).raw);
            WriteTLS(0x84, RESULT_OK);
            break;
        }

        case 0x4: // Finalize
        {
            uint32_t app_id = ReadTLS(0x84);
            ThreadLogger(this) << GetInternalName() + " received Initialize with AppID=0x" << std::hex << app_id << std::endl;

            LogStub(header);
            WriteTLS(0x80, IPC::CommandHeader::Make(0, 1, 0).raw);
            WriteTLS(0x84, RESULT_OK);
            break;
        }

        case 0x43: // NotifyToWait
        {
            uint32_t app_id = ReadTLS(0x84);
            ThreadLogger(this) << GetInternalName() + " received NotifyToWait with AppID=0x" << std::hex << app_id << std::endl;

            LogStub(header);
            WriteTLS(0x80, IPC::CommandHeader::Make(0, 1, 0).raw);
            WriteTLS(0x84, RESULT_OK);
            break;
        }

        case 0x4b: // AppletUtility
        {
            uint32_t func = ReadTLS(0x84);
            uint32_t size_in = ReadTLS(0x88);
            uint32_t size_out = ReadTLS(0x8c);
            ThreadLogger(this) << GetInternalName() + " received AppletUtility with func=0x" << std::hex << func
                               << ", size_in=0x" << size_in << ", size_out=0x" << size_out << std::endl;

            LogStub(header);
            WriteTLS(0x80, IPC::CommandHeader::Make(0, 1, 0).raw);
            WriteTLS(0x84, RESULT_OK);
            break;
        }

        default:
            UnknownRequest(header);
        }
    }
};

class FakeGSP : public FakeService {
    const uint32_t hwregs_buffer;

    HandleTable::Entry<Event> shared_mem_ready{HANDLE_INVALID,nullptr};

    // index of the next thread to call RegisterInterruptRelayQueue
    uint32_t thread_index = 0;

public:
    FakeGSP() : FakeService("gsp::Gpu", 1), hwregs_buffer(AllocateStaticBuffer(0x80)) {
        // Set up static buffers
        WriteTLS(0x180, IPC::TranslationDescriptor::MakeStaticBuffer(0, 0x80).raw);
        WriteTLS(0x184, hwregs_buffer);
    }

    virtual ~FakeGSP() = default;

    void OnIPCRequest(Handle sender, const IPC::CommandHeader& header) override {
        switch (header.command_id) {
        case 0x1: // WriteHWRegs
        {
            uint32_t address = 0x1EB00000 + ReadTLS(0x84);
            uint32_t size = ReadTLS(0x88);
            ThreadLogger(this) << GetInternalName() << " received WriteHWRegs"
                               << ", address=0x" << std::hex << std::setw(8) << std::setfill('0') << address
                               << ", size=0x" << size << std::endl;

            LogStub(header);
            WriteTLS(0x80, IPC::CommandHeader::Make(0, 1, 0).raw);
            WriteTLS(0x84, RESULT_OK);
            break;
        }

        case 0x4: // ReadHWRegs
        {
            uint32_t address = 0x1EB00000 + ReadTLS(0x84);
            uint32_t size = ReadTLS(0x88);
            ThreadLogger(this) << GetInternalName() << " received ReadHWRegs"
                               << ", address=0x" << std::hex << std::setw(8) << std::setfill('0') << address
                               << ", size=0x" << size << std::endl;

            LogStub(header);
            WriteTLS(0x80, IPC::CommandHeader::Make(0, 1, 0).raw);
            WriteTLS(0x84, RESULT_OK);
            break;
        }

        case 0xb: // SetLcdForceBlack
        {
            ThreadLogger(this) << GetInternalName() << " received SetLcdForceBlack" << std::endl;

            LogStub(header);
            WriteTLS(0x80, IPC::CommandHeader::Make(0, 1, 0).raw);
            WriteTLS(0x84, RESULT_OK);
            break;
        }

        case 0xc: // TriggerCmdReqQueue
        {
            ThreadLogger(this) << GetInternalName() << " received TriggerCmdReqQueue" << std::endl;

            // Validate request
            if (header.raw != 0x000C0000)
                GetOS().SVCBreak(*this, OS::BreakReason::Panic);

            // TODO: Process actual commands :)

            LogStub(header);
            WriteTLS(0x80, IPC::CommandHeader::Make(0, 1, 0).raw);
            WriteTLS(0x84, RESULT_OK);
            break;
        }

        case 0x13: // RegisterInterruptRelayQueue
        {
            uint32_t flags = ReadTLS(0x84);
            Handle event_handle = ReadTLS(0x8c);
            ThreadLogger(this) << GetInternalName() << " received RegisterInterruptRelayQueue, flags=" << std::hex << flags
                               << ", handle=" << event_handle << std::endl;

            if (header.raw != 0x00130042) {
                // Unsupported command header
                GetOS().SVCBreak(*this, OS::BreakReason::Panic);
            }

            // Create shared memory block;
            // Size is hardcoded to 0x1000, but ideally should be adjusted according to the number of GSP client threads
            OS::Result result;
            VAddr shared_mem_vaddr;
            const uint32_t shared_mem_size = 0x1000;
            std::tie(result,shared_mem_vaddr) = GetOS().SVCControlMemory(*this, 0, 0, shared_mem_size, 3/*ALLOC*/, 0x3/*RW*/);
            if (result != RESULT_OK)
                GetOS().SVCBreak(*this, OS::BreakReason::Panic);

            HandleTable::Entry<SharedMemoryBlock> shared_memory;
            std::tie(result,shared_memory) = GetOS().SVCCreateMemoryBlock(*this, shared_mem_vaddr, shared_mem_size, 0x3/*RW*/, 0x3/*RW*/);

            LogStub(header);
            WriteTLS(0x80, IPC::CommandHeader::Make(0, 2, 2).raw);
            WriteTLS(0x84, RESULT_OK);
            WriteTLS(0x88, thread_index++);
            WriteTLS(0x8c, IPC::TranslationDescriptor::MakeHandles(1).raw);
            WriteTLS(0x90, shared_memory.first);

            // Not really sure what to prepare currently, so just signal that we are ready.
            ThreadLogger(this) << GetInternalName() << " Signalling GSP event..." << std::endl;
            GetOS().SVCSignalEvent(*this, event_handle);
            ThreadLogger(this) << GetInternalName() << "... done!" << std::endl;

            break;
        }

        case 0x16: // AcquireRight
        {
            ThreadLogger(this) << GetInternalName() << " received AcquireRight" << std::endl;

            LogStub(header);
            WriteTLS(0x80, IPC::CommandHeader::Make(0, 1, 0).raw);
            WriteTLS(0x84, RESULT_OK);
            break;
        }

        default:
            UnknownRequest(header);
        }
    }
};

// First thread started by the OS: Takes care of setting up the system services
class BootThread : public FakeThread {
    Interpreter::CPUContext& ctx;

public:
    BootThread(Interpreter::CPUContext& ctx) : ctx(ctx) {
    }

    virtual ~BootThread() = default;

    void Run() override {
#if 0 // Disabled for the ARM-emu only version.
        ThreadLogger(this) << "FakeThread \"BootThread\" spawning Port \"srv:\"" << std::endl;

        auto srv = std::make_shared<ServiceManager>();
        GetOS().MakeProcessFromThread(srv);

        // TODO: Wait until it actually booted! As a workaround, we currently just switch to the scheduler for a bunch of times...
        GetOS().SVCDoNothing(*this);
        GetOS().SVCDoNothing(*this);
        GetOS().SVCDoNothing(*this);
        GetOS().SVCDoNothing(*this);
        GetOS().SVCDoNothing(*this);
        GetOS().SVCDoNothing(*this);
        GetOS().SVCDoNothing(*this);
        GetOS().SVCDoNothing(*this);
        GetOS().SVCDoNothing(*this);
        GetOS().SVCDoNothing(*this);

        ThreadLogger(this) << "FakeThread \"BootThread\" spawning Service \"APT:U\"" << std::endl;
        GetOS().MakeProcessFromThread(std::make_shared<FakeAPT>());

        ThreadLogger(this) << "FakeThread \"BootThread\" spawning Service \"gsp::Gpu\"" << std::endl;
        GetOS().MakeProcessFromThread(std::make_shared<FakeGSP>());

        ThreadLogger(this) << "FakeThread \"BootThread\" spawning Service \"hid:USER\"" << std::endl;
        GetOS().MakeProcessFromThread(std::make_shared<FakeHID>());

        // TODO: Wait until it actually booted! As a workaround, we currently just switch to the scheduler for a bunch of times...
        GetOS().SVCDoNothing(*this);
        GetOS().SVCDoNothing(*this);
        GetOS().SVCDoNothing(*this);
        GetOS().SVCDoNothing(*this);
        GetOS().SVCDoNothing(*this);
        GetOS().SVCDoNothing(*this);
        GetOS().SVCDoNothing(*this);
        GetOS().SVCDoNothing(*this);
        GetOS().SVCDoNothing(*this);
        GetOS().SVCDoNothing(*this);
        GetOS().SVCDoNothing(*this);
        GetOS().SVCDoNothing(*this);
        GetOS().SVCDoNothing(*this);
#endif
        // Spawn actual emulation thread... TODO: Make this less hardcoded!
        ThreadLogger(this) << "FakeThread \"BootThread\" spawning EmuThread" << std::endl;
        GetOS().CreateProcess(ctx.cpu.PC(), ctx);

        ThreadLogger(this) << "FakeThread \"BootThread\" exiting" << std::endl;
        GetOS().SVCExitThread(*this);
    }
};

void ObserverSubject::Signal() {
    for (auto& observer : observers) {
        auto ptr = observer.lock();
        if (ptr)
            ptr->SignalResourceReady();
    }
}

bool ObserverSubject::TryAcquire(std::shared_ptr<Thread> thread) {
    bool ret = TryAcquireImpl(thread);
    if (ret) {
        // This should always find a match
        auto it = std::find_if(observers.begin(), observers.end(), [&thread](auto& observer) { return observer.lock() == thread; });
        observers.erase(it);
    }
    return ret;
}

void ObserverSubject::Register(std::shared_ptr<Thread> thread) {
    observers.push_back(thread);
}

void ObserverSubject::Unregister(std::shared_ptr<Thread> thread) {
    // This should always find a match
    auto it = std::find_if(observers.begin(), observers.end(), [&thread](auto& observer) { return observer.lock() == thread; });
    observers.erase(it);
}

bool ServerPort::TryAcquireImpl(std::shared_ptr<Thread>) {
    return !session_queue.empty();
}

void ServerPort::OfferSession(std::shared_ptr<ServerSession> session) {
    session_queue.push(session);
    Signal();
}

bool ServerSession::TryAcquireImpl(std::shared_ptr<Thread>) {
    if (ipc_commands_pending) {
        return ipc_commands_pending--;
    } else {
        return false;
    }
}

void ServerSession::CommandReady() {
    ++ipc_commands_pending;
    Signal();
}

bool ClientSession::TryAcquireImpl(std::shared_ptr<Thread>) {
    bool ret = ready;
    ready = false;
    return ret;
}

void ClientSession::SetReady() {
    ready = true;
    thread.reset();
    Signal();
}

bool Mutex::TryAcquireImpl(std::shared_ptr<Thread> thread) {
    if (locked && owner.lock())
        return false;

    owner = thread;
    locked = true;
    return true;
}

void Mutex::Release() {
    owner.reset();
    locked = false;
    Signal();
}

bool Mutex::IsOwner(std::shared_ptr<Thread> thread) const {
    return thread == owner.lock();
}

bool Event::TryAcquireImpl(std::shared_ptr<Thread> thread) {
    if (!signalled)
        return false;

    if (type == ResetType::OneShot) {
        signalled = false;
    } else if (type == ResetType::Sticky) {
        // Don't reset the signal
    } else {
        // We shouldn't be able to create other events
        throw std::runtime_error("Event shouldn't have a type different than OneShot and Sticky");
    }

    return true;
}

void Event::SignalEvent() {
    signalled = true;
}

void Event::ResetEvent() {
    signalled = false;
    Signal();
}

MemoryManager::MemoryManager() {
    // FCRAM
    region_app.region_start_paddr = 0x20000000;
    region_app.region_size_bytes  = 0x04000000;
    region_app.free[region_app.region_start_paddr] = { region_app.region_size_bytes };
}

boost::optional<uint32_t> MemoryManager::AllocateBlock(uint32_t size_bytes) {
    for (auto block_it = region_app.free.begin(); block_it != region_app.free.end(); ++block_it) {
        const uint32_t addr = block_it->first;
        const uint32_t free_size = block_it->second.size_bytes;

        if (free_size < size_bytes)
            continue;

        // Declare new "taken" memory block
        region_app.free.erase(block_it);
        region_app.taken[addr] = {size_bytes};

        // Add "free" entry for remaining space
        if (free_size != size_bytes)
            region_app.free[addr+size_bytes] = {free_size - size_bytes};

        std::cout << "MemoryManager allocated new block at 0x" << std::hex << std::setw(8) << std::setfill('0') << addr << std::endl;

        return addr;
    }
    return {};
}

void OS::Initialize() {
    // TODO: Create idle process
}

void OS::CreateProcess(uint32_t entry_point_virtual, Interpreter::CPUContext& ctx) {
    // TODO: Don't hardcode TLS region like that!
    auto thread = std::make_shared<EmuThread>(ctx, 0x27f00800);
    thread->context.regs[15] = entry_point_virtual;
    MakeProcessFromThread(thread);
}

std::shared_ptr<Process> OS::MakeProcessFromThread(std::shared_ptr<Thread> thread) {
    auto process = std::make_shared<Process>(*this);
    process->threads.push_back(thread);
    process_handles.CreateHandle(process);

    // The current Process handle is accessible through a fixed constant
    process->handle_table.CreateEntry<Process>(0xFFFF8001, process);

    thread->SetOwningProcess(process);
    threads.push_back(thread);
    return process;
}

void HandleTable::ErrorWrongType(std::shared_ptr<Object> object, const char* requested_type) {
    auto& obj = *object;
    std::cout << "Requested type \"" << requested_type << "\", but found object has type \"" << typeid(obj).name() << "\"" << std::endl;
}

void HandleTable::CloseHandle(Handle handle) {
    auto it = table.find(handle);
    if (it == table.end())
        throw std::runtime_error("Tried to close handle " + std::to_string(handle) + " that is not in the handle table");

    table.erase(table.find(handle));
}

Process::Process(OS& os) : os(os) {
}

std::shared_ptr<Thread> Process::GetThreadPointer(Thread& thread) {
    // This is guaranteed to match since we register all threads to the OS.
    return *std::find_if(threads.begin(), threads.end(), [&thread](auto& cur) { return cur.get() == &thread; });
}

long Process::GetThreadUseCount(Thread& thread) {
    auto it = std::find_if(threads.begin(), threads.end(), [&thread](auto& cur) { return cur.get() == &thread; });
    return it->use_count();
}

OS::OS() : scheduler_coro([this](boost::coroutines::symmetric_coroutine<void>::yield_type& yield) { StartScheduler({yield}); }) {
}

std::pair<OS::Result,uint32_t> OS::SVCControlMemory(Thread& source, uint32_t addr0, uint32_t addr1, uint32_t size, uint32_t operation, uint32_t permissions) {
    ThreadLogger(&source) << "SVCControlMemory: addr0=0x" << std::hex << std::setw(8) << std::setfill('0') << addr0
                          << ", addr1=0x" << std::setw(8) << std::setfill('0') << addr1
                          << ", size=0x" << size << ", op=" << operation << ", perm=" << permissions << std::endl;

    // NOTE: The memory region flags (mask 0x300) are only used for process id 1 (loader)

    switch (operation & 0xFF) {
    case 3: // Allocate new memory block
    {
        auto block_address_opt = memory.AllocateBlock(size);
        if (!block_address_opt)
            SVCBreak(source, BreakReason::Panic);

        // Set virtual address region according to the LINEAR flag
        // TODO: For LINEAR memory, firmware 8.x titles and higher use the address range 0x30000000-0x38000000
        VAddr vaddr_start = (operation & 0x10000) ? 0x14000000 : 0x08000000;
        VAddr vaddr_end = vaddr_start + 0x08000000;
        auto vaddr_opt = source.FindAvailableVirtualMemory(size, vaddr_start, vaddr_end);
        if (!vaddr_opt)
            SVCBreak(source, BreakReason::Panic);

        if (!source.MapVirtualMemory(*block_address_opt, size, *vaddr_opt))
            SVCBreak(source, BreakReason::Panic);

        return { RESULT_OK, *vaddr_opt };
    }

    default:
        // Not implemented
        return { -1, 0 };
    }
}

void OS::SVCExitProcess(std::shared_ptr<Process> source) {
    std::cout << "SVCExitProcess" << std::endl;

    auto handle = process_handles.FindHandle(source);
    if (handle == HANDLE_INVALID)
        throw std::runtime_error("Couldn't find handle in process which should have been destroyed...");

    process_handles.CloseHandle(handle);

    handle = source->handle_table.FindHandle(source);
    if (handle == HANDLE_INVALID)
        throw std::runtime_error("Couldn't find handle in process which should have been destroyed...");

    // TODO: WE REALLY MUST MOVE THIS DESTRUCTION CODE INTO THE SCHEDULER.
    //       Otherwise, there is no coroutine information we could use to move back there after destroying this process.
    //SVCCloseHandle();
    source->handle_table.CloseHandle(handle);
}

std::pair<OS::Result,HandleTable::Entry<Thread>> OS::SVCCreateThread(Thread& source, uint32_t entry, uint32_t arg, uint32_t stack_top, uint32_t priority, uint32_t processor_id) {
    ThreadLogger(&source) << "SVCCreateThread, entry=0x" << std::hex << std::setw(8) << std::setfill('0') << entry
                          << ", arg=0x" << arg << ", stack=0x" << stack_top << ", prio=0x" << priority
                          << ", proc_id=" << processor_id << ", PC=0x" << dynamic_cast<EmuThread&>(source).ctx.cpu.PC() << std::endl;

    auto& emu_thread = dynamic_cast<EmuThread&>(source);

    auto thread_ptr = std::make_shared<EmuThread>(emu_thread.ctx, 0);
    thread_ptr->regs[15] = entry;

    thread_ptr->SetOwningProcess(source.GetParentProcess());
    thread_ptr->virtual_memory = source.virtual_memory; // TODO ... Don't just copy this. But the virtual memory mapping will be shared between threads of the same Process soon anyway!

    // Allocate us some nice TLS - TODO: This should all be done by Process instead, and then passed as a parameter to the EmuThread constructor
    auto paddr_opt = memory.AllocateBlock(0x200);
    auto vaddr_opt = thread_ptr->FindAvailableVirtualMemory(0x200, 0x03000000, 0x04000000);
    thread_ptr->MapVirtualMemory(*paddr_opt, 0x200, *vaddr_opt);
    thread_ptr->tls = *vaddr_opt;

    source.GetParentProcess()->threads.push_back(thread_ptr);
    auto thread = source.GetHandleTable().CreateHandle(thread_ptr);
    threads.push_back(thread_ptr);

    SwitchToSchedulerFromThread(source);
    return { RESULT_OK, thread };
}

void OS::SVCExitThread(Thread& source) {
    ThreadLogger(&source) << "SVCExitThread" << std::endl;

    // Unregister from all wait objects to make sure we don't run into cyclic dependencies
    for (auto& notifier : source.wait_list)
        notifier.second->Unregister(source.GetPointer());

    source.wait_list.clear();

    // Make sure our owning Process is the only one still referencing this thread - otherwise, we *will* be leaking memory
    // TODO: Reenable this check once it works!
    // TODO: The leftover reference might be in the scheduler, where it keeps a shared_ptr to the current thread around :/
    //if (source.GetParentProcess()->GetThreadUseCount(source) != 1)
    //    SVCBreak(source, BreakReason::Panic);

    auto parent_process = source.parent_process.lock();
    auto& process_threads = parent_process->threads;

    // TODO: Instead of anything below, put this thread into a "stop" state and let the thread dispatcher delete it!
    //       The current code (below) isn't actually safe because we delete the current coroutine from within the coroutine...
    auto it = std::find_if(process_threads.begin(), process_threads.end(), [&source](auto& cur) { return cur.get() == &source; });
    if (it == process_threads.end())
        SVCBreak(source, BreakReason::Panic);
    process_threads.erase(it);

    // TODO: Actually, the scheduler should automatically remove the threads from its list when their refcount gets to zero.
    //       However, we don't currently manage to get it to zero, hence we need to manually delete it here...
    auto it2 = std::find_if(threads.begin(), threads.end(), [&source](auto& cur) { return cur.lock().get() == &source; });
    if (it2 == threads.end())
        SVCBreak(source, BreakReason::Panic);
    threads.erase(it2);



    // TODO: If this was the last thread, exit the whole process
    /*if (process_threads.empty()) {
        // If this is the last process thread, exit the whole process
        SVCExitProcess(parent_process);
    }*/

    SwitchToSchedulerFromThread(source);
}

std::pair<OS::Result,HandleTable::Entry<Mutex>> OS::SVCCreateMutex(Thread& source, bool lock) {
    ThreadLogger(&source) << "SVCCreateMutex: lock=" << lock << std::endl;
    auto mutex = source.GetHandleTable().CreateHandle<Mutex>(std::make_shared<Mutex>(lock, source.GetPointer()));
    SwitchToSchedulerFromThread(source);
    return { RESULT_OK, mutex };
}

OS::Result OS::SVCReleaseMutex(Thread& source, Handle mutex_handle) {
    ThreadLogger(&source) << "SVCReleaseMutex: handle=" << mutex_handle << std::endl;
    auto mutex = source.GetHandleTable().FindObject<Mutex>(mutex_handle);
    if (!mutex)
        SVCBreak(source, BreakReason::Panic);

    if (!mutex->IsOwner(source.GetPointer()))
        SVCBreak(source, BreakReason::Panic);

    mutex->Release();

    SwitchToSchedulerFromThread(source);
    return RESULT_OK;
}

std::pair<OS::Result,HandleTable::Entry<Event>> OS::SVCCreateEvent(Thread& source, ResetType type) {
    ThreadLogger(&source) << "SVCCreateEvent: type=" << static_cast<uint32_t>(type) << std::endl;

    if (type != ResetType::OneShot && type != ResetType::Sticky) {
        // Unsupported reset type. TODO: Not sure if Pulse makes any sense for events!
        SVCBreak(source, BreakReason::Panic);
    }

    auto event = source.GetHandleTable().CreateHandle<Event>(std::make_shared<Event>(type));
    SwitchToSchedulerFromThread(source);
    return { RESULT_OK, event };
}

OS::Result OS::SVCSignalEvent(Thread& source, Handle event_handle) {
    ThreadLogger(&source) << "SVCSignalEvent: handle=" << event_handle << std::endl;
    auto event = source.GetHandleTable().FindObject<Event>(event_handle);
    if (!event)
        SVCBreak(source, BreakReason::Panic);
    event->SignalEvent();
    SwitchToSchedulerFromThread(source);
    return RESULT_OK;
}

OS::Result OS::SVCClearEvent(Thread& source, Handle event_handle) {
    ThreadLogger(&source) << "SVCClearEvent: handle=" << event_handle << std::endl;
    auto event = source.GetHandleTable().FindObject<Event>(event_handle);
    if (!event)
        SVCBreak(source, BreakReason::Panic);
    event->ResetEvent();
    SwitchToSchedulerFromThread(source);
    return RESULT_OK;
}

std::pair<OS::Result,HandleTable::Entry<SharedMemoryBlock>> OS::SVCCreateMemoryBlock(Thread& source, VAddr addr, uint32_t size, uint32_t owner_perms, uint32_t other_perms) {
    ThreadLogger(&source) << "SVCCreateMemoryBlock: addr=0x" << std::hex << std::setw(8) << std::setfill('0') << addr
                          << ", size=0x" << size << ", owner_perms=" << owner_perms << ", other_perms=" << other_perms << std::endl;

    // TODO: Implement permissions

    auto phys_addr_opt = source.ResolveVirtualAddr(addr);
    if (!phys_addr_opt)
        SVCBreak(source, BreakReason::Panic);

    auto block = source.GetHandleTable().CreateHandle<SharedMemoryBlock>(std::make_shared<SharedMemoryBlock>(*phys_addr_opt, size));

    SwitchToSchedulerFromThread(source);
    return { RESULT_OK, block };
}

OS::Result OS::SVCMapMemoryBlock(Thread& source, Handle block_handle, VAddr addr, uint32_t caller_perms, uint32_t other_perms) {
    ThreadLogger(&source) << "SVCMapMemoryBlock: handle=" << block_handle
                          << ", addr=" << std::hex << std::setw(8) << std::setfill('0') << addr
                          << ", caller_perms=" << caller_perms << ", other_perms=" << other_perms << std::endl;

    auto block = source.GetHandleTable().FindObject<SharedMemoryBlock>(block_handle);
    if (!block)
        SVCBreak(source, BreakReason::Panic);

    // TODO: Implement permissions

    if (!source.MapVirtualMemory(block->phys_address, block->size, addr))
        SVCBreak(source, BreakReason::Panic);

    SwitchToSchedulerFromThread(source);
    return RESULT_OK;
}


OS::Result OS::SVCCloseHandle(Thread& source, Handle handle) {
    ThreadLogger(&source) << "SVCCloseHandle: handle=" << handle << std::endl;

    // First off, release any remaining locks on ObserverSubjects
    // TODO: No idea whether we are actually supposed to do this here, since there are explicit release SVCs for this.

    if (auto mutex = source.GetHandleTable().FindObject<Mutex>(handle, true)) {
        if (mutex->IsOwner(source.GetPointer())) {
            mutex->Release();
        }
    }

    // Delete the handle from the table
    source.GetHandleTable().CloseHandle(handle);
    SwitchToSchedulerFromThread(source);
    return RESULT_OK;
}

OS::Result OS::SVCWaitSynchronization(Thread& source, Handle handle, uint64_t timeout) {
    ThreadLogger(&source) << "SVCWaitSynchronization: handle=0x" << std::hex << handle
                          << ", timeout=0x" << timeout << std::endl;
    return OS::SVCWaitSynchronizationN(source, &handle, 1, false, timeout).first;
}

// TODO: What should this return for the "index" when wait_for_all is true?
std::pair<OS::Result,int32_t> OS::SVCWaitSynchronizationN(Thread& source, Handle* handles, uint32_t handle_count, bool wait_for_all, uint64_t timeout) {
    ThreadLogger(&source) << "SVCWaitSynchronizationN: handle_count=0x" << std::hex << handle_count
                          << ", wait_for_all=" << wait_for_all << ", timeout=0x" << timeout << std::endl;

    // TODO: Assert wait_list is empty
    for (Handle* handle = handles; handle != handles + handle_count; ++handle) {
        auto subject = source.GetHandleTable().FindObject<ObserverSubject>(*handle);
        if (!subject)
            SVCBreak(source, BreakReason::Panic);
        subject->Register(source.GetPointer());
        source.wait_list.push_back({ (int32_t)(handle - handles), subject });
    }

    // TODO: In some cases, we don't even schedule once.. is that okay? Should we insert an artificial reschedule here?

    for(;;) {
        for (auto it = source.wait_list.begin(); it != source.wait_list.end();) {
            if (!it->second->TryAcquire(source.GetPointer())) {
                ++it;
                continue;
            }

            // Resource acquired successfully - remove it from the wait_list.
            // NOTE: TryAcquire already Unregister'ed us.
            int32_t index = it->first;
            it = source.wait_list.erase(it);

            if (!wait_for_all) {
                // we are already done, clear the remaining events
                for (auto& sync_object : source.wait_list)
                    sync_object.second->Unregister(source.GetPointer());

                source.wait_list.clear();
                return { RESULT_OK, index };
            } else if (source.wait_list.empty()) {
                // TODO: What index would we return in this case, though?
                return { RESULT_OK, index };
            }
            // else try to acquire any of the remaining resources and then reschedule
        }

        // Resources weren't ready immediately, hence put the thread to sleep until it's woken up using SignalResourceReady()
        source.sleeping = true;
        SwitchToSchedulerFromThread(source);
    }
}

std::pair<OS::Result,HandleTable::Entry<ClientSession>> OS::SVCConnectToPort(Thread& source, const std::string& name) {
    ThreadLogger(&source) << "SVCConnectToPort: name=" << name << std::endl;

    auto port_it = ports.find(name);
    if (port_it == ports.end())
        SVCBreak(source, BreakReason::Panic);

    auto server_port = port_it->second->server.lock();
    if (!server_port)
        SVCBreak(source, BreakReason::Panic);

    auto session = std::make_shared<Session>();
    auto client_session = std::make_shared<ClientSession>(session); // handle created below
    auto server_session = std::make_shared<ServerSession>(session); // No handle created by this here, so it either gets consumed by the server or dies
    session->client = client_session;
    session->server = server_session;

    auto client_session_handle = source.GetHandleTable().CreateHandle(client_session).first;

    server_port->OfferSession(server_session);

    OS::Result result = SVCWaitSynchronization(source, client_session_handle, 0);
    if (result != RESULT_OK) {
        // TODO: Deal more gracefully with this case!
        SVCBreak(source, BreakReason::Panic);
    }

    return { result, { client_session_handle, client_session } };
}

OS::Result OS::SVCSendSyncRequest(Thread& source, Handle session_handle) {
    ThreadLogger(&source) << "SVCSendSyncRequest: handle=" << session_handle
                          << ", cmdheader=" << std::hex << std::setw(8) << std::setfill('0') << source.ReadTLS(0x80) << std::endl;

    auto session = source.GetHandleTable().FindObject<ClientSession>(session_handle);
    if (!session)
        SVCBreak(source, BreakReason::Panic);

    ThreadLogger(&source) << "CLIENT SendSyncRequest CommandReady" << std::endl;
    // TODO: Pointer validation
    session->thread = source.GetPointer();
    session->session->server.lock()->CommandReady();

    ThreadLogger(&source) << "CLIENT SendSyncRequest WaitSync" << std::endl;
    OS::Result result = SVCWaitSynchronization(source, session_handle, 0);
    ThreadLogger(&source) << "CLIENT SendSyncRequest WaitSync over" << std::endl;

    return result;
}

void OS::SVCBreak(Thread& source, OS::BreakReason reason) {
    ThreadLogger(&source) << "KERNEL PANIC - CANNOT CONTINUE!" << std::endl;
    throw std::runtime_error("KERNEL PANIC - CANNOT CONTINUE");
}

std::tuple<OS::Result,HandleTable::Entry<ServerPort>,HandleTable::Entry<ClientPort>>
OS::SVCCreatePort(Thread& source, const std::string& name, int32_t max_sessions) {
    ThreadLogger(&source) << "SVCCreatePort: name=" << name << ", max_sessions=" << max_sessions << std::endl;

    auto port = std::make_shared<Port>();
    auto server = std::make_shared<ServerPort>(port);
    auto client = std::make_shared<ClientPort>(port);
    port->server = server;
    port->client = client;

    // TODO: Need verification on "name": Make sure it's null-terminated, make sure there is not already a port with the given name!

    // TODO: When both the client and server handles are released, this entry should be removed again!
    if (!name.empty())
        ports.insert({name, port});

    SwitchToSchedulerFromThread(source);
    source.GetHandleTable().CreateHandle(client);
    return std::make_tuple(RESULT_OK, source.GetHandleTable().CreateHandle(server), source.GetHandleTable().CreateHandle(client));
}

std::pair<OS::Result,HandleTable::Entry<ClientSession>> OS::SVCCreateSessionToPort(Thread& source, Handle client_port_handle) {
    // TODO: Behavior of this function is guess and needs to be verified!
    ThreadLogger(&source) << "SVCCreateSessionToPort: handle=" << std::dec << client_port_handle << std::endl;

    auto client_port = source.GetHandleTable().FindObject<ClientPort>(client_port_handle);
    if (!client_port)
        SVCBreak(source, BreakReason::Panic);

    // Create session
    auto session = std::make_shared<Session>();
    auto client_session = std::make_shared<ClientSession>(session); // handle created below
    auto server_session = std::make_shared<ServerSession>(session); // No handle created by this here, so it either gets consumed by the server or dies
    session->client = client_session;
    session->server = server_session;

    auto client_session_handle_entry = source.GetHandleTable().CreateHandle(client_session);

    // Offer session and wait until the server calls AcceptSession on it
    client_port->port->server.lock()->OfferSession(server_session);
    OS::Result result = SVCWaitSynchronization(source, client_session_handle_entry.first, 0);
    if (result != RESULT_OK) {
        // TODO: Deal more gracefully with this case!
        SVCBreak(source, BreakReason::Panic);
    }

    return { result, client_session_handle_entry };
}

std::pair<OS::Result,HandleTable::Entry<ServerSession>> OS::SVCAcceptSession(Thread& source, ServerPort& server_port) {
    ThreadLogger(&source) << "SVCAcceptSession" << std::endl; // TODO: Print ServerPort handle

    if (server_port.session_queue.empty())
        SVCBreak(source, BreakReason::Panic);

    auto session = server_port.session_queue.front();
    auto session_handle_entry = source.GetHandleTable().CreateHandle(session);
    server_port.session_queue.pop();

    auto client_session = session->session->client.lock();
    if (!client_session)
        SVCBreak(source, BreakReason::Panic);
    client_session->SetReady();

    SwitchToSchedulerFromThread(source);
    return { RESULT_OK, session_handle_entry };
}

void OS::TranslateIPCMessage(Thread& source, Thread& dest) {
    IPC::CommandHeader header = { source.ReadTLS(0x80) };
    // Copy header code and normal parameters
    unsigned tls_offset = 0x80;
    while (tls_offset < 0x84 + 4 * header.num_normal_params) {
        dest.WriteTLS(tls_offset, source.ReadTLS(tls_offset));
        tls_offset += 4;
    }

    // Copy translate parameters
    uint32_t command_end = 0x84 + 4 * (header.num_normal_params + header.size_translate_params);
    while (tls_offset < command_end) {
        // Copy translation descriptor
        IPC::TranslationDescriptor descriptor = { source.ReadTLS(tls_offset) };
        dest.WriteTLS(tls_offset, descriptor.raw);
        tls_offset += 4;

        // Write translated parameters
        switch (descriptor.type) {
        case IPC::TranslationDescriptor::Type::Handles:
            if (descriptor.handles.close_handle && descriptor.handles.fill_process_id) {
                // TODO: No idea whether this is supported at all or what it
                //       would be doing. Since process handles are global, this
                //       wouldn't make a lot of sense. On the other hand, maybe
                //       it's just implemented as a NOP.
                SVCBreak(dest, BreakReason::Panic);
            }

            for (uint32_t i = 0; i < descriptor.handles.NumHandles(); ++i) {
                Handle source_handle, handle;
                if (descriptor.handles.fill_process_id) {
                    source_handle = source.GetHandleTable().FindHandle<Process>(source.GetParentProcess());
                } else {
                    source_handle = source.ReadTLS(tls_offset);
                }
                {
                    // Translate handles
                    if (source_handle == HANDLE_INVALID)
                        SVCBreak(source, BreakReason::Panic);

                    auto object = source.GetHandleTable().FindObject<Object>(source_handle);
                    if (!object)
                        SVCBreak(source, BreakReason::Panic);

                    auto handle_table_entry = dest.GetHandleTable().CreateHandle(object);
                    handle = handle_table_entry.first;

                    std::cout << "Translated " << GetThreadObjectName(source) << " handle 0x" << std::hex << source_handle
                              << " to the " << GetThreadObjectName(dest) << " handle 0x" << handle
                              << " (object type: " << typeid(*object.get()).name() << ")" << std::endl;

                    if (descriptor.handles.close_handle)
                        SVCCloseHandle(source, source_handle);
                }
                dest.WriteTLS(tls_offset, handle);
                tls_offset += 4;
            }
            break;

        case IPC::TranslationDescriptor::Type::StaticBuffer:
        {
            uint32_t source_data_addr = source.ReadTLS(tls_offset);
            IPC::TranslationDescriptor dest_descriptor = { dest.ReadTLS(0x180 + 8 * descriptor.static_buffer.id) };
            uint32_t dest_buffer_size = dest_descriptor.static_buffer.size;
            uint32_t dest_buffer_addr = dest.ReadTLS(0x184 + 8 * descriptor.static_buffer.id);

            if (dest_descriptor.type != IPC::TranslationDescriptor::Type::StaticBuffer ||
                    dest_buffer_size < descriptor.static_buffer.size)
                SVCBreak(source, BreakReason::Panic);

            for (uint32_t offset = 0; offset < descriptor.static_buffer.size; ++offset)
                dest.WriteMemory(dest_buffer_addr + offset, source.ReadMemory(source_data_addr + offset));

            // Read the static buffer address from TLS and copy it into the IPC message
            dest.WriteTLS(tls_offset, dest_buffer_addr);
            tls_offset += 4;

            break;
        }

        default:
            // Unknown descriptor type
            SVCBreak(source, BreakReason::Panic);
        }
    }
}

std::pair<OS::Result,int32_t> OS::SVCReplyAndReceive(Thread& source, Handle* handles, uint32_t handle_count, Handle reply_target) {
    ThreadLogger(&source) << "SVCReplyAndReceive: handle_count=" << handle_count << ", reply_handle=" << reply_target << std::endl;

    if (reply_target != HANDLE_INVALID) {
        auto server_session = source.GetHandleTable().FindObject<ServerSession>(reply_target);
        if (!server_session)
            SVCBreak(source, OS::BreakReason::Panic);

        // Copy over server response to client's TLS
        auto client_session = server_session->session->client.lock();
        auto client_thread = client_session->thread.lock();
        TranslateIPCMessage(source, *client_thread);

        ThreadLogger(&source) << "SERVER ReplyAndReceive notifying client session about IPC reply" << std::endl;
        client_session->SetReady();
    }

    ThreadLogger(&source) << "SERVER ReplyAndReceive waiting" << std::endl;
    auto ret = SVCWaitSynchronizationN(source, handles, handle_count, 0, 0);
    if (ret.first != RESULT_OK)
        return ret;

    Handle received_handle = handles[ret.second];
    auto server_session = source.GetHandleTable().FindObject<ServerSession>(received_handle, true);
    if (server_session) {
        // Incoming client request - copy over request data to the server TLS, performing any requested parameter translation

        auto client_session = server_session->session->client.lock();
        auto client_thread = client_session->thread.lock();

        TranslateIPCMessage(*client_thread, source);

        return ret;
    } else {
        // Other kinds of handles are ignored here
        return ret;
    }
}

void OS::SVCDoNothing(Thread& source) {
    SwitchToSchedulerFromThread(source);
}

void OS::SVCRaw(Thread& source, unsigned svc_id, Interpreter::CPUContext& ctx) try {
    ThreadLogger(&source) << "SVCRaw entering" << std::endl;
    switch (svc_id) {
    case 0x1: // ControlMemory
    {
        std::tie(ctx.cpu.reg[0],ctx.cpu.reg[1]) = SVCControlMemory(source, ctx.cpu.reg[1], ctx.cpu.reg[2], ctx.cpu.reg[3], ctx.cpu.reg[0], ctx.cpu.reg[4]);
        break;
    }

    case 0x8: // CreateThread
    {
        HandleTable::Entry<Thread> thread;
        std::tie(ctx.cpu.reg[0],thread) = SVCCreateThread(source, ctx.cpu.reg[1], ctx.cpu.reg[2], ctx.cpu.reg[3], ctx.cpu.reg[0], ctx.cpu.reg[4]);
        ctx.cpu.reg[1] = thread.first;
        break;
    }

    case 0x13: // CreateMutex
    {
        HandleTable::Entry<Mutex> mutex;
        std::tie(ctx.cpu.reg[0],mutex) = SVCCreateMutex(source, ctx.cpu.reg[0]);
        ctx.cpu.reg[1] = mutex.first;
        break;
    }

    case 0x14: // ReleaseMutex
    {
        Handle handle = ctx.cpu.reg[0];
        ctx.cpu.reg[0] = SVCReleaseMutex(source, handle);
        break;
    }

    case 0x17: // CreateEvent
    {
        auto type = static_cast<ResetType>(ctx.cpu.reg[1]);
        HandleTable::Entry<Event> event;
        std::tie(ctx.cpu.reg[0],event) = SVCCreateEvent(source, type);
        ctx.cpu.reg[1] = event.first;
        break;
    }

    case 0x1f: // MapMemoryBlock
    {
        ctx.cpu.reg[0] = OS::SVCMapMemoryBlock(source, ctx.cpu.reg[0], ctx.cpu.reg[1], ctx.cpu.reg[2], ctx.cpu.reg[3]);
        break;
    }

    case 0x23: // CloseHandle
    {
        Handle handle = ctx.cpu.reg[0];
        ctx.cpu.reg[0] = SVCCloseHandle(source, handle);
        break;
    }

    case 0x24: // WaitSynchronization
    {
        Handle handle = ctx.cpu.reg[0];
        // TODO: Word order?
        int32_t timeout = (static_cast<uint64_t>(ctx.cpu.reg[1]) << 32) | ctx.cpu.reg[2];
        ctx.cpu.reg[0] = SVCWaitSynchronization(source, handle, timeout);
        break;
    }

    case 0x25: // WaitSynchronizationN
    {
        // TODO: Word order?
        int32_t timeout = (static_cast<uint64_t>(ctx.cpu.reg[4]) << 32) | ctx.cpu.reg[5];

        std::vector<Handle> handles(ctx.cpu.reg[2]);
        for (uint32_t index = 0; index < handles.size(); ++index)
            handles[index] = Interpreter::Processor::ReadVirtualMemory<uint32_t>(dynamic_cast<EmuThread&>(source).ctx, ctx.cpu.reg[1] + 4 * index);

        std::tie(ctx.cpu.reg[0],ctx.cpu.reg[1]) = SVCWaitSynchronizationN(source, handles.data(), handles.size(), ctx.cpu.reg[3], timeout);
        break;
    }

    case 0x2d: // ConnectToPort
    {
        uint32_t name_addr = ctx.cpu.reg[1];
        std::string port_name;
        uint8_t c;
        while (0 != (c = Interpreter::Processor::ReadVirtualMemory<uint8_t>(ctx, name_addr++))) {
            port_name.push_back(c);
        }

        uint32_t out_handle_addr = ctx.cpu.reg[0];

        HandleTable::Entry<ClientSession> session;
        std::tie(ctx.cpu.reg[0],session) = SVCConnectToPort(source, port_name);
        ctx.cpu.reg[1] = session.first;

        break;
    }

    case 0x32: // SendSyncRequest
    {
        Handle session_handle = ctx.cpu.reg[0];
        ctx.cpu.reg[0] = SVCSendSyncRequest(source, session_handle);
        break;
    }

    case 0xfe: // Virtual "Dump file" exit SVC call - dump buffer at address r0 with size r1
    {
        uint32_t addr = ctx.cpu.reg[0];
        uint32_t size = ctx.cpu.reg[1];
        std::ofstream file("dump.data");
        for (uint32_t dump_addr = addr; dump_addr < addr + size; ++dump_addr) {
            auto data = Interpreter::Processor::ReadVirtualMemory<uint8_t>(ctx, dump_addr);
            file.write(reinterpret_cast<const char*>(&data), sizeof(data));
        }
    }

    case 0xff: // Virtual exit SVC call - return r0
        exit(ctx.cpu.reg[0]);
        break;

    default:
        throw std::runtime_error("Unknown SVC index " + std::to_string(svc_id));
    }
    ThreadLogger(&source) << "SVCRaw leaving" << std::endl;
} catch(const std::runtime_error& err) {
    ThreadLogger(&source) << err.what() << std::endl;
    gdbstub->NotifySegfault();

    // TODO: Exit more gracefully?
    while (true) {}
}

void OS::SwitchToSchedulerFromThread(Thread& thread) {
    (*thread.coroutine_yield)(scheduler_coro);
}

void OS::StartScheduler(boost::coroutines::symmetric_coroutine<void>::yield_type& yield) {
    while (!threads.empty()) {
        // "Schedule" and dispatch next thread
        auto next_thread = threads.front().lock();
        if (!next_thread) {
            // thread was closed => remove from list and continue
            threads.pop_front();
            continue;
        }

        active_thread = next_thread.get();

        if (!next_thread->sleeping) {
            next_thread->GetHandleTable().SetCurrentThread(next_thread);
            next_thread->RestoreContext(*glob_ctx);
            yield(next_thread->coroutine);
            next_thread->SaveContext(*glob_ctx);
        }

        // Move thread to the end of the queue
        std::rotate(threads.begin(), std::next(threads.begin()), threads.end());
    }
}

void OS::Run(std::shared_ptr<Interpreter::Setup> setup, std::unique_ptr<Interpreter::ProcessorController> controller) {
    glob_ctx = &setup->cpus[0];

    gdbstub = controller.get();

    // Create hello world thread
    auto thread = std::shared_ptr<Thread>(new BootThread(setup->cpus[0]));
    MakeProcessFromThread(thread);

    scheduler_coro();
}

}  // namespace OS

}  // namespace HLE
