#!/usr/bin/env bash

: "${ARMEMU:?Need to set ARMEMU}"

if [ ! -d www.mrtc.mdh.se/projects/wcet/wcet_bench ]
then
	echo "Please run get.sh to download benchmark files first!"
	exit
fi

cd www.mrtc.mdh.se/projects/wcet/wcet_bench

# Find all tests to run (exclude bsort100 because it's broken even outside the emulator)
export TEST_PATHS=`find . ! -path . -type d | grep -v bsort100`

echo REFERENCE RESULTS
for dir in $TEST_PATHS; do echo -n $dir" "; cd $dir; gcc -lm *.c 2> /dev/null && ./a.out; export var=$?; [[ -f a.out ]] && echo $var || echo; cd ..; done

echo
echo
echo ARMEMU RESULTS
for dir in $TEST_PATHS; do echo -n $dir" "; cd $dir; arm-none-eabi-gcc -lm *.c ../../../../../start.s 2> /dev/null && $ARMEMU --input=./a.out &> /dev/null; export var=$?; [[ -f a.out ]] && echo $var || echo; cd ..; done &

# Spawn an equal number of GDB sessions to connect to the started emulators...
#for dir in $TEST_PATHS; do ~/devkitPro/devkitARM/bin/arm-none-eabi-gdb < <(echo -e "target remote localhost:12345\nc\nq"); done &> /dev/null

cd ../../../..
wait
