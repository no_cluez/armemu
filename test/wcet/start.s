# Simple startup assembly code to serve as out entry and exit point

.section .text.start
.arm
.global _start
.type _start, %function

_start:
    bl main

loop:
    # Exit emulator
    svc 0xff
    b loop
