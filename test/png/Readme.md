PNG Decoder Test
================

Create a `libs` subdirectory, download zlib and libpng and extract them in that subdirectory. The input image must be provided at compile time, and needs to be compiled to an object file using `arm-none-eabi-ld -r -b binary -o image.o <input.png>`.

zlib:
CHOST=arm-none-eabi ./configure --prefix=$PNGTESTPATH/libs --static

libpng:
CPPFLAGS=-I$PNGTESTPATH/libs/include LDFLAGS=-L$PNGTESTPATH/libs/lib ./configure --prefix=$PNGTESTPATH/libs --host=arm-none-eabi --disable-shared --enable-static

test:
arm-none-eabi-gcc main.c start.s image.o -I $PNGTESTPATH/libs/include/ -L$PNGTESTPATH/libs/lib -lpng -lz -lm -march=armv6k -mtune=mpcore -mfloat-abi=soft -g
