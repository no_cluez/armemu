.section .text.start
.arm
.global _start
.type _start, %function

.global svcDumpFile
.type svcDumpFile, %function

svcDumpFile:
    # pass on r0 and r1 to the system call
    svc 0xfe
    bx lr

_start:
    bl main

loop:
    # Exit emulator
    svc 0xff
    b loop
